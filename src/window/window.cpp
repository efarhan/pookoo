/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <window/window.h>
#include <window/internal/internal.h>
#include <log/log.h>
#include <config/config.h>
#include <utils/const.h>

#include <stdlib.h>
#include <stdarg.h>
#include <string.h>



// Externalized in internal header for other modules to use.
void* internal_window_handler = NULL;
int internal_last_keyboard_scancode = -1;
char internal_keyboard_text[MAX_KEYBOARD_INPUT_LEN] = { '\0' };

typedef struct 
{
	unsigned int windowed_width;
	unsigned int windowed_height;

	unsigned int window_width;
	unsigned int window_height;
} window_vars;

static window_vars vars;
static void (*exit_func)(void);
static int internal_has_reset;
static char internal_is_fullscreen;

void window_cursor_show(bool show)
{
	auto window = static_cast<sf::RenderWindow*>(internal_window_handler);
	window->setMouseCursorVisible(show);
}

int window_toggle_fullscreen()
{
	auto window = static_cast<sf::RenderWindow*>(internal_window_handler);

	// get previous window setting and just toggle fullscreen
	internal_is_fullscreen = !internal_is_fullscreen;
	window->create(internal_is_fullscreen ? sf::VideoMode::getDesktopMode() : sf::VideoMode(vars.windowed_width,vars.windowed_height, 32) , "GGJ15",
	(internal_is_fullscreen ? sf::Style::Fullscreen : sf::Style::Resize | sf::Style::Close));

	return 0;
}

int window_begin()
{


	internal_has_reset = 1;

	internal_is_fullscreen = config_read_int("fullscreen", 0);
	vars.window_width = config_read_int("width", 640);
	vars.window_height = config_read_int("height", 360);

	vars.windowed_width = vars.window_width;
	vars.windowed_height = vars.window_height;

	sf::RenderWindow* window;

	sf::Uint32 flags = sf::Style::Default;

	if(internal_is_fullscreen)
	{
		flags = sf::Style::Fullscreen;
		window = new sf::RenderWindow(sf::VideoMode::getDesktopMode(), "GGJ15", flags);
	}
	else
	{
		window = new sf::RenderWindow(sf::VideoMode((int)vars.window_width, (int)vars.window_height), "GGJ15", flags);

	}
	log_info("OpenGL Version: %u.%u",window->getSettings().majorVersion,window->getSettings().minorVersion);
	internal_window_handler = (void*)window;
	
	return 1;
}

int window_was_reset(void)
{
	int ret = internal_has_reset;
	internal_has_reset = 0;
	return ret;
}

void window_finish()
{
	config_write_int("width", vars.windowed_width);
	config_write_int("height", vars.windowed_height);
	config_write_int("fullscreen", internal_is_fullscreen);

	/* Return to original resolution. */
	if(internal_is_fullscreen)
	{
		window_toggle_fullscreen();
	}
}

// Returns 1 if no 'close' events were triggered.
static int internal_window_events(void)
{
	int do_continue = 1;
	int t;
	auto window = static_cast<sf::RenderWindow*>(internal_window_handler);
	sf::Event event;
	while (window->pollEvent(event))
	{
		// évènement "fermeture demandée" : on ferme la fenêtre
		if (event.type == sf::Event::Closed)
		{
			window->close();
			do_continue = 0;
		}
		else if (event.type == sf::Event::Resized)
		{
			vars.window_width = event.size.width;
			vars.window_height = event.size.height;

			// Don't store fullscreen resolution.
			if (!internal_is_fullscreen)
			{
				vars.windowed_width = vars.window_width;
				vars.windowed_height = vars.window_height;
			}
			glViewport(0, 0, event.size.width, event.size.height);
			internal_has_reset = 1;
		}
		/*
		 * if(!SDL_PollEvent(&event))
		{
			break;
		}

		t = event.type;

		if(t == SDL_KEYDOWN)
		{
			int key = event.key.keysym.scancode;

			if(key == SDL_SCANCODE_F4)
			{
				const Uint8* keys = SDL_GetKeyboardState(NULL);

				if(keys[SDL_SCANCODE_RALT]
				|| keys[SDL_SCANCODE_LALT])
				{
					do_continue = 0;
				}
			}
			else if (key == SDL_SCANCODE_Q)
			{
				const Uint8* keys = SDL_GetKeyboardState(NULL);

				if(keys[SDL_SCANCODE_MODE]
				|| keys[SDL_SCANCODE_LCTRL]
				|| keys[SDL_SCANCODE_RCTRL])
				{
					do_continue = 0;
				}
			}
			else if (key == SDL_SCANCODE_F)
			{
				const Uint8* keys = SDL_GetKeyboardState(NULL);

				if(keys[SDL_SCANCODE_MODE]
				|| keys[SDL_SCANCODE_LCTRL]
				|| keys[SDL_SCANCODE_RCTRL])
				{
					window_toggle_fullscreen();
				}
			}
			else if (key == SDL_SCANCODE_RETURN)
			{
				const Uint8* keys = SDL_GetKeyboardState(NULL);

				if(keys[SDL_SCANCODE_RALT]
				|| keys[SDL_SCANCODE_LALT])
				{
					window_toggle_fullscreen();
				}
			}
			else if (key == SDL_SCANCODE_F11)
			{
				window_toggle_fullscreen();
			}

			internal_last_keyboard_scancode = key;
		}
		else if (t == SDL_TEXTINPUT)
		{
			memcpy((void*)internal_keyboard_text, (void*)event.text.text, MAX_KEYBOARD_INPUT_LEN);
		}
		else if (t == SDL_KEYUP)
		{
			int key = event.key.keysym.scancode;

			if (key == internal_last_keyboard_scancode)
			{
				internal_last_keyboard_scancode = -1;
			}
		}
		else if(t == SDL_WINDOWEVENT)
		{
			if(event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)
			{
				vars.window_width = (unsigned int)event.window.data1;
				vars.window_height = (unsigned int)event.window.data2;

				// Don't store fullscreen resolution.
				if (!internal_is_fullscreen)
				{
					vars.windowed_width = vars.window_width;
					vars.windowed_height = vars.window_height;
				}

				internal_has_reset = 1;
			}
		}
       	else if(t == SDL_QUIT)
		{
			do_continue = 0;
		}
		 */
	}


	return do_continue;
}

int window_step()
{
	auto window = static_cast<sf::RenderWindow*>(internal_window_handler);
	window->display();

	return internal_window_events();
}

unsigned int window_width()
{
	if (internal_is_fullscreen){
		return sf::VideoMode::getDesktopMode().width;

	}
	else
	{
		return vars.window_width;
	}
	
}

unsigned int window_height()
{
	if (internal_is_fullscreen){
		return sf::VideoMode::getDesktopMode().height;

	}
	else
	{
		return vars.window_height;
	}
}
char window_is_fullscreen()
{
	return internal_is_fullscreen;
}
