
#include <chat/chat.h>
#include <log/log.h>
#include <utils/utils.h>

#include <strophe.h>

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include <list>
#include <set>
#include <algorithm>

#define CHAT_BUFFER 512

struct message_t
{
	const char* from;
	const char* message;
};

struct lt_str
{
	bool operator()(const char* s1, const char* s2) const
	{
		return strcmp(s1, s2) < 0;
	}
};

struct chat_t
{
	xmpp_conn_t *conn;
	std::list<message_t*> messages;

	std::set<const char*, lt_str> contacts_added;
	std::set<const char*, lt_str> contacts_deleted;
};

struct lt_chat
{
	bool operator()(chat_t* c1, chat_t* c2) const
	{
		return c1->conn < c2->conn;
	}
};

static std::set<chat_t*, lt_chat> internal_chats;

static xmpp_log_t *internal_logger;
static xmpp_ctx_t *internal_context;

int chat_begin()
{
	// Init library
	xmpp_initialize();

	// Create a context and logger.
	if ((internal_logger = xmpp_get_default_logger(XMPP_LEVEL_ERROR)) == NULL)
	{
		return 0;
	}
	
	if ((internal_context = xmpp_ctx_new(NULL, internal_logger)) == NULL)
	{
		return 0;
	}

	return 1;
}

static void internal_add_contact(chat_t* chat, const char* name)
{
	chat->contacts_added.insert(utils_dupstr(name));
}

static void internal_remove_contact(chat_t* chat, const char* name)
{
	chat->contacts_deleted.insert(utils_dupstr(name));
}

static int internal_presence_handler(
	xmpp_conn_t * const conn, xmpp_stanza_t * const stanza,
	void * const userdata)
{
	chat_t cmp;
	char* from = xmpp_stanza_get_attribute(stanza, "from");
	char* type = xmpp_stanza_get_attribute(stanza, "type");

	cmp.conn = conn;
	chat_t* chat = *internal_chats.find(&cmp);

	if (type == NULL
	 || !strcmp(type, "subscribed"))
	{
		// We need to have a 'to' field, or we will have ourself
		// in the contact list.
		if (xmpp_stanza_get_attribute(stanza, "to") == NULL)
		{
			return 1;
		}

		internal_add_contact(chat, from);
	}
	else if (!strcmp(type, "unsubscribed")
		|| !strcmp(type, "unavailable"))
	{
		internal_remove_contact(chat, from);
	}
	else if (!strcmp(type, "subscribe"))
	{
		// We got an invitation! We accept it...
		xmpp_stanza_t* pres = xmpp_stanza_new(internal_context);
		xmpp_stanza_set_name(pres, "presence");
		xmpp_stanza_set_type(pres, "subscribed");
		xmpp_stanza_set_attribute(pres, "to", from);
		xmpp_send(conn, pres);

		// ...and ask for a subscription back.
		xmpp_stanza_set_type(pres, "subscribe");
		xmpp_send(conn, pres);
		xmpp_stanza_release(pres);
	}
	else
	{
		log_info("%s is %s", from, type);
		return 1;
	}

	return 1;
}

/*static int internal_iq_handler(
	xmpp_conn_t * const conn, xmpp_stanza_t * const stanza,
	void * const userdata)
{
	chat_t cmp;
	char* type = xmpp_stanza_get_type(stanza);

	cmp.conn = conn;
	chat_t* chat = *internal_chats.find(&cmp);

	if (!strcmp(type, "error"))
	{
		return 1;
	}
	else if (!strcmp(type, "result"))
	{
		xmpp_stanza_t* query = xmpp_stanza_get_child_by_name(stanza, "query");

		if (query != NULL)
		{
			xmpp_stanza_t* item;
			char* name;

			for (item = xmpp_stanza_get_children(query);
				item != NULL;
				item = xmpp_stanza_get_next(item))
			{
				internal_add_contact(chat, xmpp_stanza_get_attribute(item, "jid"));
			}
		}
	}

	return 1;
}*/

static int internal_message_handler(
	xmpp_conn_t* const conn, xmpp_stanza_t * const stanza,
	void * const userdata)
{
	char* type = xmpp_stanza_get_attribute(stanza, "type");

	if (type == NULL)
	{
		// Can happen on "invite" messages for instance.
		return 1;
	}

	// Do nothing on error
	if (!strcmp(type, "error"))
	{
		return 1;
	}

	// Handle direct messages
	if (!strcmp(type, "chat"))
	{
		char* from = xmpp_stanza_get_attribute(stanza, "from");
		xmpp_stanza_t* body = xmpp_stanza_get_child_by_name(stanza, "body");

		if (body != NULL)
		{
			message_t* message;
			chat_t cmp;

			if ((message = (message_t*)malloc(sizeof(message_t))) == NULL)
			{
				return 1;
			}

			cmp.conn = conn;

			chat_t* chat = *internal_chats.find(&cmp);

			message->message = xmpp_stanza_get_text(body);
			message->from = utils_dupstr(from);

			chat->messages.push_back(message);
		}

		/*if (xmpp_stanza_get_child_by_name(stanza, "composing") != NULL) 
		{
			log_info("%s is typing...", from);
		}
		else if (xmpp_stanza_get_child_by_name(stanza, "paused") != NULL)
		{
			log_info("%s stopped typing.", from);
		}
		else if (xmpp_stanza_get_child_by_name(stanza, "active") != NULL)
		{
			log_info("%s erased what he was typing.", from);
		}*/
	}

	return 1;
}

// Define a handler for connection events
static void internal_connection_handler(
	xmpp_conn_t * const conn, const xmpp_conn_event_t status,
	const int error, xmpp_stream_error_t * const stream_error,
	void * const userdata)
{
	if (status == XMPP_CONN_CONNECT) 
	{
		//xmpp_handler_add(conn, internal_iq_handler, NULL, "iq", NULL, NULL);
		xmpp_handler_add(conn, internal_message_handler, NULL, "message", NULL, NULL);
		xmpp_handler_add(conn, internal_presence_handler, NULL, "presence", NULL, NULL);

		// Send initial presence so that we appear online to contacts
		xmpp_stanza_t* pres = xmpp_stanza_new(internal_context);
		xmpp_stanza_set_name(pres, "presence");
		xmpp_send(conn, pres);
		xmpp_stanza_release(pres);

		// Send request to know our contacts.
		/*xmpp_stanza_t* iq = xmpp_stanza_new(internal_context);
		xmpp_stanza_set_name(iq, "iq");
		xmpp_stanza_set_type(iq, "get");
		xmpp_stanza_set_id(iq, "roster");
		xmpp_stanza_t* query = xmpp_stanza_new(internal_context);
		xmpp_stanza_set_name(query, "query");
		xmpp_stanza_set_ns(query, XMPP_NS_ROSTER);
		xmpp_stanza_add_child(iq, query);
		xmpp_stanza_release(query);
		xmpp_send(conn, iq);
		xmpp_stanza_release(iq);*/
	}
	else 
	{
		xmpp_stop(internal_context);
	}
}

chat_t* chat_open(const char* jid, const char* password)
{
	chat_t* chat;

	if ((chat = new chat_t) == NULL)
	{
		return NULL;
	}

	chat->conn = xmpp_conn_new(internal_context);

	if (chat->conn == NULL)
	{
		delete chat;
		return NULL;
	}

	// Setup authentication information
	xmpp_conn_set_jid(chat->conn, jid);
	xmpp_conn_set_pass(chat->conn, password);

	// initiate connection
	if (xmpp_connect_client(chat->conn, NULL, 0, internal_connection_handler, internal_context) < 0)
	{
		xmpp_conn_release(chat->conn);
		delete chat;
		return NULL;
	}

	internal_chats.insert(chat);
	return chat;
}

void chat_step(chat_t* chat)
{
	xmpp_run_once(internal_context, 0);
}

int chat_receive(chat_t* chat, const char** from, const char** message)
{
	auto it = chat->messages.begin();

	if (it == chat->messages.end())
	{
		return 0;
	}

	message_t* msg = *it;

	if (from)
	{
		*from = msg->from;
	}
	else
	{
		free((void*)msg->from);
	}

	if (message)
	{
		*message = msg->message;
	}
	else
	{
		free((void*)msg->message);
	}

	// It is the user's responsability to free *from and *message.
	chat->messages.erase(it);
	free(msg);

	return 1;
}

static void internal_send(chat_t* chat, const char* to, const char* message, const char* type)
{
	xmpp_stanza_t *reply, *body, *text;

	reply = xmpp_stanza_new(internal_context);
	xmpp_stanza_set_name(reply, "message");
	xmpp_stanza_set_type(reply, type);
	xmpp_stanza_set_attribute(reply, "to", to);

	body = xmpp_stanza_new(internal_context);
	xmpp_stanza_set_name(body, "body");

	text = xmpp_stanza_new(internal_context);
	xmpp_stanza_set_text(text, message);
	xmpp_stanza_add_child(body, text);
	xmpp_stanza_add_child(reply, body);

	xmpp_send(chat->conn, reply);
	xmpp_stanza_release(reply);
}

void chat_direct(chat_t* chat, const char* to, const char* format, ...)
{
	va_list args;
	char buffer[CHAT_BUFFER];

	va_start(args, format);
	vsnprintf(buffer, CHAT_BUFFER, format, args);
	va_end(args);

	internal_send(chat, to, buffer, "chat");
}

void chat_group(chat_t* chat, const char* to, const char* format, ...)
{
	va_list args;
	char buffer[CHAT_BUFFER];

	va_start(args, format);
	vsnprintf(buffer, CHAT_BUFFER, format, args);
	va_end(args);

	internal_send(chat, to, buffer, "groupchat");
}

void chat_presence(chat_t* chat, const char* to)
{
	// Send presence message
	// TODO: Handle name conflict
	xmpp_stanza_t* pres = xmpp_stanza_new(internal_context);
	xmpp_stanza_set_name(pres, "presence");
	xmpp_stanza_set_attribute(pres, "to", to);
	xmpp_send(chat->conn, pres);
	xmpp_stanza_release(pres);
}

const char* chat_contact_added(chat_t* chat)
{
	auto it = chat->contacts_added.begin();

	if (it != chat->contacts_added.end())
	{
		const char* ret = *it;
		chat->contacts_added.erase(it);
		return ret;
	}

	return NULL;
}

const char* chat_contact_deleted(chat_t* chat)
{
	auto it = chat->contacts_deleted.begin();

	if (it != chat->contacts_deleted.end())
	{
		const char* ret = *it;
		chat->contacts_deleted.erase(it);
		return ret;
	}
	
	return NULL;
}

void chat_close(chat_t* chat)
{
	if (chat != NULL)
	{
		for (auto it = chat->contacts_added.begin();
			      it != chat->contacts_added.end();)
		{
			free((void*)*it);
			it = chat->contacts_added.erase(it);
		}

		for (auto it = chat->contacts_deleted.begin();
			it != chat->contacts_deleted.end();)
		{
			free((void*)*it);
			it = chat->contacts_deleted.erase(it);
		}

		for (auto it = chat->messages.begin();
			      it != chat->messages.end();)
		{
			message_t* m = *it;

			free((void*)m->from);
			free((void*)m->message);
			free(m);

			it = chat->messages.erase(it);
		}

		internal_chats.erase(chat);

		// Release our connection
		xmpp_conn_release(chat->conn);
		delete chat;
	}
}

void chat_finish()
{
	// Final shutdown of the library
	xmpp_ctx_free(internal_context);
	xmpp_shutdown();
}