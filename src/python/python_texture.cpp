
#include <python/internal/internal.h>
#include <texture/texture.h>
#include <draw/draw.h>

typedef struct {
	PyObject_HEAD
	texture_t* texture;
} TextureObject;

static PyObject* python_texture_begin(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	texture_begin();

	Py_RETURN_TRUE;


}
static PyObject* python_texture_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	texture_finish();

	Py_RETURN_TRUE;


}

static int python_texture_init(TextureObject *self, PyObject *args)
{
	char* s;

	(void)self;

	if (!PyArg_ParseTuple(args, "s", &s)) 
	{
		PyErr_SetString(PyExc_AttributeError, "Missing texture name.");
		return -1;
	}

	self->texture = texture_open(s);

	if (self->texture == NULL)
	{
		// Font not found :(
		PyErr_SetString(PyExc_AttributeError, "Texture file not found.");
		return -1;
	}

	return 0;
}

static void python_texture_close(TextureObject *self)
{
	texture_close(self->texture);
}

static PyObject* python_texture_size(TextureObject *self, PyObject *args)
{
	return Py_BuildValue("ii", texture_width(self->texture), texture_height(self->texture));
}

static PyObject* python_texture_render(TextureObject *self, PyObject *args)
{
	int flip;
	if (!PyArg_ParseTuple(args, "i", &flip))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing flip argument.");
		Py_RETURN_NONE;
	}
	draw_texture(self->texture, flip);
	Py_RETURN_NONE;
}

static PyMethodDef texture_object_methods[] = {
	{ "size", (PyCFunction)python_texture_size, METH_NOARGS, "Returns the texture's width and height." },
	{ "render", (PyCFunction)python_texture_render, METH_VARARGS, "Render the given texture." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject TextureObjectType = {
	PyObject_HEAD_INIT(NULL)
	"TextureObject",                  /* tp_name        */
	sizeof(TextureObject),            /* tp_basicsize   */
	0,                                /* tp_itemsize    */
	(destructor)python_texture_close, /* tp_dealloc     */
	0,                                /* tp_print       */
	0,                                /* tp_getattr     */
	0,                                /* tp_setattr     */
	0,                                /* tp_compare     */
	0,                                /* tp_repr        */
	0,                                /* tp_as_number   */
	0,                                /* tp_as_sequence */
	0,                                /* tp_as_mapping  */
	0,                                /* tp_hash        */
	0,                                /* tp_call        */
	0,                                /* tp_str         */
	0,                                /* tp_getattro    */
	0,                                /* tp_setattro    */
	0,                                /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,               /* tp_flags       */
	"Texture instance.",              /* tp_doc         */
	0,	                              /* tp_traverse       */
	0,		                          /* tp_clear          */
	0,		                          /* tp_richcompare    */
	0,		                          /* tp_weaklistoffset */
	0,		                          /* tp_iter           */
	0,		                          /* tp_iternext       */
	texture_object_methods,           /* tp_methods        */
	0,                                /* tp_members        */
	0,		                          /* tp_getset         */
	0,		                          /* tp_base           */
	0,		                          /* tp_dict           */
	0,		                          /* tp_descr_get      */
	0,		                          /* tp_descr_set      */
	0,		                          /* tp_dictoffset     */
	(initproc)python_texture_init,    /* tp_init           */
};

static PyMethodDef texture_methods[] = {
		{ "begin", (PyCFunction)python_texture_begin, METH_NOARGS, "Load the texture module" },
		{ "finish", (PyCFunction)python_texture_finish, METH_NOARGS, "Unload the texture module" },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef texture_module = {
	PyModuleDef_HEAD_INIT,
	"texture",
	"texture",
	-1,
	texture_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_texture(PyObject* module)
{
	PyObject* texture = PyModule_Create(&texture_module);
	PyModule_AddObject(module, "texture", texture);

	TextureObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&TextureObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&TextureObjectType);
	PyModule_AddObject(texture, "Texture", (PyObject *)&TextureObjectType);
}
