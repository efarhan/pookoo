
#include <python/internal/internal.h>
#include <font/font.h>

typedef struct {
	PyObject_HEAD
	font_t* font;
} FontObject;

static int python_font_init(FontObject *self, PyObject *args, PyObject *kwds)
{
	char* s;

	(void)kwds;

	if (!PyArg_ParseTuple(args, "s", &s))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing font name.");
		return -1;
	}

	self->font = font_open(s);

	if (self->font == NULL)
	{
		// Font not found :(
		PyErr_SetString(PyExc_AttributeError, "Font file not found.");

		return -1;
	}

	return 0;
}

static void python_font_close(FontObject *self)
{
	font_close(self->font);
}

static PyObject* python_font_render(FontObject *self, PyObject *args)
{
	char* s;

	if (!PyArg_ParseTuple(args, "s", &s))
	{
		return NULL;
	}

	font_render(self->font, "%s", s);

	Py_RETURN_NONE;
}

static PyObject* python_font_facesize(FontObject *self, PyObject *args)
{
	int size;

	if (!PyArg_ParseTuple(args, "i", &size))
	{
		return NULL;
	}

	font_facesize(self->font, size);

	Py_RETURN_NONE;
}

static PyObject* python_font_size(FontObject *self, PyObject *args)
{
	float width, height;
	char* s;

	if (!PyArg_ParseTuple(args, "s", &s))
	{
		return NULL;
	}

	font_size(self->font, &width, &height, "%s", s);

	return Py_BuildValue("ff", width, height);
}

static PyMethodDef font_object_methods[] = {
	{ "render", (PyCFunction)python_font_render, METH_VARARGS, "Draw the given text with the given font." },
	{ "size", (PyCFunction)python_font_size, METH_VARARGS, "Get the size drawing the given text would take." },
	{ "facesize", (PyCFunction)python_font_facesize, METH_VARARGS, "Set the fonts face size." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject FontObjectType = {
	PyObject_HEAD_INIT(NULL)
	"FontObject",                  /* tp_name        */
	sizeof(FontObject),            /* tp_basicsize   */
	0,                             /* tp_itemsize    */
	(destructor)python_font_close, /* tp_dealloc     */
	0,                             /* tp_print       */
	0,                             /* tp_getattr     */
	0,                             /* tp_setattr     */
	0,                             /* tp_compare     */
	0,                             /* tp_repr        */
	0,                             /* tp_as_number   */
	0,                             /* tp_as_sequence */
	0,                             /* tp_as_mapping  */
	0,                             /* tp_hash        */
	0,                             /* tp_call        */
	0,                             /* tp_str         */
	0,                             /* tp_getattro    */
	0,                             /* tp_setattro    */
	0,                             /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,            /* tp_flags       */
	"Font instance.",              /* tp_doc         */
	0,	                           /* tp_traverse       */
	0,		                       /* tp_clear          */
	0,		                       /* tp_richcompare    */
	0,		                       /* tp_weaklistoffset */
	0,		                       /* tp_iter           */
	0,		                       /* tp_iternext       */
	font_object_methods,           /* tp_methods        */
	0,                             /* tp_members        */
	0,		                       /* tp_getset         */
	0,		                       /* tp_base           */
	0,		                       /* tp_dict           */
	0,		                       /* tp_descr_get      */
	0,		                       /* tp_descr_set      */
	0,		                       /* tp_dictoffset     */
	(initproc)python_font_init,    /* tp_init           */
};

static PyMethodDef font_methods[] = {
{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef font_module = {
	PyModuleDef_HEAD_INIT,
	"font",
	"font",
	-1,
	font_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_font(PyObject* module)
{
	PyObject* font = PyModule_Create(&font_module);
	PyModule_AddObject(module, "font", font);

	FontObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&FontObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&FontObjectType);
	PyModule_AddObject(font, "Font", (PyObject *)&FontObjectType);
}
