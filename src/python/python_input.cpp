
#include <python/internal/internal.h>
#include <input/input.h>

/*
 * Keyboard part
 */
static PyObject* python_input_keyboard_pressed(PyObject *self, PyObject *args)
{
	int key;

	(void)self;

	if (!PyArg_ParseTuple(args, "i", &key))
	{
		return NULL;
	}

	if (input_keyboard_pressed(key) != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_input_keyboard_get(PyObject *self, PyObject *args)
{
	int key;

	(void)self;
	(void)args;

	key = input_keyboard_get();

	if (key == -1)
	{
		Py_RETURN_NONE;
	}
	else
	{
		return Py_BuildValue("i", key);
	}
}



static PyObject* python_input_keyboard_string(PyObject* self, PyObject* args)
{
	(void)self;

	const char* str = input_keyboard_string_get();

	if (str[0] == '\0')
	{
		Py_RETURN_NONE;
	}
	else
	{
		PyObject* ret = Py_BuildValue("s", str);
		input_keyboard_string_clear();
		return ret;
	}
}

static PyMethodDef keyboard_methods[] = {
	{ "pressed", python_input_keyboard_pressed, METH_VARARGS, "Returns True if the key with the given scancode is pressed." },
	{ "get", python_input_keyboard_get, METH_NOARGS, "Returns the last pressed scancode." },
	{ "string", python_input_keyboard_string, METH_NOARGS, "Returns the last unicode string the user typed." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef keyboard_module = {
	PyModuleDef_HEAD_INIT,
	"keyboard",
	"keyboard",
	-1,
	keyboard_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

/*
 * Mouse part
 */
static PyObject* python_input_mouse_position(PyObject *self, PyObject *args)
{
	int px, py;

	(void)self;
	(void)args;

	input_mouse_position(&px, &py);

	return Py_BuildValue("ii", px, py);
}

static PyObject* python_input_mouse_pressed(PyObject *self, PyObject *args)
{
	int key;

	if (!PyArg_ParseTuple(args, "i", &key))
	{
		return NULL;
	}

	(void)self;
	(void)args;

	if (input_mouse_pressed(key) != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyMethodDef mouse_methods[] = {
	{ "position", python_input_mouse_position, METH_NOARGS, "Returns the current mouse position." },
	{ "pressed", python_input_mouse_pressed, METH_VARARGS, "Returns True if the given button of the mouse is pressed." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef mouse_module = {
	PyModuleDef_HEAD_INIT,
	"mouse",
	"mouse",
	-1,
	mouse_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

/*
 * Joystick part
 */
typedef struct {
	PyObject_HEAD
	int joystick_nb;
	const char* joystick_name;
} JoystickObject;

static int python_joystick_object_init(JoystickObject *self, PyObject *args)
{
	int index;

	if (!PyArg_ParseTuple(args, "i", &index))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing joystick index argument.");
		return -1;
	}

	self->joystick_nb = index;

	if (self->joystick_nb < 0 || self->joystick_nb >= input_joystick_count())
	{
		// Invalid index.
		PyErr_SetString(PyExc_AttributeError, "Joystick index out of range.");
		return -1;
	}

	return 0;
}

static PyObject* python_input_joystick_button_count(JoystickObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	return Py_BuildValue("i", input_joystick_button_count(self->joystick_nb));
}

static PyObject* python_input_joystick_axis_count(JoystickObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	return Py_BuildValue("i", input_joystick_axis_count(self->joystick_nb));
}

static PyObject* python_input_joystick_button_get(JoystickObject *self, PyObject *args)
{
	int button;

	(void)self;
	(void)args;

	if (!PyArg_ParseTuple(args, "i", &button))
	{
		return NULL;
	}

	if (input_joystick_button_get(self->joystick_nb, button) != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_input_joystick_axis_get(JoystickObject *self, PyObject *args)
{
	int axis;

	(void)self;
	(void)args;

	if (!PyArg_ParseTuple(args, "i", &axis))
	{
		return NULL;
	}

	short value = input_joystick_axis_get(self->joystick_nb, axis);
	return Py_BuildValue("f", (float)value);
}


static PyMethodDef joystick_object_methods[] = {
	{ "button_count", (PyCFunction)python_input_joystick_button_count, METH_NOARGS, "Returns the number of buttons of the given Joystick." },
	{ "button_get", (PyCFunction)python_input_joystick_button_get, METH_VARARGS, "Returns True if the given button is pressed on the Joystick." },
	{ "axis_count", (PyCFunction)python_input_joystick_axis_count, METH_NOARGS, "Returns the number of axis of the given Joystick." },
	{ "axis_get", (PyCFunction)python_input_joystick_axis_get, METH_VARARGS, "Returns the axis value in range [-1, 1]." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject JoystickObjectType = {
	PyObject_HEAD_INIT(NULL)
	"JoystickObject",                      /* tp_name        */
	sizeof(JoystickObject),                /* tp_basicsize   */
	0,                                     /* tp_itemsize    */
	0,                                     /* tp_dealloc     */
	0,                                     /* tp_print       */
	0,                                     /* tp_getattr     */
	0,                                     /* tp_setattr     */
	0,                                     /* tp_compare     */
	0,                                     /* tp_repr        */
	0,                                     /* tp_as_number   */
	0,                                     /* tp_as_sequence */
	0,                                     /* tp_as_mapping  */
	0,                                     /* tp_hash        */
	0,                                     /* tp_call        */
	0,                                     /* tp_str         */
	0,                                     /* tp_getattro    */
	0,                                     /* tp_setattro    */
	0,                                     /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,                    /* tp_flags       */
	"Joystick instance.",                  /* tp_doc         */
	0,	                                   /* tp_traverse       */
	0,		                               /* tp_clear          */
	0,		                               /* tp_richcompare    */
	0,		                               /* tp_weaklistoffset */
	0,		                               /* tp_iter           */
	0,		                               /* tp_iternext       */
	joystick_object_methods,               /* tp_methods        */
	0,                                     /* tp_members        */
	0,		                               /* tp_getset         */
	0,		                               /* tp_base           */
	0,		                               /* tp_dict           */
	0,		                               /* tp_descr_get      */
	0,		                               /* tp_descr_set      */
	0,		                               /* tp_dictoffset     */
	(initproc)python_joystick_object_init, /* tp_init           */
};

static PyObject* python_input_joystick_begin(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (input_joystick_begin() != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_input_joystick_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	input_joystick_finish();
	Py_RETURN_NONE;
}

static PyObject* python_input_joystick_count(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	return Py_BuildValue("i", input_joystick_count());
}

static PyMethodDef joystick_methods[] = {
	{ "begin", python_input_joystick_begin, METH_NOARGS, "Call once to initialize the Joystick module." },
	{ "finish", python_input_joystick_finish, METH_NOARGS, "Call to unitialize the Joystick module." },
	{ "count", python_input_joystick_count, METH_NOARGS, "Returns the number of Joysticks detected." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef joystick_module = {
	PyModuleDef_HEAD_INIT,
	"joystick",
	"joystick",
	-1,
	joystick_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

static PyMethodDef input_methods[] = {
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef input_module = {
	PyModuleDef_HEAD_INIT,
	INPUT_MODULE_NAME,
	"pookoo's input module",
	-1,
	input_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_input(PyObject* module)
{
	PyObject* input = PyModule_Create(&input_module);
	PyModule_AddObject(module, "input", input);

	PyObject* keyboard = PyModule_Create(&keyboard_module);
	PyModule_AddObject(input, "keyboard", keyboard);

	PyObject* mouse = PyModule_Create(&mouse_module);
	PyModule_AddObject(input, "mouse", mouse);

	PyObject* joystick = PyModule_Create(&joystick_module);
	PyModule_AddObject(input, "joystick", joystick);

	/* joystick object */
	JoystickObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&JoystickObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&JoystickObjectType);
	PyModule_AddObject(joystick, "Joystick", (PyObject *)&JoystickObjectType);
}
