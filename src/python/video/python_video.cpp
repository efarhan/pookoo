
#include <python/internal/internal.h>
#include <video/video.h>

typedef struct {
	PyObject_HEAD
	video_t* video;
} VideoObject;

static PyObject* python_video_init(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (video_init())
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_video_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	video_finish();

	Py_RETURN_NONE;
}

static int python_video_open(VideoObject *self, PyObject *args)
{
	char* filename;

	if (!PyArg_ParseTuple(args, "s", &filename))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing filename argument.");
		return -1;
	}

	self->video = video_open(filename);

	if (self->video == NULL)
	{
		// Probably file doesn't exist
		PyErr_SetString(PyExc_AttributeError, "Failed to decode file.");
		return -1;
	}

	return 0;
}

static void python_video_close(VideoObject *self)
{
	if (self->video != NULL)
	{
		video_close(self->video);
	}
}

static PyObject* python_video_next(VideoObject *self, PyObject *args)
{
	uint64_t timestamp;
	int r = video_next(self->video, &timestamp);

	if (r)
	{
		return PyLong_FromLongLong(timestamp);
	}
	else
	{
		Py_RETURN_NONE;
	}
}

static PyObject* python_video_size(VideoObject *self, PyObject *args)
{
	unsigned int width, height;
	video_size(self->video, &width, &height);
	return Py_BuildValue("ii", (int)width, (int)height);
}

static PyMethodDef video_object_methods[] = {
	{ "next", (PyCFunction)python_video_next, METH_NOARGS, "Draw next video image, returns the timestamp of the rendered frame, or None if stream is over." },
	{ "size", (PyCFunction)python_video_size, METH_NOARGS, "Returns the video's image size." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject VideoObjectType = {
	PyObject_HEAD_INIT(NULL)
	"VideoObject",                    /* tp_name        */
	sizeof(VideoObject),              /* tp_basicsize   */
	0,                                /* tp_itemsize    */
	(destructor)python_video_close,   /* tp_dealloc     */
	0,                                /* tp_print       */
	0,                                /* tp_getattr     */
	0,                                /* tp_setattr     */
	0,                                /* tp_compare     */
	0,                                /* tp_repr        */
	0,                                /* tp_as_number   */
	0,                                /* tp_as_sequence */
	0,                                /* tp_as_mapping  */
	0,                                /* tp_hash        */
	0,                                /* tp_call        */
	0,                                /* tp_str         */
	0,                                /* tp_getattro    */
	0,                                /* tp_setattro    */
	0,                                /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,               /* tp_flags       */
	"Video instance.",                /* tp_doc         */
	0,	                              /* tp_traverse       */
	0,		                          /* tp_clear          */
	0,		                          /* tp_richcompare    */
	0,		                          /* tp_weaklistoffset */
	0,		                          /* tp_iter           */
	0,		                          /* tp_iternext       */
	video_object_methods,             /* tp_methods        */
	0,                                /* tp_members        */
	0,		                          /* tp_getset         */
	0,		                          /* tp_base           */
	0,		                          /* tp_dict           */
	0,		                          /* tp_descr_get      */
	0,		                          /* tp_descr_set      */
	0,		                          /* tp_dictoffset     */
	(initproc)python_video_open,      /* tp_init           */
};

static PyMethodDef video_methods[] = {
	{ "begin", (PyCFunction)python_video_init, METH_NOARGS, "Call once to initialize the Video module." },
	{ "finish", (PyCFunction)python_video_finish, METH_NOARGS, "Call once to initialize the Video module." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef video_module = {
	PyModuleDef_HEAD_INIT,
	"video",
	"video",
	-1,
	video_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_video(PyObject* module)
{
	// Video module
	PyObject* video = PyModule_Create(&video_module);
	PyModule_AddObject(module, "video", video);

	// Video object
	VideoObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&VideoObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&VideoObjectType);
	PyModule_AddObject(video, "Video", (PyObject *)&VideoObjectType);
}
