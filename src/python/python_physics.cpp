
#include <python/internal/internal.h>
#include <physics/physics.h>
#include "structmember.h"
#include <list>
#include <log/log.h>

typedef struct {
	PyObject_HEAD
	PyObject* userData1;
	PyObject* userData2;
	bool begin;
} EventObject;

typedef struct {
	PyObject_HEAD
	physics_body_t* body;
	physics_world_t* world;
} BodyObject;

typedef struct {
	PyObject_HEAD
	physics_world_t* world;
} WorldObject;

static int python_physics_world_begin(WorldObject *self, PyObject *args)
{
	float gx;
	float gy;
	int velocity_it;
	int position_it;

	if (!PyArg_ParseTuple(args, "(ff)ii", &gx, &gy, &velocity_it,&position_it))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing argument: (gravity, velocity_it,position_it)");
		return -1;
	}

	self->world = physics_world_begin(gx,gy,velocity_it,position_it);

	return 0;
}

static PyObject* python_physics_world_step(WorldObject *self, PyObject *args)
{
	float dt;

	(void)self;

	if (!PyArg_ParseTuple(args, "f", &dt))
	{
		log_error("Missing time argument.");
		return NULL;
	}

	physics_world_step(self->world, dt);

	Py_RETURN_NONE;
}

static PyObject* python_physics_world_end(WorldObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	physics_world_end(self->world);

	Py_RETURN_NONE;
}

static PyMemberDef event_member[] = {
    {"begin", T_INT, offsetof(EventObject, begin), 0, "begin status"},
	{"userDataA", T_OBJECT_EX, offsetof(EventObject, userData1), 0, "user data a"},
	{"userDataB", T_OBJECT_EX, offsetof(EventObject, userData2), 0, "user data b"},
    {NULL}
};
static PyTypeObject EventObjectType = {
	PyObject_HEAD_INIT(NULL)
	"EventObject",                          /* tp_name        */
	sizeof(EventObject),                    /* tp_basicsize   */
	0,                                     /* tp_itemsize    */
	0,/* tp_dealloc     */
	0,                                     /* tp_print       */
	0,                                     /* tp_getattr     */
	0,                                     /* tp_setattr     */
	0,                                     /* tp_compare     */
	0,                                     /* tp_repr        */
	0,                                     /* tp_as_number   */
	0,                                     /* tp_as_sequence */
	0,                                     /* tp_as_mapping  */
	0,                                     /* tp_hash        */
	0,                                     /* tp_call        */
	0,                                     /* tp_str         */
	0,                                     /* tp_getattro    */
	0,                                     /* tp_setattro    */
	0,                                     /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,                    /* tp_flags       */
	"Physics contact event instance.",              /* tp_doc         */
	0,	                                   /* tp_traverse       */
	0,		                               /* tp_clear          */
	0,		                               /* tp_richcompare    */
	0,		                               /* tp_weaklistoffset */
	0,		                               /* tp_iter           */
	0,		                               /* tp_iternext       */
	0,                  					/* tp_methods        */
	event_member,                           /* tp_members        */
	0,		                               /* tp_getset         */
	0,		                               /* tp_base           */
	0,		                               /* tp_dict           */
	0,		                               /* tp_descr_get      */
	0,		                               /* tp_descr_set      */
	0,		                               /* tp_dictoffset     */
	0,    /* tp_init           */
};

static PyObject* python_physics_events_buffer_get(PyObject* self, PyObject *args)
{
	(void)self;
	(void)args;
	void* buffer = physics_events_buffer_get();
	auto physics_buffer = static_cast<std::list<physics_event_t*>*>(buffer);
	PyObject* events_buffer = PyList_New(0);
	for(auto i = physics_buffer->begin(); i != physics_buffer->end(); i++)
	{
		//Add all the C event in the python events buffer
		PyObject* user_data_a = (PyObject*)((*i)->user_data_a);
		PyObject* user_data_b = (PyObject*)((*i)->user_data_b);

		PyObject* event = PyObject_CallObject((PyObject *) &EventObjectType, NULL);
		PyObject_SetAttrString(event, "userDataA",user_data_a);
		PyObject_SetAttrString(event, "userDataB",user_data_b);
		PyObject* begin = Py_BuildValue("i", (*i)->begin);
		if(begin != NULL)
		{
			PyObject_SetAttrString(event, "begin", begin);
		}
		else
		{
			log_error("begin is NULL");
		}

		PyList_Append(events_buffer,Py_BuildValue("O",event));
	}
	return events_buffer;
}

static PyMethodDef physics_methods[] = {
	{ "get_events", (PyCFunction)python_physics_events_buffer_get, METH_NOARGS, "Returns the list of physics event" },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef physics_module = {
	PyModuleDef_HEAD_INIT,
	PHYSICS_MODULE_NAME,
	"pookoo's physics module",
	-1,
	physics_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

static int python_physics_body_add(BodyObject *self, PyObject *args)
{
	float posx;
	float posy;
	float angle;
	const char* type;
	PyObject* world;

	if (!PyArg_ParseTuple(args, "O(ff)fs", &world, &posx, &posy, &angle, &type))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing argument: (world, pos, angle, 'static'|'dynamic')");
		return -1;
	}

	WorldObject* world_object = (WorldObject*)world;
	self->world = world_object->world;
	
	if (!strcmp(type, "static"))
	{
		self->body = physics_body_add_static(
			self->world,
			posx,
			posy,
			angle);
	}
	else if (!strcmp(type, "dynamic"))
	{
		self->body = physics_body_add_dynamic(
			self->world,
			posx,
			posy,
			angle);
	}
	else
	{
		PyErr_SetString(PyExc_AttributeError, "Body type can only be 'static' or 'dynamic'");
		return -1;
	}

	return 0;
}



static void python_physics_body_remove(BodyObject *self)
{
	physics_body_dealloc(self->body);
}

static PyObject* python_physics_geometry_add_box(BodyObject *self, PyObject *args)
{
	float posx;
	float posy;
	float sizex;
	float sizey;
	float angle;
	int sensor;
	PyObject* user_data;

	if (!PyArg_ParseTuple(args, "(ff)(ff)fpO",
				&posx,
				&posy,
                &sizex,
				&sizey,
                &angle,
                &sensor,
                &user_data))
	{
		log_error("Argument should be: (pos, size, angle, sensor, user_data)");
		return NULL;
	}

	physics_geometry_add_box(
		self->body,
        posx,
		posy,
        sizex,
		sizey,
        angle,
        sensor,
        (void*)user_data);

	Py_RETURN_NONE;
}

static PyObject* python_geometry_add_circle(BodyObject *self, PyObject *args)
{
	float posx;
	float posy;
	float radius;
	float angle;
	int sensor;
	PyObject* user_data;

	if (!PyArg_ParseTuple(args, "(ff)ffpO",
		&posx,
		&posy,
        &radius,
        &angle,
        &sensor,
        &user_data))
	{
		log_error("Argument should be: (pos, radius, angle, sensor, user_data)");
		return NULL;
	}

	physics_geometry_add_circle(
		self->body,
		posx,
		posy,
		radius,
		sensor,
		(void*)user_data);

	Py_RETURN_NONE;
}

static PyObject* python_body_position_get(BodyObject *self, PyObject *args)
{
	(void)args;

	float px, py;

	physics_body_get_position(
		self->body,
		&px,
		&py);

	return Py_BuildValue("ff", px, py);
}
static PyObject* python_physics_body_set_angle(BodyObject *self, PyObject *args)
{

	float angle;


	if (!PyArg_ParseTuple(args, "f",

                &angle
))
	{
		log_error("Argument should be: angle");
		return NULL;
	}

	physics_body_set_angle(
		self->body,
        angle);

	Py_RETURN_NONE;
}
static PyObject* python_body_move(BodyObject *self, PyObject *args)
{
	float vx;
	float vy;
	float dt;
	int type;

	if (!PyArg_ParseTuple(args, "fffi",
		&vx,
		&vy,
		&dt,
		&type))
	{
		log_error("Argument should be: (vx,vy,dt,type)");
		return NULL;
	}

	physics_body_move(
		self->body,
		vx,
		vy,
		dt,
		type);

	Py_RETURN_NONE;
}
static PyObject* python_body_stop(BodyObject *self, PyObject *args)
{
	int vx;
	int vy;
	float dt;
	int type;

	if (!PyArg_ParseTuple(args, "iifi",
		&vx,
		&vy,
		&dt,
		&type))
	{
		log_error("Argument should be: (vx,vy,dt,type)");
		return NULL;
	}

	physics_body_stop(
		self->body,
		vx,
		vy,
		dt,
		type);

	Py_RETURN_NONE;
}
static PyObject* python_body_jump(BodyObject *self, PyObject *args)
{
	float vy;

	if (!PyArg_ParseTuple(args, "f",
		&vy))
	{
		log_error("Argument should be: (vx,vy,dt,type)");
		return NULL;
	}

	physics_body_jump(
		self->body,
		vy);

	Py_RETURN_NONE;
}
static PyObject* python_body_position_set(BodyObject *self, PyObject *args)
{
	float x;
	float y;

	if (!PyArg_ParseTuple(args, "(ff)",
		&x, &y))
	{
		log_error("Argument should be: (xy)");
		return NULL;
	}

	physics_body_set_position(
		self->body,
		x,
		y);

	Py_RETURN_NONE;
}

static PyMethodDef physics_body_methods[] = {
	{ "move", (PyCFunction)python_body_move, METH_VARARGS, "Move the body at velocity given" },
	{ "stop", (PyCFunction)python_body_stop, METH_VARARGS, "Stop the body at 0-velocity given" },
	{ "jump", (PyCFunction)python_body_jump, METH_VARARGS, "Jump" },
	{ "set_position", (PyCFunction)python_body_position_set, METH_VARARGS, "Set body position" },
	{ "get_position", (PyCFunction)python_body_position_get, METH_NOARGS, "Returns the given body's position." },
	{ "add_box", (PyCFunction)python_physics_geometry_add_box, METH_VARARGS, "Creates a box geometry for the given body." },
	{ "set_angle", (PyCFunction)python_physics_body_set_angle, METH_VARARGS, "Set the body angle." },
	{ "add_circle", (PyCFunction)python_geometry_add_circle, METH_VARARGS, "Creates a circle geometry for the given body." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject BodyObjectType = {
	PyObject_HEAD_INIT(NULL)
	"BodyObject",                          /* tp_name        */
	sizeof(BodyObject),                    /* tp_basicsize   */
	0,                                     /* tp_itemsize    */
	(destructor)python_physics_body_remove,/* tp_dealloc     */
	0,                                     /* tp_print       */
	0,                                     /* tp_getattr     */
	0,                                     /* tp_setattr     */
	0,                                     /* tp_compare     */
	0,                                     /* tp_repr        */
	0,                                     /* tp_as_number   */
	0,                                     /* tp_as_sequence */
	0,                                     /* tp_as_mapping  */
	0,                                     /* tp_hash        */
	0,                                     /* tp_call        */
	0,                                     /* tp_str         */
	0,                                     /* tp_getattro    */
	0,                                     /* tp_setattro    */
	0,                                     /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,                    /* tp_flags       */
	"Physics body instance.",              /* tp_doc         */
	0,	                                   /* tp_traverse       */
	0,		                               /* tp_clear          */
	0,		                               /* tp_richcompare    */
	0,		                               /* tp_weaklistoffset */
	0,		                               /* tp_iter           */
	0,		                               /* tp_iternext       */
	physics_body_methods,                  /* tp_methods        */
	0,                                     /* tp_members        */
	0,		                               /* tp_getset         */
	0,		                               /* tp_base           */
	0,		                               /* tp_dict           */
	0,		                               /* tp_descr_get      */
	0,		                               /* tp_descr_set      */
	0,		                               /* tp_dictoffset     */
	(initproc)python_physics_body_add,    /* tp_init           */
};

static PyObject * python_physics_world_add_body(WorldObject *self, PyObject *args)
{
	float posx;
	float posy;
	float angle;
	const char* type;

	if (!PyArg_ParseTuple(args, "(ff)fs", &posx, &posy, &angle, &type))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing argument: (pos, angle,'static'|'dynamic')");
		return NULL;

	}

	PyObject* arg_list = Py_BuildValue("O(ff)fs", (PyObject*)self, posx, posy, angle, type);
	PyObject* body_obj = PyObject_CallObject((PyObject*)&BodyObjectType, arg_list);

	Py_DECREF(arg_list);
	return body_obj;
}

static PyObject* python_physics_world_remove_body(WorldObject *self,PyObject *args)
{
	BodyObject* body_obj;
	if (!PyArg_ParseTuple(args, "O", &body_obj))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing argument: body");
	}
	else
	{
		physics_body_remove(self->world, body_obj->body);
	}
	Py_RETURN_NONE;
}

static PyMethodDef physics_world_methods[] = {
	{ "add_body", (PyCFunction)python_physics_world_add_body, METH_VARARGS, "Creates a body in the given world." },
	{ "remove_body", (PyCFunction)python_physics_world_remove_body, METH_VARARGS, "Remove a body in the given world." },
	{ "step", (PyCFunction)python_physics_world_step, METH_VARARGS, "Update the world with the given delta time"},
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject WorldObjectType = {
	PyObject_HEAD_INIT(NULL)
	"WorldObject",                          /* tp_name        */
	sizeof(WorldObject),                    /* tp_basicsize   */
	0,                                     /* tp_itemsize    */
	(destructor)python_physics_world_end,/* tp_dealloc     */
	0,                                     /* tp_print       */
	0,                                     /* tp_getattr     */
	0,                                     /* tp_setattr     */
	0,                                     /* tp_compare     */
	0,                                     /* tp_repr        */
	0,                                     /* tp_as_number   */
	0,                                     /* tp_as_sequence */
	0,                                     /* tp_as_mapping  */
	0,                                     /* tp_hash        */
	0,                                     /* tp_call        */
	0,                                     /* tp_str         */
	0,                                     /* tp_getattro    */
	0,                                     /* tp_setattro    */
	0,                                     /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,                    /* tp_flags       */
	"Physics world instance.",              /* tp_doc         */
	0,	                                   /* tp_traverse       */
	0,		                               /* tp_clear          */
	0,		                               /* tp_richcompare    */
	0,		                               /* tp_weaklistoffset */
	0,		                               /* tp_iter           */
	0,		                               /* tp_iternext       */
	physics_world_methods,                  /* tp_methods        */
	0,                                     /* tp_members        */
	0,		                               /* tp_getset         */
	0,		                               /* tp_base           */
	0,		                               /* tp_dict           */
	0,		                               /* tp_descr_get      */
	0,		                               /* tp_descr_set      */
	0,		                               /* tp_dictoffset     */
	(initproc)python_physics_world_begin,    /* tp_init           */
};

void python_init_physics(PyObject* module)
{
	// Module
	PyObject* physics = PyModule_Create(&physics_module);
	PyModule_AddObject(module, "physics", physics);

	// Body
	BodyObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&BodyObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&BodyObjectType);
	PyModule_AddObject(physics, "Body", (PyObject *)&BodyObjectType);

	// World
	WorldObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&WorldObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&WorldObjectType);
	PyModule_AddObject(physics, "World", (PyObject *)&WorldObjectType);
	// Contact Event
	EventObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&EventObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&EventObjectType);
	PyModule_AddObject(physics, "Event", (PyObject *)&EventObjectType);
}
