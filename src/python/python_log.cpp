
#include <python/internal/internal.h>
#include <log/log.h>

static PyObject* python_log_info(PyObject *self, PyObject *args)
{
	char* s;

	(void)self;

	if (!PyArg_ParseTuple(args, "s", &s)) 
	{
		return NULL;
	}

	log_info("%s", s);

	Py_RETURN_NONE;
}

static PyObject* python_log_error(PyObject *self, PyObject *args)
{
	char* s;

	(void)self;

	if (!PyArg_ParseTuple(args, "s", &s)) 
	{
		return NULL;
	}

	log_error("%s", s);

	Py_RETURN_NONE;
}

static PyObject* python_log_expect(PyObject *self, PyObject *args)
{
	char* s;

	(void)self;

	if (!PyArg_ParseTuple(args, "s", &s))
	{
		return NULL;
	}

	log_error("%s", s);

	Py_RETURN_NONE;
}

static PyObject* python_log_debug(PyObject *self, PyObject *args)
{
	char* s;

	(void)self;

	if (!PyArg_ParseTuple(args, "s", &s)) 
	{
		return NULL;
	}

	Py_RETURN_NONE;
}

static PyMethodDef log_methods[] = {
	{ "info", (PyCFunction)python_log_info, METH_VARARGS, "Log an informational message." },
	{ "error", (PyCFunction)python_log_error, METH_VARARGS, "Log an error." },
	{ "expect", (PyCFunction)python_log_expect, METH_VARARGS, "Expect a given error message (unit-tests only!)." },
	{ "debug", (PyCFunction)python_log_debug, METH_VARARGS, "Debug message, not displayed in release build." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef log_module = {
	PyModuleDef_HEAD_INIT,
	LOG_MODULE_NAME,
	"pookoo's log module",
	-1,
	log_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_log(PyObject* module)
{
	PyObject* log = PyModule_Create(&log_module);
	PyModule_AddObject(module, "log", log);
}
