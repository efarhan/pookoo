
#include <utils/utils.h>
#include <utils/const.h>

#include <python/internal/internal.h>

static PyObject* python_utils_path(PyObject *self, PyObject *args)
{
	char buffer[PATH_LIMIT];

	(void)self;
	(void)args;

	utils_data_path(buffer, PATH_LIMIT, "");

	return Py_BuildValue("s", buffer);
}

static PyObject* python_utils_mkdir(PyObject *self, PyObject *args)
{
	char* dirname;
	
	(void)self;
	(void)args;

	if (!PyArg_ParseTuple(args, "s", &dirname))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing directory name.");
		return NULL;
	}

	if (utils_makedir(dirname))
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}
	
static PyMethodDef utils_methods[] = {
	{ "path", (PyCFunction)python_utils_path, METH_NOARGS, "Gets the path where to store configuration files." },
	{ "mkdir", (PyCFunction)python_utils_mkdir, METH_VARARGS, "Creates recursively the given directory." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef utils_module = {
	PyModuleDef_HEAD_INIT,
	"utils",
	"utils",
	-1,
	utils_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_utils(PyObject* module)
{
	PyObject* utils = PyModule_Create(&utils_module);
	PyModule_AddObject(module, "utils", utils);
}
