#include <limits>
#include <python/internal/internal.h>
#include <cmath>
#include <draw/draw.h>
#include <log/log.h>
#include <texture/texture.h>

static PyObject* python_draw_begin(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (draw_begin())
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_draw_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	draw_finish();
	Py_RETURN_NONE;
}

static PyObject* python_draw_clear(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	draw_clear();
	Py_RETURN_NONE;
}

static PyObject* python_draw_translate(PyObject *self, PyObject *args)
{
	float x, y;

	(void)self;

	if (!PyArg_ParseTuple(args, "(ff)", &x, &y))
	{
		return NULL;
	}

	draw_translate(x, y);
	Py_RETURN_NONE;
}

static PyObject* python_draw_scale(PyObject *self, PyObject *args)
{
	float x; 
	float y = std::numeric_limits<float>::max();

	(void)self;

	if (!PyArg_ParseTuple(args, "f|f", &x, &y))
	{
		return NULL;
	}

	if (x < 0.0f)
	{
		log_error("draw.scale: Only positive values are supported!");
		return NULL;
	}

	if (y ==  std::numeric_limits<float>::max())
	{
		y = x;
	}

	draw_scale(x, y);

	Py_RETURN_NONE;
}

static PyObject* python_draw_rotate(PyObject *self, PyObject *args)
{
	float a;

	(void)self;

	if (!PyArg_ParseTuple(args, "f", &a))
	{
		return NULL;
	}

	draw_rotate(a);

	Py_RETURN_NONE;
}

static PyObject* python_draw_clip_begin(PyObject *self, PyObject *args)
{
	int x, y, w, h;

	(void)self;

	if (!PyArg_ParseTuple(args, "(ii)(ii)", &x, &y, &w, &h))
	{
		return NULL;
	}

	draw_clip_begin(x, y, w, h);

	Py_RETURN_NONE;
}

static PyObject* python_draw_clip_stop(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	draw_clip_finish();
	Py_RETURN_NONE;
}

static PyObject* python_draw_color(PyObject *self, PyObject *args)
{
	float r, g, b, a = 1.0f;

	(void)self;

	if (!PyArg_ParseTuple(args, "fff|f", &r, &g, &b, &a)) 
	{
		return NULL;
	}

	draw_color_rgba(r, g, b, a);

	Py_RETURN_NONE;
}

static PyObject* python_draw_rectangle(PyObject *self, PyObject *args)
{
	float w, h;

	(void)self;

	if (!PyArg_ParseTuple(args, "(ff)", &w, &h)) 
	{
		return NULL;
	}

	draw_rectangle(w, h);

	Py_RETURN_NONE;
}

static PyObject* python_draw_line(PyObject *self, PyObject *args)
{
	float dx, dy;
	float width = 1.0f;

	(void)self;

	if (!PyArg_ParseTuple(args, "(ff)|f", &dx, &dy, &width))
	{
		return NULL;
	}

	draw_line(dx, dy, width);
	Py_RETURN_NONE;
}

static PyObject* python_draw_circle(PyObject *self, PyObject *args)
{
	float outer_radius, inner_ratio = 0.0f;

	(void)self;

	if (!PyArg_ParseTuple(args, "f|f", &outer_radius, &inner_ratio))
	{
		return NULL;
	}

	draw_circle(outer_radius, inner_ratio);
	Py_RETURN_NONE;
}

static PyObject* python_draw_push(PyObject *self, PyObject *args)
{
	draw_save();
	Py_RETURN_NONE;
}

static PyObject* python_draw_pop(PyObject *self, PyObject *args)
{
	draw_restore();
	Py_RETURN_NONE;
}

static PyObject* python_draw_cursor(PyObject *self, PyObject *args)
{
	int show;

	if (!PyArg_ParseTuple(args, "i", &show))
	{
		return NULL;
	}

	draw_cursor(show);

	Py_RETURN_NONE;
}

static PyObject* python_draw_blend(PyObject* self, PyObject *args)
{
	blend_t blend;

	if (!PyArg_ParseTuple(args, "i", &blend))
	{
		return NULL;
	}

	draw_blend(blend);
	Py_RETURN_NONE;
}

static PyObject* python_draw_blend_normal(PyObject* self, PyObject *args)
{
	draw_blend(BLEND_NORMAL);
	Py_RETURN_NONE;
}

static PyObject* python_draw_blend_masker(PyObject* self, PyObject *args)
{
	draw_blend(BLEND_MASKER);
	Py_RETURN_NONE;
}

static PyObject* python_draw_blend_masked(PyObject* self, PyObject *args)
{
	draw_blend(BLEND_MASKED);
	Py_RETURN_NONE;
}

static PyMethodDef draw_methods[] = {
	// General functions
	{ "begin", (PyCFunction)python_draw_begin, METH_NOARGS, "Initializer." },
	{ "finish", (PyCFunction)python_draw_finish, METH_NOARGS, "Uninitializer." },

	// Geometry functions
	{ "rectangle", (PyCFunction)python_draw_rectangle, METH_VARARGS, "Draw a rectangle of the given size." },
	{ "line", (PyCFunction)python_draw_line, METH_VARARGS, "Draw a line in the given direction." },
	{ "circle", (PyCFunction)python_draw_circle, METH_VARARGS, "Draw a circle of the given radius and inner circle ratio." },

	// State functions
	{ "clear", (PyCFunction)python_draw_clear, METH_NOARGS, "To be called at the beginning of each frame." },
	{ "scale", (PyCFunction)python_draw_scale, METH_VARARGS, "Scale by a given factor next draw operations." },
	{ "move", (PyCFunction)python_draw_translate, METH_VARARGS, "Translate by a given value next draw operations." },
	{ "rotate", (PyCFunction)python_draw_rotate, METH_VARARGS, "Rotate by a given angle next draw operations." },
	{ "color", (PyCFunction)python_draw_color, METH_VARARGS, "Set drawing color to the given red-green-blue(-alpha) value." },
	{ "push", (PyCFunction)python_draw_push, METH_NOARGS, "Save the current state." },
	{ "pop", (PyCFunction)python_draw_pop, METH_NOARGS, "Restore the previous state." },

	{ "cursor", (PyCFunction)python_draw_cursor, METH_VARARGS, "Tell if you want to display the cursor or not." },

	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef draw_module = {
	PyModuleDef_HEAD_INIT,
	"draw",
	"draw",
	-1,
	draw_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

static PyMethodDef clip_methods[] = {
	{ "begin", (PyCFunction)python_draw_clip_begin, METH_VARARGS, "Clip in the given region." },
	{ "stop", (PyCFunction)python_draw_clip_stop, METH_VARARGS, "Stop clipping." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef clip_module = {
	PyModuleDef_HEAD_INIT,
	"clip",
	"clip",
	-1,
	clip_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

static PyMethodDef blend_methods[] = {
	{ "normal", (PyCFunction)python_draw_blend_normal, METH_NOARGS, "Normal blending." },
	{ "masker", (PyCFunction)python_draw_blend_masker, METH_NOARGS, "The mask for the image called with 'masked'." },
	{ "masked", (PyCFunction)python_draw_blend_masked, METH_NOARGS, "The masked image" },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef blend_module = {
	PyModuleDef_HEAD_INIT,
	"blend",
	"blend",
	-1,
	blend_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_draw(PyObject* module)
{
	PyObject* draw = PyModule_Create(&draw_module);
	PyModule_AddObject(module, "draw", draw);

	PyObject* clip = PyModule_Create(&clip_module);
	PyModule_AddObject(draw, "clip", clip);

	PyObject* blend = PyModule_Create(&blend_module);
	PyModule_AddObject(draw, "blend", blend);
}
