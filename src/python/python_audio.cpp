
#include <python/python.h>
#include <python/internal/internal.h>

#include <audio/audio.h>
#include <log/log.h>

typedef struct {
	PyObject_HEAD
	audio_stream_t* audio;
} AudioStreamObject;

typedef struct {
	PyObject_HEAD
	audio_sound_t* audio;
} AudioSoundObject;

static PyObject* python_audio_init(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (audio_begin() != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_audio_step(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	audio_step();
	Py_RETURN_NONE;
}

static PyObject* python_audio_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	audio_finish();
	Py_RETURN_NONE;
}

static PyObject* python_audio_pitch(PyObject *self, PyObject *args)
{
	float pitch;

	(void)self;

	if (!PyArg_ParseTuple(args, "f", &pitch))
	{
		return NULL;
	}

	audio_pitch(pitch);
	Py_RETURN_NONE;
}

static PyObject* python_audio_tempo(PyObject *self, PyObject *args)
{
	float tempo;

	(void)self;

	if (!PyArg_ParseTuple(args, "f", &tempo))
	{
		return NULL;
	}

	audio_tempo(tempo);
	Py_RETURN_NONE;
}

static PyObject* python_audio_volume(PyObject *self, PyObject *args)
{
	float volume;

	(void)self;

	if (!PyArg_ParseTuple(args, "f", &volume))
	{
		return NULL;
	}

	audio_volume(volume);
	Py_RETURN_NONE;
}

static PyObject* python_audio_stream_clear(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	audio_stream_clear();
	Py_RETURN_NONE;
}

static PyObject* python_audio_capture_begin(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	audio_capture_begin();
	Py_RETURN_NONE;
}

static PyObject* python_audio_capture_read(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	#define BUFFSIZE 2048
	char buffer[BUFFSIZE];
	size_t r = audio_capture_read(buffer, BUFFSIZE);

	return Py_BuildValue("s#", buffer, r);
}

static PyObject* python_audio_capture_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	audio_capture_begin();
	Py_RETURN_NONE;
}

static PyMethodDef audio_methods[] = {
	{ "begin", (PyCFunction)python_audio_init, METH_NOARGS, "Call once to initialize the Audio module." },
	{ "step", (PyCFunction)python_audio_step, METH_NOARGS, "Call between each frame to read the next part of queued sounds." },
	{ "finish", (PyCFunction)python_audio_finish, METH_NOARGS, "Call once to initialize the Audio module." },
	{ "volume", (PyCFunction)python_audio_volume, METH_VARARGS, "Change global sound volume, between 0.0 and 1.0." },
	{ "pitch", (PyCFunction)python_audio_pitch, METH_VARARGS, "Change stream's pitch" },
	{ "tempo", (PyCFunction)python_audio_tempo, METH_VARARGS, "Change stream's tempo" },
	{ "clear", (PyCFunction)python_audio_stream_clear, METH_NOARGS, "Unload all queued music." },
	{ "capture_begin", (PyCFunction)python_audio_capture_begin, METH_NOARGS, "Begin microphone capture." },
	{ "capture_read", (PyCFunction)python_audio_capture_read, METH_VARARGS, "Read microphone input." },
	{ "capture_finish", (PyCFunction)python_audio_capture_finish, METH_NOARGS, "End microphone capture." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef audio_module = {
	PyModuleDef_HEAD_INIT,
	"audio",
	"audio",
	-1,
	audio_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

static int python_audio_stream_init(AudioStreamObject *self, PyObject *args)
{
	log_info("Python Audio Stream init\n");
	char* s;

	(void)self;

	if (!PyArg_ParseTuple(args, "s", &s))
	{
		return NULL;
	}

	self->audio = audio_stream_load(s);

	if (self->audio == NULL)
	{
		// File not found :(
		PyErr_SetString(PyExc_AttributeError, "Stream file not found.");
		return -1;
	}

	return 0;
}

static void python_audio_stream_unload(AudioStreamObject *self)
{
	audio_stream_unload(self->audio);
}

static PyObject* python_audio_stream_status(AudioStreamObject *self, PyObject* args)
{
	int status = audio_stream_status(self->audio);

	if (status == PLAYING)
	{
		return Py_BuildValue("s", "playing");
	}
	else if (status == PENDING)
	{
		return Py_BuildValue("s", "pending");
	} 
	else if (status == STOPPED)
	{
		return Py_BuildValue("s", "stopped");
	}

	// This shouldn't happen!
	Py_RETURN_NONE;
}

static PyObject* python_audio_stream_volume_set(AudioStreamObject *self, PyObject *args)
{

	float v;

	if (!PyArg_ParseTuple(args, "f", &v))
	{
		log_error("Argument should be: volume");

		return NULL;
	}

	audio_stream_volume_set(self->audio, v);

	Py_RETURN_NONE;
}
static PyObject* python_audio_stream_volume_get(AudioStreamObject *self, PyObject* args)
{
	(void)self;
	(void)args;
	return Py_BuildValue("f", audio_stream_volume_get(self->audio));

}

static PyMethodDef audio_stream_methods[] = {
	{ "status", (PyCFunction)python_audio_stream_status, METH_NOARGS, "Get the stream status, 'playing, 'pending' or 'stopped'" },
	{ "volume_set",(PyCFunction)python_audio_stream_volume_set, METH_VARARGS, "Set the volume"},
	{ "volume_get",(PyCFunction)python_audio_stream_volume_get, METH_NOARGS, "Get the volume"},
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject AudioStreamObjectType = {
	PyObject_HEAD_INIT(NULL)
	"AudioStreamObject",                    /* tp_name        */
	sizeof(AudioStreamObject),              /* tp_basicsize   */
	0,                                      /* tp_itemsize    */
	(destructor)python_audio_stream_unload, /* tp_dealloc     */
	0,                                      /* tp_print       */
	0,                                      /* tp_getattr     */
	0,                                      /* tp_setattr     */
	0,                                      /* tp_compare     */
	0,                                      /* tp_repr        */
	0,                                      /* tp_as_number   */
	0,                                      /* tp_as_sequence */
	0,                                      /* tp_as_mapping  */
	0,                                      /* tp_hash        */
	0,                                      /* tp_call        */
	0,                                      /* tp_str         */
	0,                                      /* tp_getattro    */
	0,                                      /* tp_setattro    */
	0,                                      /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,                     /* tp_flags       */
	"Stream instance.",                     /* tp_doc         */
	0,	                                    /* tp_traverse       */
	0,		                                /* tp_clear          */
	0,		                                /* tp_richcompare    */
	0,		                                /* tp_weaklistoffset */
	0,		                                /* tp_iter           */
	0,		                                /* tp_iternext       */
	audio_stream_methods,                   /* tp_methods        */
	0,                                      /* tp_members        */
	0,		                                /* tp_getset         */
	0,		                                /* tp_base           */
	0,		                                /* tp_dict           */
	0,		                                /* tp_descr_get      */
	0,		                                /* tp_descr_set      */
	0,		                                /* tp_dictoffset     */
	(initproc)python_audio_stream_init,     /* tp_init           */
};

static int python_audio_sound_init(AudioSoundObject *self, PyObject *args, PyObject *kwds)
{
	char* s;

	(void)kwds;

	if (!PyArg_ParseTuple(args, "s", &s))
	{
		return NULL;
	}

	self->audio = audio_sound_load(s);

	if (self->audio != NULL)
	{
		return 0;
	}
	else
	{
		// File not found :(
		PyErr_SetString(PyExc_AttributeError, "Sound file not found.");
		return -1;
	}
}

static PyObject* python_audio_sound_play(AudioSoundObject *self, PyObject *args)
{
	audio_sound_play(self->audio);
	Py_RETURN_NONE;
}


static void python_audio_sound_unload(AudioSoundObject *self)
{
	audio_sound_unload(self->audio);
}

static PyMethodDef audio_sound_methods[] = {
	{ "play", (PyCFunction)python_audio_sound_play, METH_VARARGS, "Play the given sound effect." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject AudioSoundObjectType = {
	PyObject_HEAD_INIT(NULL)
	"AudioSoundObject",                    /* tp_name        */
	sizeof(AudioSoundObject),              /* tp_basicsize   */
	0,                                     /* tp_itemsize    */
	(destructor)python_audio_sound_unload, /* tp_dealloc     */
	0,                                     /* tp_print       */
	0,                                     /* tp_getattr     */
	0,                                     /* tp_setattr     */
	0,                                     /* tp_compare     */
	0,                                     /* tp_repr        */
	0,                                     /* tp_as_number   */
	0,                                     /* tp_as_sequence */
	0,                                     /* tp_as_mapping  */
	0,                                     /* tp_hash        */
	0,                                     /* tp_call        */
	0,                                     /* tp_str         */
	0,                                     /* tp_getattro    */
	0,                                     /* tp_setattro    */
	0,                                     /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,                    /* tp_flags       */
	"Sound instance.",                     /* tp_doc         */
	0,	                                   /* tp_traverse       */
	0,		                               /* tp_clear          */
	0,		                               /* tp_richcompare    */
	0,		                               /* tp_weaklistoffset */
	0,		                               /* tp_iter           */
	0,		                               /* tp_iternext       */
	audio_sound_methods,                   /* tp_methods        */
	0,                                     /* tp_members        */
	0,		                               /* tp_getset         */
	0,		                               /* tp_base           */
	0,		                               /* tp_dict           */
	0,		                               /* tp_descr_get      */
	0,		                               /* tp_descr_set      */
	0,		                               /* tp_dictoffset     */
	(initproc)python_audio_sound_init,     /* tp_init           */
};

void python_init_audio(PyObject* module)
{
	/* Audio */
	PyObject* audio = PyModule_Create(&audio_module);
	PyModule_AddObject(module, "audio", audio);

	/* Stream */
	AudioStreamObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&AudioStreamObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&AudioStreamObjectType);
	PyModule_AddObject(audio, "Stream", (PyObject *)&AudioStreamObjectType);

	/* Sound */
	AudioSoundObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&AudioSoundObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&AudioSoundObjectType);
	PyModule_AddObject(audio, "Sound", (PyObject *)&AudioSoundObjectType);
}
