
#include <python/python.h>
#include <python/internal/internal.h>
#include <log/log.h>
#include <utils/const.h>
#include <utils/utils.h>
#include <sstream>

#include <stdio.h>

extern void python_init_window(PyObject*);
extern void python_init_log(PyObject*);
extern void python_init_texture(PyObject*);
extern void python_init_font(PyObject*);
extern void python_init_audio(PyObject*);
extern void python_init_draw(PyObject*);
extern void python_init_physics(PyObject*);
extern void python_init_input(PyObject*);
extern void python_init_sync(PyObject*);
#ifdef COMPILE_VIDEO
extern void python_init_video(PyObject*);
#endif
extern void python_init_utils(PyObject*);
#ifdef COMPILE_CHAT
extern void python_init_chat(PyObject*);
#endif
static PyMethodDef pookoo_methods[] = {
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef pookoo_module = {
	PyModuleDef_HEAD_INIT,
	"pookoo",
	"pookoo framework",
	-1,
	pookoo_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

static PyObject* python_init_pookoo(void)
{
	PyObject* pookoo = PyModule_Create(&pookoo_module);

	python_init_window(pookoo);
	python_init_log(pookoo);
	python_init_texture(pookoo);
	python_init_font(pookoo);
	python_init_audio(pookoo);
	python_init_draw(pookoo);
	python_init_physics(pookoo);
	python_init_input(pookoo);
	python_init_sync(pookoo);
#ifdef COMPILE_VIDEO
	python_init_video(pookoo);
#endif
	python_init_utils(pookoo);
#ifdef COMPILE_CHAT
	python_init_chat(pookoo);
#endif
	return pookoo;
}

void python_init(const char* script_path)
{
	PyImport_AppendInittab("pookoo", &python_init_pookoo);
	
	Py_Initialize();

	// Make sure we are able to run the python files.
	PyRun_SimpleString("import sys");


	std::ostringstream oss;
	oss << "sys.path.insert(1, '" << script_path << "')";

	PyRun_SimpleString(oss.str().c_str());

}

int python_runfile(const char* filename)
{

	PyObject* dict = PyDict_New();

	if (dict == NULL)
	{
		return 0;
	}

	PyDict_SetItemString(dict, "__builtins__", PyEval_GetBuiltins());
	
	//utils_snprintf(buffer, PATH_LIMIT, "import %s", filename);

	// We need to use PyRun_String() and not PyRun_SimpleString(), because
	// otherwise an exit() will exit the executable, 
	// but we want to catch this event and exit ourselves.
	std::ostringstream oss;
	oss << "import " << filename;

	PyRun_String(oss.str().c_str(), Py_file_input, dict, dict);

	if (PyErr_Occurred())
	{
		log_error("Failed to open file '%s'", filename);

		// Handle system exit
		if (PyErr_ExceptionMatches(PyExc_SystemExit))
		{
			log_info("Script exited.");
			PyErr_Clear();
		}
		else
		{
			PyErr_Print();
		}

		return 0;
	}

	return 1;
}

void python_exit()
{
	Py_Finalize();

#if defined(_WIN32)
	system("pause");
#endif
}

