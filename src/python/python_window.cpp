
#include <python/internal/internal.h>
#include <window/window.h>

static PyObject* python_window_begin(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (window_begin() != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_window_step(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (window_step() != 0)
	{
		Py_RETURN_TRUE;
	}
	else
	{
		Py_RETURN_FALSE;
	}
}

static PyObject* python_window_size(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	return Py_BuildValue("ii", (int)window_width(), (int)window_height());
}

static PyObject* python_window_is_fullscreen(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	if (window_is_fullscreen())
		Py_RETURN_TRUE;
	else
		Py_RETURN_FALSE;
}

static PyObject* python_window_toggle_fullscreen(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	window_toggle_fullscreen();
	Py_RETURN_NONE;
}

static PyObject* python_window_finish(PyObject *self, PyObject *args)
{
	(void)self;
	(void)args;

	window_finish();
	Py_RETURN_NONE;
}

static PyMethodDef window_methods[] = {
	{ "begin", (PyCFunction)python_window_begin, METH_NOARGS, "Opens the GUI. To be called only once." },
	{ "step", (PyCFunction)python_window_step, METH_NOARGS, "Call between each frame. Among other things, it will swap the screen buffers." },
	{ "size", (PyCFunction)python_window_size, METH_NOARGS, "Returns the current windows width and height." },
	{ "finish", (PyCFunction)python_window_finish, METH_NOARGS, "Closes the GUI. To be called once when closing the application." },
	{ "toggle_fullscreen", (PyCFunction)python_window_toggle_fullscreen, METH_NOARGS, "Toggle the fullscreen argument" },
	{ "is_fullscreen", (PyCFunction)python_window_is_fullscreen, METH_NOARGS, "Check fullscreen state" },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef window_module = {
	PyModuleDef_HEAD_INIT,
	"window",
	"window",
	-1,
	window_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_window(PyObject* module)
{
	PyObject* window = PyModule_Create(&window_module);
	PyModule_AddObject(module, "window", window);
}
