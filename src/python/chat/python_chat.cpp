
#include <python/internal/internal.h>

#include <chat/chat.h>
#include <log/log.h>

typedef struct {
	PyObject_HEAD
	chat_t* chat;
} ChatObject;

static PyObject* python_chat_begin(PyObject* self, PyObject* args)
{
	(void)self;
	(void)args;

	if (!chat_begin())
	{
		log_error("Initializing chat failed :(");
		Py_RETURN_FALSE;
	}
	else
	{
		Py_RETURN_TRUE;
	}
}

static PyObject* python_chat_finish(PyObject* self, PyObject* args)
{
	(void)self;
	(void)args;

	chat_finish();

	Py_RETURN_NONE;
}

static int python_chat_init(ChatObject *self, PyObject *args)
{
	char* jid, *password;

	if (!PyArg_ParseTuple(args, "ss", &jid, &password))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing jid and password argument.");
		return -1;
	}

	self->chat = chat_open(jid, password);

	if (self->chat == NULL)
	{
		PyErr_SetString(PyExc_AttributeError, "Failed to connect.");
		return -1;
	}

	return 0;
}

static void python_chat_close(ChatObject *self)
{
	chat_close(self->chat);
}

static PyObject* python_chat_step(ChatObject *self, PyObject *args)
{
	chat_step(self->chat);
	Py_RETURN_NONE;
}

static PyObject* python_chat_receive(ChatObject *self, PyObject *args)
{
	const char* from, *message;

	if (!chat_receive(self->chat, &from, &message))
	{
		Py_RETURN_NONE;
	}

	// Return a tuple with 'from' and 'message'.
	PyObject* ret = Py_BuildValue("ss", from, message);

	free((void*)from);
	free((void*)message);

	return ret;
}

static PyObject* python_chat_direct(ChatObject *self, PyObject *args)
{
	char* to, *message;

	if (!PyArg_ParseTuple(args, "ss", &to, &message))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing destinator and message argument.");
		return NULL;
	}

	chat_direct(self->chat, to, "%s", message);

	Py_RETURN_NONE;
}

static PyObject* python_chat_group(ChatObject *self, PyObject *args)
{
	char* to, *message;

	if (!PyArg_ParseTuple(args, "ss", &to, &message))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing destinator and message argument.");
		return NULL;
	}

	chat_group(self->chat, to, "%s", message);

	Py_RETURN_NONE;
}

static PyObject* python_chat_presence(ChatObject *self, PyObject *args)
{
	char* to;

	if (!PyArg_ParseTuple(args, "s", &to))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing destinator argument.");
		return NULL;
	}

	chat_presence(self->chat, to);

	Py_RETURN_NONE;
}

static PyObject* python_chat_contact_added(ChatObject *self, PyObject *args)
{
	const char* added = chat_contact_added(self->chat);

	if (added != NULL)
	{
		PyObject* ret = Py_BuildValue("s", added);
		free((void*)added);
		return ret;
	}
	else
	{
		Py_RETURN_NONE;
	}
}

static PyObject* python_chat_contact_deleted(ChatObject *self, PyObject *args)
{
	const char* deleted = chat_contact_deleted(self->chat);

	if (deleted != NULL)
	{
		PyObject* ret = Py_BuildValue("s", deleted);
		free((void*)deleted);
		return ret;
	}
	else
	{
		Py_RETURN_NONE;
	}
}

static PyMethodDef chat_object_methods[] = {
	{ "step", (PyCFunction)python_chat_step, METH_NOARGS, "To be called to fetch the messages since last call." },
	{ "receive", (PyCFunction)python_chat_receive, METH_NOARGS, "To be called to get one of the messages in the queue." },
	{ "direct", (PyCFunction)python_chat_direct, METH_VARARGS, "Send a direct message to the given destinator." },
	{ "group", (PyCFunction)python_chat_group, METH_VARARGS, "Send a message to the group." },
	{ "contact_added", (PyCFunction)python_chat_contact_added, METH_NOARGS, "Get the next added contact, or None if all have been listed." },
	{ "contact_deleted", (PyCFunction)python_chat_contact_deleted, METH_VARARGS, "Get the next deleted contact, or None if all have been listed." },
	{ "presence", (PyCFunction)python_chat_presence, METH_VARARGS, "Send a presence message." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject ChatObjectType = {
	PyObject_HEAD_INIT(NULL)
	"ChatObject",                     /* tp_name        */
	sizeof(ChatObject),               /* tp_basicsize   */
	0,                                /* tp_itemsize    */
	(destructor)python_chat_close,    /* tp_dealloc     */
	0,                                /* tp_print       */
	0,                                /* tp_getattr     */
	0,                                /* tp_setattr     */
	0,                                /* tp_compare     */
	0,                                /* tp_repr        */
	0,                                /* tp_as_number   */
	0,                                /* tp_as_sequence */
	0,                                /* tp_as_mapping  */
	0,                                /* tp_hash        */
	0,                                /* tp_call        */
	0,                                /* tp_str         */
	0,                                /* tp_getattro    */
	0,                                /* tp_setattro    */
	0,                                /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,               /* tp_flags       */
	"Chat instance.",                 /* tp_doc         */
	0,	                              /* tp_traverse       */
	0,		                          /* tp_clear          */
	0,		                          /* tp_richcompare    */
	0,		                          /* tp_weaklistoffset */
	0,		                          /* tp_iter           */
	0,		                          /* tp_iternext       */
	chat_object_methods,              /* tp_methods        */
	0,                                /* tp_members        */
	0,		                          /* tp_getset         */
	0,		                          /* tp_base           */
	0,		                          /* tp_dict           */
	0,		                          /* tp_descr_get      */
	0,		                          /* tp_descr_set      */
	0,		                          /* tp_dictoffset     */
	(initproc)python_chat_init,       /* tp_init           */
};

static PyMethodDef chat_methods[] = {
	{ "begin", (PyCFunction)python_chat_begin, METH_NOARGS, "Init chat module." },
	{ "finish", (PyCFunction)python_chat_finish, METH_NOARGS, "Uninit chat module." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef chat_module = {
	PyModuleDef_HEAD_INIT,
	"chat",
	"chat",
	-1,
	chat_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_chat(PyObject* module)
{
	PyObject* chat = PyModule_Create(&chat_module);
	PyModule_AddObject(module, "chat", chat);

	ChatObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&ChatObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&ChatObjectType);
	PyModule_AddObject(chat, "Chat", (PyObject *)&ChatObjectType);
}
