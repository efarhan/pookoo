
#include <python/internal/internal.h>

#include <sync/sync.h>
#include <log/log.h>

typedef struct {
	PyObject_HEAD
	sync_t* sync;
} SyncObject;

static PyObject* python_sync_begin(PyObject* self, PyObject* args)
{
	log_info("Initializing sync");
	(void)self;
	(void)args;

	if (!sync_begin())
	{
		log_error("Initializing sync failed :(");
		Py_RETURN_FALSE;
	}
	else
	{
		Py_RETURN_TRUE;
	}
}

static PyObject* python_sync_finish(PyObject* self, PyObject* args)
{
	(void)self;
	(void)args;

	sync_finish();

	Py_RETURN_NONE;
}

static int python_sync_init(SyncObject *self, PyObject *args)
{
	int framerate;

	if (!PyArg_ParseTuple(args, "i", &framerate))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing framerate argument.");
		return -1;
	}

	self->sync = sync_open(framerate);

	if (self->sync == NULL)
	{
		// Bad error :(
		PyErr_SetString(PyExc_AttributeError, "Sync failed to open.");
		return -1;
	}

	return 0;
}

static void python_sync_close(SyncObject *self)
{
	sync_close(self->sync);
}

static PyObject* python_sync_step(SyncObject *self, PyObject *args)
{
	sync_step(self->sync);
	Py_RETURN_NONE;
}

static PyObject* python_sync_delta(SyncObject *self, PyObject *args)
{
	float delta = sync_delta(self->sync);
	return Py_BuildValue("f", delta);
}

static PyObject* python_sync_set(SyncObject* self, PyObject* args)
{
	PY_LONG_LONG t;

	if (!PyArg_ParseTuple(args, "L", &t))
	{
		PyErr_SetString(PyExc_AttributeError, "Missing time argument.");
		return NULL;
	}

	sync_set(self->sync, (int)t);

	Py_RETURN_NONE;
}

static PyMethodDef sync_object_methods[] = {
	{ "step", (PyCFunction)python_sync_step, METH_NOARGS, "To be called to sync the the given framerate." },
	{ "dt", (PyCFunction)python_sync_delta, METH_NOARGS, "Get the time since the last frame." },
	{ "set", (PyCFunction)python_sync_set, METH_VARARGS, "Set wait time in nanoseconds." },
	{ NULL, NULL, 0, NULL },
};

static PyTypeObject SyncObjectType = {
	PyObject_HEAD_INIT(NULL)
	"SyncObject",                     /* tp_name        */
	sizeof(SyncObject),               /* tp_basicsize   */
	0,                                /* tp_itemsize    */
	(destructor)python_sync_close,    /* tp_dealloc     */
	0,                                /* tp_print       */
	0,                                /* tp_getattr     */
	0,                                /* tp_setattr     */
	0,                                /* tp_compare     */
	0,                                /* tp_repr        */
	0,                                /* tp_as_number   */
	0,                                /* tp_as_sequence */
	0,                                /* tp_as_mapping  */
	0,                                /* tp_hash        */
	0,                                /* tp_call        */
	0,                                /* tp_str         */
	0,                                /* tp_getattro    */
	0,                                /* tp_setattro    */
	0,                                /* tp_as_buffer   */
	Py_TPFLAGS_DEFAULT,               /* tp_flags       */
	"Sync instance.",                 /* tp_doc         */
	0,	                              /* tp_traverse       */
	0,		                          /* tp_clear          */
	0,		                          /* tp_richcompare    */
	0,		                          /* tp_weaklistoffset */
	0,		                          /* tp_iter           */
	0,		                          /* tp_iternext       */
	sync_object_methods,              /* tp_methods        */
	0,                                /* tp_members        */
	0,		                          /* tp_getset         */
	0,		                          /* tp_base           */
	0,		                          /* tp_dict           */
	0,		                          /* tp_descr_get      */
	0,		                          /* tp_descr_set      */
	0,		                          /* tp_dictoffset     */
	(initproc)python_sync_init,       /* tp_init           */
};

static PyMethodDef sync_methods[] = {
	{ "begin", (PyCFunction)python_sync_begin, METH_NOARGS, "Init sync module." },
	{ "finish", (PyCFunction)python_sync_finish, METH_NOARGS, "Uninit module." },
	{ NULL, NULL, 0, NULL },
};

static struct PyModuleDef sync_module = {
	PyModuleDef_HEAD_INIT,
	"sync",
	"sync",
	-1,
	sync_methods,
	NULL,
	NULL,
	NULL,
	NULL
};

void python_init_sync(PyObject* module)
{
	PyObject* sync = PyModule_Create(&sync_module);
	PyModule_AddObject(module, "sync", sync);

	SyncObjectType.tp_new = PyType_GenericNew;

	if (PyType_Ready(&SyncObjectType) < 0)
	{
		return;
	}

	Py_INCREF(&SyncObjectType);
	PyModule_AddObject(sync, "Sync", (PyObject *)&SyncObjectType);
}
