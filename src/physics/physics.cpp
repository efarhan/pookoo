/* Implement physics function for the tinypy interpretor
 * Dependencies Box2D
 * b2physics.cpp
 *
 *  Created on: Oct 3, 2013
 *      Author: efarhan
 */

#include <physics/physics.h>
#include <utils/const.h>
#include <log/log.h>

#include <Box2D/Box2D.h>
#include <stdlib.h>
#include <list>

struct physics_world_t
{
	b2World* world;
	int32 velocity_it;
	int32 positition_it;
	
};

struct physics_body_t
{
	b2Body* body;
};


std::list<physics_event_t*> physics_events_buffer;

class PookooContactListener: public b2ContactListener
{
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
};

class PookooRayCastCallBack: public b2RayCastCallback
{
public:
	PookooRayCastCallBack() : hit(true), fraction(1.0f), fixture(NULL){};
	~PookooRayCastCallBack() {};

	bool hit;
	b2Fixture* fixture;
	b2Vec2 point;
	b2Vec2 normal;
	float32 fraction;

private:
	float32 ReportFixture(
		b2Fixture* fixture,
		const b2Vec2& point,
		const b2Vec2& normal,
		float32 fraction);
};



void physics_events_buffer_clear(bool remove = false)
{

	for(auto i = physics_events_buffer.begin(); i != physics_events_buffer.end(); i++)
	{
		auto event = *i;
		if(remove && event != NULL)
		{
			free(event);
		}
		else if(remove && event == NULL)
		{
			break;
		}
	}
	physics_events_buffer.clear();
}
void physics_events_buffer_push(struct physics_event_t* event)
{
	physics_events_buffer.push_back(event);
}

extern physics_world_t* physics_world_begin(
	float gx,
	float gy,
	int velocity_it,
	int position_it)
{

	physics_world_t* world;

	if ((world = (physics_world_t*)malloc(sizeof(physics_world_t))) == NULL)
	{
		return NULL;
	}

	world->velocity_it = (int32)velocity_it;
	world->positition_it = (int32)position_it;
	world->world = new b2World(b2Vec2(gx, gy));
	world->world->SetContactListener(new PookooContactListener());
	return world;
}

extern void physics_world_step(
	physics_world_t* world,
	float dt)
{
	physics_events_buffer_clear(true);
	//log_info("Is empty %i", physics_events_buffer.empty());
	world->world->Step(dt, world->velocity_it, world->positition_it);
	world->world->ClearForces();
}

extern void physics_world_end(physics_world_t* world)
{

	if (world != NULL)
	{
		
		/*for(int i = 0; i < world->world->GetBodyCount(); i++){
			world->world->DestroyBody(world->world->GetBodyList()+i);
		}*/
		printf("Remove world: %p %p\n", world, world->world);
		printf("World bodycount: %i\n", world->world->GetBodyCount());
		fflush(stdout);

		delete world->world;
		world->world = NULL;
		
		fflush(stdout);
		free(world);
		world = NULL;
	}
}

/*
* posx, posy defines the center of the newly created body
* in meter
*/
physics_body_t* physics_body_add_dynamic(
	physics_world_t* world,
	float px,
	float py,
	float angle)
{
	physics_body_t* body;

	if ((body = (physics_body_t*)malloc(sizeof(physics_body_t))) == NULL)
	{
		return NULL;
	}

	b2BodyDef body_def = b2BodyDef();
	body_def.position = b2Vec2(px, py);
	body_def.angle = angle;
	body_def.fixedRotation = true;
	body_def.type = b2_dynamicBody;

	body->body = world->world->CreateBody(&body_def);
	return body;
}

/*
* posx, posy defines the center of the newly created body
* in meter
*/
physics_body_t* physics_body_add_static(
	physics_world_t* world,
	float px,
	float py,
	float angle)
{
	physics_body_t* body;

	if ((body = (physics_body_t*)malloc(sizeof(physics_body_t))) == NULL)
	{
		return NULL;
	}

	b2BodyDef body_def = b2BodyDef();
	body_def.position = b2Vec2(px, py);
	body_def.angle = angle;
	body_def.fixedRotation = true;
	body_def.type = b2_staticBody;

	body->body = world->world->CreateBody(&body_def);
	return body;
}

void physics_body_get_position(
	physics_body_t* body,
	float* px,
	float* py)
{
	b2Vec2 position = body->body->GetPosition();

	if (px)
	{
		*px = position.x;
	}

	if (py)
	{
		*py = position.y;
	}
}
void physics_body_set_position(
	physics_body_t* body,
	float x,
	float y)
{
	body->body->SetTransform(b2Vec2(x,y), body->body->GetAngle());
}
void physics_body_set_angle(
	physics_body_t* body,
	float angle)
{
	body->body->SetTransform( body->body->GetPosition(), angle );
	body->body->SetAngularVelocity(0);
}
void physics_body_move(
		physics_body_t* body,
		float vx,
		float vy,
		float dt,
		int type)
{
	b2Vec2 velocity = body->body->GetLinearVelocity();
	printf("MOVE %f %f\n",vx,vy);
	if (vx != 0.0f)
	{
		if(type == 0)
		{
			body->body->SetLinearVelocity(b2Vec2(vx,velocity.y));
		}
		else if (type == 1)
		{

			float desiredVel = vx;

			float velChange = desiredVel - velocity.x;
			float force = body->body->GetMass() * velChange / dt; //f = mv/t
			body->body->ApplyForce( b2Vec2(force,0), body->body->GetWorldCenter(), true );
		}

	}

	if (vy != 0.0f)
	{

		if(type == 0)
		{
			body->body->SetLinearVelocity(b2Vec2(velocity.x,vy));
		}
		else if (type==1)
		{
			float desiredVel = vy;

			float velChange = desiredVel - velocity.y;
			float force = body->body->GetMass() * velChange / dt; //f = mv/t
			body->body->ApplyForce( b2Vec2(0,force), body->body->GetWorldCenter(), true );
		}

	}
}

void physics_body_jump(
	physics_body_t* body,
	float vy)
{
	float impulse = body->body->GetMass() * vy;
	body->body->ApplyLinearImpulse( b2Vec2(0,-impulse), body->body->GetWorldCenter(), true );
}

void physics_body_stop(
		physics_body_t* body,
		int vx,
		int vy,
		float dt,
		int type)
{
	b2Vec2 velocity = body->body->GetLinearVelocity();
	if (vx == 0)
	{
		if(type == 0)
		{
			body->body->SetLinearVelocity(b2Vec2(0.0f,velocity.y));
		}
		else if (type == 1)
		{

			float desiredVel = vx;

			float velChange = desiredVel - velocity.x;
			float force = body->body->GetMass() * velChange / dt; //f = mv/t
			body->body->ApplyForce( b2Vec2(force,0), body->body->GetWorldCenter(), true );
		}

	}

	if (vy == 0)
	{
		if (type == 0)
		{
			body->body->SetLinearVelocity(b2Vec2(velocity.x,0.0f));
		}
		else if (type == 1)
		{

			float desiredVel = vy;

			float velChange = desiredVel - velocity.y;
			float force = body->body->GetMass() * velChange / dt; //f = mv/t
			body->body->ApplyForce( b2Vec2(0,force), body->body->GetWorldCenter(), true );
		}

	}
}

/*
* position is relative to the center of the body if given and absolute unless
* size in meter
* angle in radiant
*/
void physics_geometry_add_box(
	physics_body_t* body,
	float px,
	float py,
	float sx,
	float sy,
	float angle,
	int sensor,
	void* data)
{
	auto center_pos = b2Vec2(px, py);
	auto box_shape = b2PolygonShape();
	box_shape.SetAsBox(sx, sy, center_pos, angle * M_PI / 180.0f);

	auto fixture_def = b2FixtureDef();
	fixture_def.shape = &box_shape;
	fixture_def.userData = data;
	fixture_def.isSensor = sensor;

	body->body->CreateFixture(&fixture_def);
}

void physics_geometry_add_circle(
	physics_body_t* body,
	float px,
	float py,
	float radius,
	int sensor,
	void* data)
{
	auto circle_shape = b2CircleShape();
	circle_shape.m_p.Set(px, py);
	circle_shape.m_radius = radius;

	b2FixtureDef fixture_def = b2FixtureDef();
	fixture_def.shape = &circle_shape;
	fixture_def.userData = data;
	fixture_def.isSensor = sensor;

	body->body->CreateFixture(&fixture_def);
}

void physics_body_dealloc(
	physics_body_t* body)
{

		if (body != NULL)
		{
			free(body);
			body = NULL;
		}
}

void physics_body_remove(
	physics_world_t* world,
	physics_body_t* body)
{

	if ( body != NULL)
	{
		if (world != NULL && world->world != NULL && body->body != NULL)
		{
			world->world->DestroyBody(body->body);
			body->body = NULL;
		}
	}


}

void physics_raycast(
	physics_world_t* world,
	float p1x,
	float p1y,
	float p2x,
	float p2y)
{
	auto p1 = b2Vec2(p1x, p1y);
	auto p2 = b2Vec2(p2x, p2y);

	auto delta_pos = p1 - p2;
	(void)delta_pos;

	auto callback = PookooRayCastCallBack();
	world->world->RayCast(&callback, p1, p2);

	if(callback.hit)
	{

		void* user_data = callback.fixture->GetUserData();
		(void)user_data;

	}
}

void PookooContactListener::BeginContact(b2Contact* contact)
{
	void* a = contact->GetFixtureA()->GetUserData();
	void* b = contact->GetFixtureB()->GetUserData();
	bool begin = true;

	physics_event_t* event;

	//push data into physics event buffer
	if ((event = (physics_event_t*)malloc(sizeof(physics_event_t))) != NULL)
	{
		event->user_data_a = a;
		event->user_data_b = b;
		event->begin = begin;
		physics_events_buffer_push(event);
	}
}

void PookooContactListener::EndContact(b2Contact* contact)
{
	void* a = contact->GetFixtureA()->GetUserData();
	void* b = contact->GetFixtureB()->GetUserData();
	bool begin = false;

	physics_event_t* event;

	//push data into physics event buffer
	if ((event = (physics_event_t*)malloc(sizeof(physics_event_t))) != NULL)
	{
		event->user_data_a = a;
		event->user_data_b = b;
		event->begin = begin;
		physics_events_buffer_push(event);
	}
}

float32 PookooRayCastCallBack::ReportFixture(
	b2Fixture* fixture,
	const b2Vec2& point,
	const b2Vec2& normal,
	float32 fraction)
{
	this->hit = true;
	this->fixture = fixture;
	this->point = point;
	this->normal = normal;
	this->fraction = fraction;
	return this->fraction;
}
extern void* physics_events_buffer_get()
{
	return &physics_events_buffer;
}
