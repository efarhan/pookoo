#include <stdlib.h>
#include <math.h>
#include <string.h>

#include <video/video.h>
#include <video/internal/webm.h>
#include <video/internal/vpx.h>

#include <draw/internal/opengl_header.h>
#include <draw/internal/opengl.h>
#include <draw/internal/opengl_utils.h>
#include <draw/draw.h>
#include <window/window.h>

#include <audio/internal/openal.h>
#include <audio/audio.h>

#include <vorbis/codec.h>

#include <utils/alloca.h>

#include <log/log.h>

struct video_t
{
	// Container part
	FILE* file;
	webm_t* webm;

	// Video part
	vpx_t* vpx;
	unsigned int width, height;

	// Audio part
	ogg_int64_t audio_packet_nb;
	vorbis_stream_t vorbis;
	audio_buffer_t openal;
};

// The textures themselves.
static GLuint internal_yuv_textures[3];

// The location where the textures should be filled for the shader.
static GLuint internal_yuv_arb[3];

static const GLcharARB* internal_fragment_yuv_to_rgb = "      \
	uniform sampler2D Ytex, Utex, Vtex;                       \
	                                                          \
	void main(void) {                                         \
		float y = texture2D(Ytex, gl_TexCoord[0].xy).r;       \
  		float u = texture2D(Utex, gl_TexCoord[0].xy).r - 0.5; \
  		float v = texture2D(Vtex, gl_TexCoord[0].xy).r - 0.5; \
	                                                          \
		float r = y + 1.13983 * v;                            \
		float g = y - 0.39465 * u - 0.58060 * v;              \
		float b = y + 2.03211 * u;	  	                      \
														      \
		gl_FragColor = vec4(r, g, b, 1.0);                    \
	}";

static GLhandleARB internal_arb_program;
static GLhandleARB internal_arb_fragment_shader;

int video_init()
{
	/*if (!audio_begin())
	{
		return 0;
	}*/

	internal_arb_program = glCreateProgramObjectARB();
	internal_arb_fragment_shader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);

	// Compile the fragment shader.
	glShaderSourceARB(internal_arb_fragment_shader, 1, &internal_fragment_yuv_to_rgb, NULL);
	glCompileShaderARB(internal_arb_fragment_shader);

	if (!opengl_shader_log(internal_arb_fragment_shader))
	{
		return 0;
	}

	// Create a complete program object.
	glAttachObjectARB(internal_arb_program, internal_arb_fragment_shader);
	glLinkProgramARB(internal_arb_program);

	if (!opengl_shader_log(internal_arb_program))
	{
		return 0;
	}

	// Create textures for all three components.
	glGenTextures(3, internal_yuv_textures);

	for (int i = 0; i < 3; i++)
	{
		glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[i]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	return 1;
}

void video_finish()
{
	log_info("Close Video Module");
	glDeleteObjectARB(internal_arb_program);
	glDeleteObjectARB(internal_arb_fragment_shader);
	glDeleteTextures(3, internal_yuv_textures);
	
	//openal_finish();
}

video_t* video_open(const char* filename)
{
	video_t* video;

	if ((video = (video_t*)malloc(sizeof(video_t))) == NULL)
	{
		return NULL;
	}

	if ((video->vpx = vpx_open()) == NULL)
	{
		free(video);
		return NULL;
	}

	if ((video->file = fopen(filename, "rb")) == NULL)
	{
		log_error("File not found: %s", filename);
		vpx_close(video->vpx);
		free(video);
		return NULL;
	}

	if ((video->webm = webm_open(video->file, &video->width, &video->height, &video->vorbis)) == NULL)
	{
		fclose(video->file);
		vpx_close(video->vpx);
		free(video);
		return NULL;
	}
	
	video->openal.buffer_pos = 0;

	audio_buffer_register(&video->openal);
	video->openal.openal_source = openal_source_create(video->vorbis.state.vi->channels, video->vorbis.state.vi->rate);

	if (video->openal.openal_source == NULL)
	{
		webm_close(video->webm);
		fclose(video->file);
		vpx_close(video->vpx);
		free(video);
		return NULL;
	}
	// There are 3 header packets, so we start at 3.
	video->audio_packet_nb = 3;

	// Generate the textures in memory. It won't be created again, only filled with glTexSubImage2D.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[0]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, video->width, video->height, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[1]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, video->width / 2, video->height / 2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[2]);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, video->width / 2, video->height / 2, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, NULL);

	return video;
}

void video_close(video_t* video)
{

	log_info("Close Video instance");
	audio_buffer_unregister(&video->openal);

	vorbis_dsp_clear(&video->vorbis.state);
	vorbis_block_clear(&video->vorbis.block);
	vorbis_info_clear(&video->vorbis.info);
	vorbis_comment_clear(&video->vorbis.comment);





	openal_source_close(video->openal.openal_source);

	webm_close(video->webm);
	fclose(video->file);
	vpx_close(video->vpx);
	free(video);
}

void video_size(video_t* video, unsigned int* width, unsigned int* height)
{
	if (width)
	{
		*width = video->width;
	}

	if (height)
	{
		*height = video->height;
	}
}

int video_next(video_t* video, uint64_t* timestamp)
{
	unsigned char** vpx_data;
	int stride[3];
	unsigned int width;
	unsigned int height;

	// If we have audio, continue processing it until we have a video frame.
	while (true)
	{
		if ((vpx_data = vpx_read(video->vpx, &width, &height, stride)) == NULL)
		{
			unsigned char* webm_data;
			size_t webm_data_size;
			int data_type;

			if (!webm_read(video->webm, &webm_data, &webm_data_size, timestamp, &data_type))
			{
				return 0;
			}

			if (data_type == WEBM_VIDEO)
			{
				vpx_feed(video->vpx, webm_data, webm_data_size);

				if ((vpx_data = vpx_read(video->vpx, &width, &height, stride)) == NULL)
				{
					log_error("Something went wrong while reading data...");
					return 0;
				}

				// We have all we need, we can display the texture.
				break;
			}
			else // WEBM_AUDIO
			{
				ogg_packet packet;

				packet.packet = webm_data;
				packet.bytes = (long)webm_data_size;
				packet.b_o_s = 0;
				packet.e_o_s = 0;
				packet.granulepos = -1;
				packet.packetno = video->audio_packet_nb++;

				if (vorbis_synthesis(&video->vorbis.block, &packet) != 0)
				{
					log_info("Trying to synthesize invalid vorbis packet...");
					continue;
				}

				if (vorbis_synthesis_blockin(&video->vorbis.state, &video->vorbis.block) != 0) 
				{
					log_info("Invalid block generation...");
					continue;
				}

				float** pcm;
				int samples;
				size_t channels;
				openal_source_params(video->openal.openal_source, &channels, NULL);

				while ((samples = vorbis_synthesis_pcmout(&video->vorbis.state, &pcm)) > 0)
				{
					char* buffer = (char*)alloca(samples * channels * sizeof(short));
					short* p = (short*)buffer;

					for (int i = 0; i < samples; i++) 
					{
						for (int j = 0; j < channels; j++) 
						{
							int v = (int)floorf(0.5f + pcm[j][i] * 32767.0f);
							
							if (v > 32767)
							{
								v = 32767;
							}

							if (v < -32768)
							{
								v = -32768;
							}

							*p++ = v;
						}
					}

					// Fill the data into the buffer. It is filled in audio_step.
					size_t data_size = samples * channels * sizeof(short);

					while (sizeof(video->openal.buffer) - video->openal.buffer_pos < data_size)
					{
						// We can't afford to realloc or everything will be too slow.
						// We'll just have override existing data...						
						memcpy(video->openal.buffer, video->openal.buffer + data_size, data_size);
						video->openal.buffer_pos -= data_size;
					}

					memcpy(video->openal.buffer + video->openal.buffer_pos, buffer, data_size);
					video->openal.buffer_pos += data_size;

					if (vorbis_synthesis_read(&video->vorbis.state, samples) != 0)
					{
						log_info("Failed to mark frames as read.");
						continue;
					}
				}
			}
		}
	}

	glUseProgramObjectARB(internal_arb_program);

	internal_yuv_arb[0] = glGetUniformLocationARB(internal_arb_program, "Ytex");
	internal_yuv_arb[1] = glGetUniformLocationARB(internal_arb_program, "Utex");
	internal_yuv_arb[2] = glGetUniformLocationARB(internal_arb_program, "Vtex");

	glUniform1iARB(internal_yuv_arb[0], 0);  // Bind Ytex to texture unit 0
	glUniform1iARB(internal_yuv_arb[1], 1);  // Bind Utex to texture unit 1
	glUniform1iARB(internal_yuv_arb[2], 2);  // Bind Vtex to texture unit 2

	glEnable(GL_TEXTURE_2D);

	// Y
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[0]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, stride[0]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_LUMINANCE, GL_UNSIGNED_BYTE, vpx_data[0]);

	// U
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[1]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, stride[1]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width / 2, height / 2, GL_LUMINANCE, GL_UNSIGNED_BYTE, vpx_data[1]);

	// V
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, internal_yuv_textures[2]);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, stride[2]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width / 2, height / 2, GL_LUMINANCE, GL_UNSIGNED_BYTE, vpx_data[2]);

	glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);

	GLfloat vertices[] =
	{
		0.0f, 0.0f,
		(GLfloat)width, 0.0f,
		0.0f, (GLfloat)height,
		(GLfloat)width, (GLfloat)height,
	};

	GLfloat tex_indices[] =
	{
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f,
	};

	GLfloat colors[] =
	{
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
	};

	opengl_draw_data(vertices, tex_indices, colors, GL_TRIANGLE_STRIP, 4);

	glActiveTexture(GL_TEXTURE0);
	glDisable(GL_TEXTURE_2D);
	glUseProgramObjectARB(0);

	return 1;
}
