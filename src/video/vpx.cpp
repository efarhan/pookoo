#define VPX_CODEC_DISABLE_COMPAT 1

#include <video/internal/vpx.h>

#include <vpx/vpx_decoder.h>
#include <vpx/vp8dx.h>

#include <log/log.h>

#include <stdlib.h>

#define interface (vpx_codec_vp8_dx())

struct vpx_t
{
	vpx_codec_ctx_t codec;
	vpx_codec_iter_t iter;
};

static void internal_log_codec_error(vpx_codec_ctx_t *ctx, const char *s)
{
	const char *detail = vpx_codec_error_detail(ctx);

	log_error("%s: %s [%s]", s, vpx_codec_error(ctx), detail);
}

vpx_t* vpx_open()
{
	vpx_t* vpx;

	if ((vpx = (vpx_t*)malloc(sizeof(vpx_t))) == NULL)
	{
		return NULL;
	}

	if (vpx_codec_dec_init(&vpx->codec, interface, NULL, 0))
	{
		internal_log_codec_error(&vpx->codec, "Failed to initialize decoder");
		free(vpx);
		return NULL;
	}

	vpx->iter = NULL;
	return vpx;
}

int vpx_feed(vpx_t* vpx, unsigned char* buffer, size_t buffer_size)
{
	/* Decode the frame */
	if (vpx_codec_decode(&vpx->codec, (const uint8_t*)buffer, buffer_size, NULL, 0))
	{
		internal_log_codec_error(&vpx->codec, "Failed to decode frame");
		return 0;
	}

	vpx->iter = NULL;
	return 1;
}

unsigned char** vpx_read(vpx_t* vpx, unsigned int* width, unsigned int* height, int* stride)
{
	vpx_image_t* img;

	if ((img = vpx_codec_get_frame(&vpx->codec, &vpx->iter)))
	{
		if (width)
		{
			*width = img->d_w;
		}

		if (height)
		{
			*height = img->d_h;
		}

		if (stride)
		{
			for (int i = 0; i < 3; i++)
			{
				stride[i] = img->stride[i];
			}
		}

		if (img->fmt != VPX_IMG_FMT_I420)
		{
			log_error("Invalid video format. Only YUV420 is supported.");
			return NULL;
		}

		return img->planes;
	}

	return NULL;
}

void vpx_close(vpx_t* vpx)
{
	if (vpx_codec_destroy(&vpx->codec))
	{
		internal_log_codec_error(&vpx->codec, "Failed to destroy codec");
	}

	free(vpx);
}