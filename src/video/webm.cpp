#include <video/internal/webm.h>
#include <log/log.h>

#include <nestegg/nestegg.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct webm_t
{
	nestegg* ctx;
	nestegg_packet* pkt;
	int pkt_data_type;
	unsigned int pkt_count;
	unsigned int pkt_current;

	int has_video;
	unsigned int video_track;

	int has_audio;
	unsigned int audio_track;
};

static int internal_stdio_read(void * p, size_t length, void * fp)
{
	size_t r = fread(p, length, 1, (FILE*)fp);

	if (r == 0 && feof((FILE*)fp))
	{
		return 0;
	}

	return r == 0 ? -1 : 1;
}

static int internal_stdio_seek(int64_t offset, int whence, void* fp)
{
	return fseek((FILE*)fp, offset, whence);
}

static int64_t internal_stdio_tell(void* fp)
{
	return ftell((FILE*)fp);
}

static int internal_vorbis_header_process(webm_t* webm, vorbis_stream_t* vorbis)
{
	// Get the Vorbis header data
	unsigned int nheaders = 0;
	int count = nestegg_track_codec_data_count(webm->ctx, webm->audio_track, &nheaders);

	if (count == -1 || nheaders != 3)
	{
		log_error("Invalid vorbis header count.");
		return 0;
	}
	
	vorbis_info_init(&vorbis->info);
	vorbis_comment_init(&vorbis->comment);

	for (uint32_t header = 0; header < nheaders; ++header)
	{
		unsigned char* data = 0;
		size_t length = 0;

		if (nestegg_track_codec_data(webm->ctx, webm->audio_track, header, &data, &length) == -1)
		{
			log_error("Invalid vorbis header reading.");
			return 0;
		}

		ogg_packet packet;
		packet.packet = data;
		packet.bytes = length;
		packet.b_o_s = header == 0;
		packet.e_o_s = 0;
		packet.granulepos = 0;
		packet.packetno = header;

		if (vorbis_synthesis_headerin(&vorbis->info, &vorbis->comment, &packet) != 0)
		{
			log_error("Invalid vorbis header data.");
			return 0;
		}
	}

	if (vorbis_synthesis_init(&vorbis->state, &vorbis->info) != 0)
	{
		log_error("Failed to initialize vorbis synthesis.");
		return 0;
	}

	if (vorbis_block_init(&vorbis->state, &vorbis->block) != 0)
	{
		log_error("Failed to initialize vorbis block.");
		return 0;
	}
	return 1;
}

webm_t* webm_open(FILE* file, unsigned int* width, unsigned int* height, vorbis_stream_t* vorbis)
{
	webm_t* webm;

	if ((webm = (webm_t*)malloc(sizeof(webm_t))) == NULL)
	{
		return NULL;
	}

	nestegg_io io = {
		internal_stdio_read,
		internal_stdio_seek,
		internal_stdio_tell,
		file
	};

	nestegg_init(&webm->ctx, io, NULL);

	webm->has_audio = 0;
	webm->has_video = 0;

	unsigned int n;
	nestegg_track_count(webm->ctx, &n);

	for (unsigned int i = 0; i < n; i++) 
	{
		int track_type = nestegg_track_type(webm->ctx, i);
		int codec_id = nestegg_track_codec_id(webm->ctx, i);

		if (track_type == NESTEGG_TRACK_VIDEO) 
		{
			if (codec_id == NESTEGG_CODEC_VP8)
			{
				webm->video_track = i;
				webm->has_video = 1;

				nestegg_video_params params;
				
				if (nestegg_track_video_params(webm->ctx, i, &params) >= 0)
				{
					if (width)
					{
						*width = params.width;
					}

					if (height)
					{
						*height = params.height;
					}
				}
				else
				{
					log_error("Failed to fetch video parameters.");
					webm_close(webm);
					return NULL;
				}
			}
			else
			{
				log_error("Invalid video format: Only VP8 is supported.");
				webm_close(webm);
				return NULL;
			}
		}
		else if (track_type == NESTEGG_TRACK_AUDIO)
		{
			if (codec_id == NESTEGG_CODEC_VORBIS)
			{
				webm->audio_track = i;
				webm->has_audio = 1;

				if (!internal_vorbis_header_process(webm, vorbis))
				{
					webm_close(webm);
					return NULL;
				}
			}
			else
			{
				log_error("Invalid audio format: Only Vorbis is supported.");
				webm_close(webm);
				return NULL;
			}
		}

		if (webm->has_audio && webm->has_video)
		{
			break;
		}
	}

	if (!webm->has_audio && !webm->has_video)
	{
		log_error("Failed to find any video or audio track in webm file!");
		webm_close(webm);
		return 0;
	}

	webm->pkt = NULL;
	webm->pkt_count = 0;
	webm->pkt_current = 0;

	return webm;
}

void webm_close(webm_t* webm)
{
	nestegg_free_packet(webm->pkt);
	nestegg_destroy(webm->ctx);
	free(webm);
}

int webm_read(webm_t* webm, unsigned char** data, size_t* data_size, uint64_t* timestamp, int* data_type)
{
	if (webm->pkt != NULL)
	{
		if (webm->pkt_current < webm->pkt_count)
		{
			nestegg_packet_data(webm->pkt, webm->pkt_current, data, data_size);
			webm->pkt_current++;
			*data_type = webm->pkt_data_type;
			return 1;
		}
		else
		{
			nestegg_free_packet(webm->pkt);
			webm->pkt = NULL;
		}
	}

	while (true)
	{
		if (nestegg_read_packet(webm->ctx, &webm->pkt) <= 0)
		{
			// No data left.
			return 0;
		}

		unsigned int track;
		nestegg_packet_track(webm->pkt, &track);

		if (timestamp)
		{
			nestegg_packet_tstamp(webm->pkt, timestamp);
		}

		if (track == webm->video_track)
		{
			if (data_type)
			{
				*data_type = WEBM_VIDEO;
				webm->pkt_data_type = *data_type;
			}

			break;
		}

		if (track == webm->audio_track)
		{
			if (data_type)
			{
				*data_type = WEBM_AUDIO;
				webm->pkt_data_type = *data_type;
			}

			break;
		}
	}

	nestegg_packet_count(webm->pkt, &webm->pkt_count);
	nestegg_packet_data(webm->pkt, 0, data, data_size);
	webm->pkt_current = 1;
	return 1;
}
