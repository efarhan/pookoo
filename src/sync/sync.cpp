/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <sync/internal/timer.h>
#include <sync/internal/sleep.h>
#include <sync/sync.h>

#include <stdlib.h>

struct sync_t
{
	int dt;        // asked nanoseconds between frames
	int actual_dt; // actual time between frames

	int sec;
	int nsec;

	void* sleep;
};

int sync_begin()
{
	return timer_begin();
}

sync_t* sync_open(unsigned int framerate)
{
	sync_t* sync;
	
	if ((sync = (sync_t*)malloc(sizeof(sync_t))) == NULL)
	{
		return NULL;
	}

	sync->dt = 1000000000 / framerate;
	sync->actual_dt = 0;
	sync->sleep = sleep_open();

	if(sync->sleep == NULL)
	{
		free(sync);
		return NULL;
	}

	timer_get(&sync->sec, &sync->nsec);

	return sync;
}

void sync_set(sync_t* sync, int dt)
{
	sync->dt = dt;
}

static void internal_sync_diff(
	int  sec1,
	int  nsec1,
	int  sec2,
	int  nsec2,
	int* sec3,
	int* nsec3)
{
	int rs = sec1 - sec2;
	int rn = nsec1 - nsec2;

	if(rn < 0)
	{
		rs--;
		rn += 1000000000;
	}

	if(sec3)
	{
		*sec3  = rs;
	}

	if(nsec3)
	{
		*nsec3 = rn;
	}
}

int sync_step(sync_t* sync)
{
	int old_sec, old_nsec;
	int new_sec, new_nsec;
	int diff_sec, diff_nsec;

	old_sec = sync->sec;
	old_nsec = sync->nsec;

	timer_get(
		&new_sec,
		&new_nsec);

	internal_sync_diff(
		new_sec,
		new_nsec,
		old_sec,
		old_nsec,
		&diff_sec,
		&diff_nsec);

	internal_sync_diff(
		0,
		sync->dt,
		diff_sec,
		diff_nsec,
		&diff_sec,
		&diff_nsec);

	// If the program is running to slow,
	// don't try to synchronize.
	if(diff_sec > 0
	|| (diff_sec  == 0
	 && diff_nsec > 0))
	{
		sleep_wait(
			diff_sec,
			diff_nsec,
			sync->sleep);
	
		timer_get(
			&sync->sec,
			&sync->nsec);

		internal_sync_diff(
			sync->sec,
			sync->nsec,
			old_sec,
			old_nsec,
			&diff_sec,
			&diff_nsec);

		sync->actual_dt = diff_sec * 1000000000 + diff_nsec;

		return 1;
	}
	else
	{
		sync->sec  = new_sec;
		sync->nsec = new_nsec;

		sync->actual_dt = sync->dt;

		return 0;
	}
}

float sync_delta(sync_t* sync)
{
	return (float)sync->actual_dt / 1000000000;
}

void sync_close(sync_t* sync)
{
	if (sync == NULL)
	{
		return;
	}

	sleep_close(sync->sleep);
	free(sync);
}

void sync_finish()
{
	timer_finish();
}
