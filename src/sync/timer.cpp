/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <sync/internal/timer.h>
#include <stdlib.h> // malloc

#ifndef _WIN32

	#include <time.h>
	
	#if defined(__APPLE__)

		#include <mach/clock.h>
		#include <mach/mach.h>

	#endif

#else

	#include <windows.h>

#endif

typedef struct
{
#if defined(_WIN32)

	LARGE_INTEGER time;
	LARGE_INTEGER freq;

#elif defined(__APPLE__)

	clock_serv_t clock;

#else

	struct timespec time;

#endif
} m_time;

static m_time timer_handle;

void timer_get(int* sec, int* nsec)
{
#if defined(_WIN32)

	int tick;
	
	QueryPerformanceCounter(&timer_handle.time);

	tick = (int)(timer_handle.time.QuadPart / timer_handle.freq.QuadPart);

	if(sec)
	{
		*sec = tick;
	}

	if(nsec)
	{
		double d = (double)timer_handle.time.QuadPart / (double)timer_handle.freq.QuadPart;
		*nsec = (int)((d - tick) * 1000000000);
	}

	// Other less precise method for testing!
	/*tick = GetTickCount();
 
	if(sec)
		*sec = tick / 1000;

	if(nsec)
		*nsec = (tick % 1000) * 1000000;*/

#elif defined(__APPLE__)

	mach_timespec_t mts;
	
	clock_get_time(timer_handle.clock, &mts);

	if(sec)
		*sec = (int)mts.tv_sec;

	if(nsec)
		*nsec = (int)mts.tv_nsec;
	
#else

	clock_gettime(CLOCK_REALTIME, &timer_handle.time);
	
	if(sec)
		*sec  = timer_handle.time.tv_sec;
	
	if(nsec)
		*nsec = timer_handle.time.tv_nsec;

#endif
}

int timer_begin()
{
#if defined(_WIN32)

	QueryPerformanceFrequency(&timer_handle.freq);

#elif defined(__APPLE__)

	host_get_clock_service(
		mach_host_self(),
		CALENDAR_CLOCK,
		&timer_handle.clock);

#endif

	return 1;
}

void timer_finish()
{
#if defined(__APPLE__)

	mach_port_deallocate(
		mach_task_self(),
		timer_handle.clock);

#endif
}

