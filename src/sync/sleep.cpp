/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <sync/internal/sleep.h>
#include <stdlib.h> // malloc

#ifndef _WIN32
#include <time.h>
#else
#include <windows.h>
#endif

void* sleep_open()
{
#ifdef _WIN32

	HANDLE timer;

	if((timer = CreateWaitableTimer(NULL, FALSE, NULL)) == NULL)
	{
		return NULL;
	}

	return (void*)timer;

#else

	return (void*)malloc; // Any not-null pointer, we can't fail.

#endif
}

void sleep_close(void* handle)
{
#ifdef _WIN32

	CloseHandle((HANDLE)handle);

#else

	// Nothing.
	(void)handle;

#endif
}

void sleep_wait(
	int sec,
	int nsec,
	void* handle)
{
#if defined(_WIN32)

	LARGE_INTEGER duetime;
	 __int64 t = - (sec * 10000000 + nsec / 100);

	// The unit is 100 nsec, so dividing by 100.
	duetime.LowPart = (DWORD)(t & 0xFFFFFFFF);
	duetime.HighPart = (LONG)(t >> 32);

	if(SetWaitableTimer(
		(HANDLE)handle,
		&duetime,
		0,
		NULL,
		NULL,
		TRUE))
		WaitForSingleObject((HANDLE)handle, INFINITE);

	// Other less precise version for testing.
	//Sleep(sec * 1000 + nsec / 1000000);

#else

	struct timespec st;

	st.tv_sec  = sec + nsec / 1000000000;
	st.tv_nsec = nsec % 1000000000;
	nanosleep(&st, NULL);

	(void)handle; // Not used, only for Windows.

#endif
}

