/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <texture/texture.h>
#include <texture/internal/gltexture.h>
#include <utils/utils.h>
#include <log/log.h>

#include <stdlib.h>
#include <string.h>
#include <set>

struct lt_texture
{
	bool operator()(texture_t* t1, texture_t* t2) const
	{
		return strcmp(
				t1->filename,
				t2->filename) < 0;
	}
};

static std::set<texture_t*, lt_texture>* loaded_textures;

void texture_begin()
{
	loaded_textures = new std::set<texture_t*, lt_texture>();
}

texture_t* texture_open(const char* filename)
{
	FILE* f;	
	texture_t *texture, my_tex;

	my_tex.filename = filename;
	
	auto it = loaded_textures->find(&my_tex);

	if(it != loaded_textures->end())
	{
		texture = *it;

		texture->counter++;
		return texture; // It already exists!
	}
	
	/*
	 * Else, we load it.
	 */
	if((texture = (texture_t*)malloc(sizeof(texture_t))) == NULL)
	{
		return NULL;
	}
	
	if((f = fopen(filename, "rb")) == NULL)
	{
		log_error("File '%s' was not found.", filename);
		return NULL;
	}

	if (!internal_texture_loadpng(f, texture))
	{
		free(texture);
		fclose(f);
		return NULL;
	}

	fclose(f);

	texture->filename = utils_dupstr(filename);
	texture->counter = 1;

	loaded_textures->insert(texture);

	return texture;
}

void texture_close(texture_t* texture)
{
	if (texture != NULL)
	{
		texture->counter--;

		/*
		* Delete only when all occurrences
		* also called this function.
		*/
		if (texture->counter == 0)
		{
			if (loaded_textures != NULL)
			{
				loaded_textures->erase(texture);

			}
			
			glDeleteTextures(1, &texture->texid);

			free((void*)texture->filename);
			free(texture);
			texture = NULL;
		}
	}
}

int texture_finish(void)
{
	int ret = 0;

	if(loaded_textures != NULL && loaded_textures->size() > 0)
	{
		size_t count = loaded_textures->size();
		log_debug("%d textures weren't properly unloaded!", count);

		for(auto it = loaded_textures->begin();
		    it != loaded_textures->end();
		    it++)
		{
			texture_t* t = *it;
			log_debug("  Filename=%s", t->filename);
		}

		ret = 1;
	}

	delete loaded_textures;
	loaded_textures = NULL;
	return ret;
}

int texture_width(texture_t* t)
{
	return (int)t->width;
}

int texture_height(texture_t* t)
{
	return (int)t->height;
}

