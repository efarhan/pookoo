/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#include <input/input.h>
#include <window/internal/internal.h>
#include <log/log.h>

#include <list>

#include <SFML/Window.hpp>

/*
 * Keyboard part
 */
int input_keyboard_pressed(int key)
{
	return sf::Keyboard::isKeyPressed((sf::Keyboard::Key)key);
}

// Returns the last pressed scancode.
int input_keyboard_get()
{
	return internal_last_keyboard_scancode;
}

const char* input_keyboard_string_get()
{
	return internal_keyboard_text;
}

void input_keyboard_string_clear()
{
	internal_keyboard_text[0] = '\0';
}

/*
 * Mouse part
 */
void input_mouse_position(int* px, int* py)
{
	//TODO set the window position of the mouse
	sf::Vector2i position = sf::Mouse::getPosition(*((sf::Window*)internal_window_handler));
	*(px) = position.x;
	*(py) = position.y;
}

bool input_mouse_pressed(int button_nb)
{
	return sf::Mouse::isButtonPressed((sf::Mouse::Button)button_nb);
}

/*
 * Joystick part
 */
static int count;

int input_joystick_begin()
{

	for (int i = 0; i < sf::Joystick::Count; i++)
	{
		if (!sf::Joystick::isConnected(i)){
			count = i;
			break;
		}
	}

	return 1;
}



int input_joystick_count()
{
	return count;
}


int input_joystick_axis_count(int joystick_nb)
{
	for(int i = 0; i<sf::Joystick::AxisCount; i++){
		if(!sf::Joystick::hasAxis(joystick_nb, sf::Joystick::X)){
			return i;
		}
	}

	return sf::Joystick::AxisCount;
}

int input_joystick_button_count(int joystick_nb)
{


	return sf::Joystick::getButtonCount(joystick_nb);
}

float input_joystick_axis_get(int joystick_nb, int axis)
{


	return sf::Joystick::getAxisPosition(joystick_nb, (sf::Joystick::Axis)axis);
}

bool input_joystick_button_get(int joystick_nb, int button_nb)
{

	return sf::Joystick::isButtonPressed(joystick_nb, button_nb);
}

void input_joystick_finish()
{

}
