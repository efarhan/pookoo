/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <log/log.h>

#include <utils/utils.h>
#include <utils/const.h>

#include <stdarg.h>
#include <stdio.h> // vsnprintf
#include <string.h>
#include <time.h>
#include <list>

#include <iostream>

#define LOG_BUFSIZE 512

static std::list<void(*)(int, const char*)>* internal_handlers;
static FILE* internal_file;

int log_begin()
{
	char buffer[PATH_LIMIT];
	char filename[PATH_LIMIT];

	internal_handlers = new std::list<void(*)(int, const char*)>();

	// Create log/ directory if it doesn't exist yet.
	utils_data_path(buffer, PATH_LIMIT, "log");
	
	if (!utils_makedir(buffer))
	{
		printf("Warning: Failed to create config directory: %s...\n", buffer);
	}

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);

	utils_snprintf(filename, PATH_LIMIT, "log/%d-%d-%d.log", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday);
	utils_data_path(buffer, PATH_LIMIT, filename);

	internal_file = fopen(buffer, "a");

	if (internal_file != NULL)
	{
		utils_snprintf(buffer, PATH_LIMIT, "[ -- %d:%d:%d -- ]\n", tm.tm_hour, tm.tm_min, tm.tm_sec);
		fwrite(buffer, 1, strlen(buffer), internal_file);
	}

	return internal_file != NULL;
}

void log_handler(void(*handler)(int level, const char* message))
{
	internal_handlers->push_back(handler);
}

void log_clear(void)
{
	internal_handlers->clear();
}

static void internal_log(int level, const char* message)
{
	for (auto it = internal_handlers->begin();
		it != internal_handlers->end();
		it++)
	{
		auto func = *it;
		func(level, message);
	}

	if (internal_file != NULL)
	{
		fwrite(message, 1, strlen(message), internal_file);
		fwrite("\n", 1, 1, internal_file);

		// Should we flush? I don't really know... Let's say we only do so
		// for critical errors.
		if (level == LOG_ERROR)
		{
			fflush(internal_file);
		}
	}
}

void log_info(const char* format, ...)
{
	va_list args;
	char buffer[LOG_BUFSIZE];

	va_start(args, format);
	vsnprintf(buffer, LOG_BUFSIZE, format, args);
	va_end(args);

	internal_log(LOG_INFO, buffer);
}

void log_error(const char* format, ...)
{
	va_list args;
	char buffer[LOG_BUFSIZE];

	va_start(args, format);
	vsnprintf(buffer, LOG_BUFSIZE, format, args);
	va_end(args);

	internal_log(LOG_ERROR, buffer);
}

void log_debug(const char* format, ...)
{
#ifdef _DEBUG
	va_list args;
	char buffer[LOG_BUFSIZE];

	va_start(args, format);
	vsnprintf(buffer, LOG_BUFSIZE, format, args);
	va_end(args);

	internal_log(LOG_DEBUG, buffer);
#else
	(void)format;
#endif
}

void log_finish()
{
	if (internal_file != NULL)
	{
		fclose(internal_file);
	}

	delete internal_handlers;
}
