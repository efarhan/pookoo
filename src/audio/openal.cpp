/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <audio/internal/openal.h>
#include <audio/internal/vorbis.h>

#include <log/log.h>

#include <SoundTouch/include/SoundTouch.h>

#include <AL/al.h>
#include <AL/alc.h>

#include <string.h>

static ALCcontext *internal_context = NULL;
static ALCdevice *internal_microphone_device = NULL;

struct openal_t
{
	size_t channels;
	size_t samplerate;

	ALuint buffers[OPENAL_NB_BUFFERS];
	ALuint source;
};

static ALenum internal_get_format(size_t channels)
{
	return channels == 1 ? AL_FORMAT_MONO16 : AL_FORMAT_STEREO16;
}

int openal_begin()
{
	log_info("Init OpenAL");
	if (internal_context != NULL)
	{
		// openal_open already called! This can happen if you video_init and audio_init.
		log_info("OpenAL already opened");
		return 1;
	}

	ALCdevice *device;

	if ((device = alcOpenDevice(NULL)) == NULL)
	{
		return 0;
	}
	log_info("alcOpenDevice");
	if ((internal_context = alcCreateContext(device, NULL)) == NULL)
	{
		alcCloseDevice(device);
		return 0;
	}
	log_info("alcCreateContext");
	if (!alcMakeContextCurrent(internal_context))
	{
		alcDestroyContext(internal_context);
		alcCloseDevice(device);
		return 0;
	}
	log_info("alcMakeContextCurrent");
	log_debug("AL_VENDOR: %s", (char *)alGetString(AL_VENDOR));
	log_debug("AL_RENDERER: %s", (char *)alGetString(AL_RENDERER));
	log_debug("AL_VERSION: %s", (char *)alGetString(AL_VERSION));

	return 1;
}

int openal_capture_begin()
{
	if((internal_microphone_device = alcCaptureOpenDevice(NULL, 44100, AL_FORMAT_STEREO16, OPENAL_BUFFER_SIZE * OPENAL_NB_BUFFERS)) == NULL)
	{
		return 0;
	}

	alcCaptureStart(internal_microphone_device);
	return 1;
}

size_t openal_capture_read(char* buffer, size_t buffersize)
{
	ALCint samples;
	alcGetIntegerv(internal_microphone_device, ALC_CAPTURE_SAMPLES, (ALCsizei)sizeof(ALCint), &samples);
	buffersize /= 4; // AL_FORMAT_STEREO16 in alcCaptureOpenDevice

	// If the data we got is too small, ignore it and leave it for next time.
	if (samples < buffersize)
	{
		return 0;
	}

	if (samples > buffersize)
	{
		samples = buffersize;
	}

	alcCaptureSamples(internal_microphone_device, (ALCvoid*)buffer, (ALCsizei)samples);
	return (size_t)(samples * 4);
}

void openal_capture_finish()
{
	alcCaptureStop(internal_microphone_device);
	alcCaptureCloseDevice(internal_microphone_device);
	internal_microphone_device = NULL;
}

openal_t* openal_source_create(size_t channels, size_t samplerate)
{
	openal_t* source;

	if ((source = (openal_t*)malloc(sizeof(openal_t))) == NULL)
	{
		return NULL;
	}

	source->channels = channels;
	source->samplerate = samplerate;

	alGenSources(1, &source->source);
	alGenBuffers(OPENAL_NB_BUFFERS, source->buffers);

	ALenum format = internal_get_format(channels);

	// Fill with empty buffers such that they are noticed to be empty.
	for (int i = 0; i < OPENAL_NB_BUFFERS; i++)
	{
		alBufferData(source->buffers[i], format, NULL, 0, (ALsizei)samplerate);
	}

	alSourceQueueBuffers(source->source, OPENAL_NB_BUFFERS, source->buffers);
	alSourcePlay(source->source);

	return source;
}

void openal_source_params(openal_t* source, size_t* channels, size_t* samplerate)
{
	if (channels)
	{
		*channels = source->channels;
	}

	if (samplerate)
	{
		*samplerate = source->samplerate;
	}
}

int openal_source_update_buffer(audio_buffer_t* audio_data)
{
	ALint processed;
	ALenum state;
	ALenum format;

	ALuint openal_buffer;

	openal_t* source = audio_data->openal_source;

	if (internal_context == NULL)
	{
		log_error("openal_open wasn't called!");
		return 0;
	}
	
	if (audio_data->buffer_pos <= 0)
	{
		// Nothing to do.
		return 1;
	}

	alSourcef(source->source, AL_GAIN, internal_volume_get());
	alGetSourcei(source->source, AL_BUFFERS_PROCESSED, &processed);

	// Only Mono and Stero is supported (check done in vorbis_open)
	format = internal_get_format(source->channels);

	while (processed > 0)
	{
		alSourceUnqueueBuffers(source->source, 1, &openal_buffer);

		size_t size = audio_data->buffer_pos;

		if (size > OPENAL_BUFFER_SIZE)
		{
			size = OPENAL_BUFFER_SIZE;
		}

		alBufferData(openal_buffer, format, audio_data->buffer, size, (ALsizei)source->samplerate);
		// Shift the data to the left. It is safe in this direction.
		memcpy(audio_data->buffer, audio_data->buffer + size, audio_data->buffer_pos - size);
		audio_data->buffer_pos -= size;

		alSourceQueueBuffers(source->source, 1, &openal_buffer);

		processed--;
	}

	alGetSourcei(source->source, AL_SOURCE_STATE, &state);

	if (state != AL_PLAYING)
	{
		alSourcePlay(source->source);
	}

	return 1;
}

int openal_source_update_stream(audio_stream_t* audio_data)
{
	openal_t* source = audio_data->openal_source;
	float volume = audio_data->volume;
	int end_of_file = 0;
	ALint processed;
	ALenum state;
	ALenum format;

	ALuint openal_buffer;

	if (internal_context == NULL)
	{
		log_error("openal_open wasn't called!");
		return end_of_file;
	}

	float tempo = internal_stream_tempo_get();

	if (tempo <= 0.0f)
	{
		log_error("tempo is <= 0.0 !");
		return end_of_file;
	}

	alSourcef(source->source, AL_GAIN, volume);
	alGetSourcei(source->source, AL_BUFFERS_PROCESSED, &processed);

	// Only Mono and Stero is supported (check done in vorbis_open)
	format = internal_get_format(source->channels);

	auto st = static_cast<soundtouch::SoundTouch*>(audio_data->stream_soundtouch);

	st->setChannels((uint)source->channels);
	st->setSampleRate((uint)source->samplerate);

	while (processed > 0)
	{
		char internal_data_buffer[OPENAL_BUFFER_SIZE];
		auto soundtouch_buffer = (soundtouch::SAMPLETYPE*)alloca((size_t)(OPENAL_BUFFER_SIZE / tempo));

		alSourceUnqueueBuffers(source->source, 1, &openal_buffer);

		size_t read = vorbis_read(audio_data->vorbis_data, (char*)internal_data_buffer, OPENAL_BUFFER_SIZE);
		uint soundTouchRead;

		st->putSamples((soundtouch::SAMPLETYPE*)internal_data_buffer, (uint)(read / sizeof(soundtouch::SAMPLETYPE) / source->channels));
		soundTouchRead = st->receiveSamples(soundtouch_buffer, (uint)(OPENAL_BUFFER_SIZE / tempo) / sizeof(soundtouch::SAMPLETYPE) / source->channels);
		alBufferData(openal_buffer, format, soundtouch_buffer, soundTouchRead * source->channels * sizeof(soundtouch::SAMPLETYPE), (ALsizei)source->samplerate);
		
		alSourceQueueBuffers(source->source, 1, &openal_buffer);

		processed--;

		if (read < OPENAL_BUFFER_SIZE)
		{
			end_of_file = 1;
			break;
		}
	}

	alGetSourcei(source->source, AL_SOURCE_STATE, &state);

	if (state != AL_PLAYING)
	{
		alSourcePlay(source->source);
	}

	return end_of_file;
}

int openal_source_update_sound(playing_sound_t* audio_data)
{
	openal_t* source = audio_data->openal_source;
	ALint processed;
	ALenum state;
	ALenum format;

	ALuint openal_buffer;

	if (audio_data->current_position >= audio_data->sound->buffer_size)
	{
		alGetSourcei(source->source, AL_SOURCE_STATE, &state);
		alSourcef(source->source, AL_GAIN, internal_volume_get());

		if (state != AL_PLAYING)
		{
			// Tell we are really done playing the sound.
			return 1;
		}

		// Tell we reached end of file, but we are still playing.
		return 0;
	}

	if (internal_context == NULL)
	{
		log_error("openal_open wasn't called!");
		return 1;
	}

	alGetSourcei(source->source, AL_BUFFERS_PROCESSED, &processed);

	// Only Mono and Stero is supported (check done in vorbis_open)
	format = internal_get_format(source->channels);

	while (processed > 0)
	{
		char* buffer = audio_data->sound->buffer + audio_data->current_position;
		size_t buffsize;
		
		if (audio_data->current_position + OPENAL_BUFFER_SIZE > audio_data->sound->buffer_size)
		{
			buffsize = audio_data->sound->buffer_size - audio_data->current_position;
		}
		else
		{
			buffsize = OPENAL_BUFFER_SIZE;
		}

		alSourceUnqueueBuffers(source->source, 1, &openal_buffer);
		alBufferData(openal_buffer, format, buffer, buffsize, (ALsizei)source->samplerate);
		alSourceQueueBuffers(source->source, 1, &openal_buffer);

		audio_data->current_position += buffsize;
		processed--;

		if (audio_data->current_position >= audio_data->sound->buffer_size)
		{
			break;
		}
	}

	alGetSourcei(source->source, AL_SOURCE_STATE, &state);

	if (state != AL_PLAYING)
	{
		alSourcePlay(source->source);
	}

	return 0;
}

void openal_source_close(openal_t* source)
{
	alSourceStop(source->source);

	alDeleteSources(1, &source->source);
	alDeleteBuffers(OPENAL_NB_BUFFERS, source->buffers);

	free(source);
}

void openal_finish()
{
	if (internal_context == NULL)
	{
		// This can happen if video_finish and audio_finish
		return;
	}

	ALCdevice* device = alcGetContextsDevice(internal_context);
	alcMakeContextCurrent(NULL);
	alcDestroyContext(internal_context);
	alcCloseDevice(device);
}
