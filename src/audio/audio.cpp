/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */
#include <SoundTouch/include/SoundTouch.h>

#include <audio/audio.h>
#include <audio/internal/internal.h>
#include <audio/internal/vorbis.h>
#include <audio/internal/openal.h>

#include <log/log.h>

#include <list>
#include <vector>
#include <algorithm> // find

static std::list<audio_stream_t*> internal_stream_list;
static std::list<playing_sound_t*> internal_sound_list;
static std::list<audio_buffer_t*> internal_buffer_list;


static float internal_stream_tempo;
static float internal_volume;



float internal_stream_tempo_get()
{
	return internal_stream_tempo;
}

float internal_volume_get()
{
	return internal_volume;
}

int audio_begin()
{
	// Already initialized. Can happen if audio_init and video_init

	internal_stream_tempo = 1.0f;
	internal_volume = 1.0f;
	return openal_begin();
}

void audio_volume(float volume)
{
	internal_volume = volume;
}


void audio_finish()
{
	if (internal_stream_list.size() > 0)
	{
		log_info("There are %d unfreed streams still playing!", (int)internal_stream_list.size());
	}

	for (auto it = internal_sound_list.begin();
		      it != internal_sound_list.end();)
	{
		playing_sound_t* playing = *it;
		openal_source_close(playing->openal_source);
		it = internal_sound_list.erase(it);
	}

	openal_finish();
}

audio_stream_t* audio_stream_load(const char* inputurl)
{
	
	vorbis_t* vorbis_data;
	audio_stream_t* ret;

	size_t channels;
	size_t samplerate;

	log_info("Loading audio file: %s\n", inputurl);

	if ((vorbis_data = vorbis_open(
		inputurl,
		&channels,
		&samplerate)) == NULL)
	{
		return NULL;
	}

	if ((ret = (audio_stream_t*)malloc(sizeof(audio_stream_t))) == NULL)
	{
		vorbis_close(vorbis_data);
		return NULL;
	}

	ret->vorbis_data = vorbis_data;
	ret->openal_source = openal_source_create(channels, samplerate);
	ret->is_stopped = 0;
	ret->volume = 1.0f;
	ret->stream_soundtouch = new soundtouch::SoundTouch();

	internal_stream_list.push_back(ret);

	return ret;
}

stream_status audio_stream_status(audio_stream_t* audio)
{
	if (audio->is_stopped)
	{
		return STOPPED;
	}

	auto it = internal_stream_list.begin();

	if (it != internal_stream_list.end() && *it == audio)
	{
		return PLAYING;
	}
	else
	{
		return PENDING;
	}
}

void audio_stream_volume_set(audio_stream_t* audio, float volume) {
	audio->volume = volume;
}

float  audio_stream_volume_get(audio_stream_t* audio) {
	return audio->volume;
}
int audio_capture_begin()
{
	return openal_capture_begin();
}

size_t audio_capture_read(char* buffer, size_t buffsize)
{
	return openal_capture_read(buffer, buffsize);
}

void audio_capture_finish()
{
	openal_capture_finish();
}

void audio_tempo(float tempo)
{
	internal_stream_tempo = tempo;
	//st->setTempo(tempo);
}

void audio_pitch(float pitch)
{
	//st->setPitch(pitch);
}

static void internal_audio_stream_stop(audio_stream_t* audio)
{
	if (audio != NULL)
	{
		static_cast<soundtouch::SoundTouch*>(audio->stream_soundtouch)->clear();
		delete static_cast<soundtouch::SoundTouch*>(audio->stream_soundtouch);
		vorbis_close(audio->vorbis_data);
		openal_source_close(audio->openal_source);
		free(audio);
	}
}

void audio_stream_unload(audio_stream_t* audio)
{
	std::list<audio_stream_t*>::iterator it;

	while ((it = std::find(internal_stream_list.begin(), internal_stream_list.end(), audio)) != internal_stream_list.end())
	{
		internal_stream_list.erase(it);
	}

	internal_audio_stream_stop(audio);
}

void audio_stream_clear()
{
	for (auto it = internal_stream_list.begin();
		      it != internal_stream_list.end();)
	{
		audio_stream_t* audio = *it;
		internal_audio_stream_stop(audio);
		it = internal_stream_list.erase(it);
	}
}

void audio_step()
{

	// Play the first element of the playlist.


	for(auto stream_iter = internal_stream_list.begin();
			stream_iter != internal_stream_list.end();){
		int end_of_file = openal_source_update_stream(*stream_iter);

		if (end_of_file)
		{
			(*stream_iter)->is_stopped = 1;
			stream_iter = internal_stream_list.erase(stream_iter);
		}
		else if ((*stream_iter)->volume <= 0.0f){
			(*stream_iter)->is_stopped = 1;
			stream_iter = internal_stream_list.erase(stream_iter);
		}
		else
		{
			stream_iter++;
		}

	}

	for (auto it = internal_sound_list.begin();
		      it != internal_sound_list.end();)
	{
		playing_sound_t* playing = *it;

		int end_of_file = openal_source_update_sound(playing);

		if (end_of_file)
		{
			openal_source_close(playing->openal_source);
			it = internal_sound_list.erase(it);
		}
		else
		{
			it++;
		}
	}

	for (auto it = internal_buffer_list.begin();
		      it != internal_buffer_list.end();
		      it++)
	{
		openal_source_update_buffer(*it);
	}
}

audio_sound_t* audio_sound_load(const char* inputurl)
{
	vorbis_t* vorbis_data;
	audio_sound_t* ret;

	size_t channels;
	size_t samplerate;

	size_t read;
	size_t buffsize = 0;

	if ((vorbis_data = vorbis_open(
		inputurl,
		&channels,
		&samplerate)) == NULL)
	{
		return NULL;
	}

	if ((ret = (audio_sound_t*)malloc(sizeof(audio_sound_t))) == NULL)
	{
		vorbis_close(vorbis_data);
		return NULL;
	}

	#define CHUNK_SIZE 512
	char* buffer = (char*)malloc(CHUNK_SIZE);

	while (1)
	{
		read = vorbis_read(vorbis_data, buffer + buffsize, CHUNK_SIZE);
		buffsize += read;

		if (read == CHUNK_SIZE)
		{
			buffer = (char*)realloc(buffer, buffsize + CHUNK_SIZE);
		}
		else
		{
			break;
		}
	}

	vorbis_close(vorbis_data);

	ret->buffer = buffer;
	ret->buffer_size = buffsize;
	ret->channels = channels;
	ret->samplerate = samplerate;

	return ret;
}

extern void audio_sound_unload(audio_sound_t* sound)
{
	if (sound != NULL)
	{
		free(sound->buffer);
		free(sound);
	}
}

extern void audio_sound_play(audio_sound_t* sound)
{
	playing_sound_t* playing;
	
	if ((playing = (playing_sound_t*)malloc(sizeof(playing_sound_t))) == NULL)
	{
		return;
	}

	if ((playing->openal_source = openal_source_create(sound->channels, sound->samplerate)) == NULL)
	{
		free(playing);
		return;
	}

	playing->current_position = 0;
	playing->sound = sound;

	internal_sound_list.push_back(playing);
}

extern void audio_buffer_register(audio_buffer_t* buffer)
{
	internal_buffer_list.push_back(buffer);
}

extern void audio_buffer_unregister(audio_buffer_t* buffer)
{
	internal_buffer_list.remove(buffer);
}
