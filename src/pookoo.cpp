#include <log/log.h>
#include <config/config.h>
#include <python/python.h>
#include <utils/utils.h>
#include <utils/const.h>

#include <stdio.h> // printf
#include <string.h> // strcmp

static void console_logger(int level, const char* message)
{
	const char* prefix;

	if (level == LOG_INFO)
	{
		prefix = "Info";
	}
	else if (level == LOG_ERROR)
	{
		prefix = "\033[31mError\033[0m";
	}
	else if (level == LOG_DEBUG)
	{
		prefix = "Debug";
	}
	else
	{
		prefix = "Expecting";
	}

	printf("%s: %s\n", prefix, message);
}

int main(int argc, char** argv)
{
	if (!log_begin())
	{
		printf("Failed to create log file... output will be unsaved.\n");
	}

	config_begin();


	const char* script_path = config_read_str("script_path", "script");
	const char* script_main = config_read_str("script_main", "main");

	python_init(script_path);

	for (int i = 1; i < argc; i++)
	{
		log_error("Unknown command-line argument: '%s'", argv[i]);
	}

	log_handler(console_logger);
	python_runfile(script_main);

	//Python script config
	config_write_str("script_path",script_path);
	config_write_str("script_main",script_main);
	python_exit();




	config_finish();
	log_clear();
	log_finish();

	return 0;
}
