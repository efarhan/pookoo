/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <font/font.h>
#include <log/log.h>

#include <draw/internal/opengl_utils.h>
#include <draw/internal/internal.h>

#include <FTGL/ftgl.h>

#include <stdlib.h>

#define FONT_BUFFER_SIZE 512
#define FTGL_FONT_TYPE FTTextureFont

struct font_t
{
	FTGL_FONT_TYPE* ftgl;
};

font_t* font_open(const char* filename)
{
	font_t* font;
	FTGL_FONT_TYPE* ftgl;

	if ((font = (font_t*)malloc(sizeof(font_t))) == NULL)
	{
		return NULL;
	}

	ftgl = new FTGL_FONT_TYPE(filename);

	if(ftgl->Error())
	{
		delete ftgl;
		free(font);
		return NULL;
	}

	// A default font size
	ftgl->FaceSize(36);

	font->ftgl = ftgl;
	return font;
}

extern void font_close(font_t* font)
{
	if (font != NULL)
	{
		delete font->ftgl;
		free(font);
	}
}

void font_facesize(font_t* font, unsigned int size)
{
	font->ftgl->FaceSize(size);
}

void font_size(font_t* font, float* advance, float* height, const char* format, ...)
{
	if (advance)
	{
		va_list args;
		char buffer[FONT_BUFFER_SIZE];

		va_start(args, format);
		vsnprintf(buffer, FONT_BUFFER_SIZE, format, args);
		va_end(args);

		*advance = font->ftgl->Advance(buffer);
	}

	if (height)
	{
		*height = font->ftgl->LineHeight();
	}

}

void font_render(font_t* font, const char* format, ...)
{
	float h;
	float sx, sy;
	va_list args;
	char buffer[FONT_BUFFER_SIZE];

	va_start(args, format);
	vsnprintf(buffer, FONT_BUFFER_SIZE, format, args);
	va_end(args);

	// Not very satisfied with this...
	internal_set_color();
	opengl_apply_matrix();

	glScalef(1.0f, -1.0f, 1.0f);
	font->ftgl->Render(buffer);
	glScalef(1.0f, -1.0f, 1.0f);
}

