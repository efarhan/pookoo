/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <config/config.h>
#include <utils/utils.h>
#include <utils/const.h>
#include <log/log.h>
#include <iterator>
#include <stdlib.h>
#include <set>
#include <string>
#include <string.h>

#include <iostream>

#include <expat.h>

typedef struct
{
	const char* tag;
	const char* value;
} config_t;

struct lt_config
{
	bool operator()(config_t* t1, config_t* t2) const
	{
		return strcmp(
			t1->tag,
			t2->tag) < 0;
	}
};

static std::set<config_t*, lt_config>* internal_registered;

static config_t* internal_register(const char* tag)
{
	config_t* config;

	if ((config = (config_t*)malloc(sizeof(config_t))) == NULL)
	{
		return NULL;
	}

	config->tag = utils_dupstr(tag);
	config->value = NULL;

	internal_registered->insert(config);

	return config;
}

static void internal_xml_start(void *data, const char *el, const char **attr)
{
	auto depth = (int*)data;

	if (*depth == 0)
	{
		if (!strcmp(el, "config"))
		{
			// Nothing to do.
		}
		else if (!strcmp(el, "entry"))
		{
			const char* key = NULL;
			const char* value = NULL;

			for (int i = 0; attr[i]; i += 2)
			{
				if (!strcmp(attr[i], "key"))
				{
					key = attr[i + 1];
				}
				else if (!strcmp(attr[i], "value"))
				{
					value = attr[i + 1];
				}
				else
				{
					log_error("Invalid attribute [%s] for <entry> tag.");
					return;
				}
			}

			if (key == NULL || value == NULL)
			{
				log_error("<entry> tag should have both the [key] and [value] attributes.");
				return;
			}

			config_t* config = internal_register(key);
			config->value = utils_dupstr(value);
		}
		else
		{
			log_error("Invalid tag <%s> at depth [%d].", el, *depth);
			return;
		}
	}

	*depth++;
}

static void internal_xml_end(void *data, const char *el)
{
	auto depth = (int*)data;
	*depth--;
}

void config_begin()
{
	XML_Parser parser;
	char buffer[PATH_LIMIT];
	FILE* f;
	int done;
	int depth = 0;

	internal_registered = new std::set<config_t*, lt_config>();

	utils_data_path(buffer, PATH_LIMIT, CONFIG_FILE);
	if ((f = fopen(buffer, "r")) == NULL)
	{
		// Nothing to parse, file was not found.

		// Try to create the directory if it doesn't exist.
		char buffer[PATH_LIMIT];
		utils_data_path(buffer, PATH_LIMIT, "");
		utils_makedir(buffer);

		return;
	}

	if ((parser = XML_ParserCreate(NULL)) == NULL)
	{
		// Woops :/
		log_error("Internal error on XML parsing.");

		fclose(f);
		return;
	}

	XML_SetElementHandler(parser, internal_xml_start, internal_xml_end);
	XML_SetUserData(parser, &depth);

	do
	{
		size_t len = fread(buffer, 1, PATH_LIMIT, f);
		done = feof(f);

		if (XML_Parse(parser, buffer, (int)len, done) != XML_STATUS_OK)
		{
			// Something went wrong...
			log_error("Something went wrong while reading the configuration file. Ignoring.");

			fclose(f);
			XML_ParserFree(parser);
			return;
		}
	} 
	while (!done);

	XML_ParserFree(parser);
	fclose(f);
}

const char* config_read_str(const char* tag, const char* default_value)
{
	config_t cmp;

	cmp.tag = tag;

	auto it = internal_registered->find(&cmp);

	if (it == internal_registered->end())
	{

		return default_value;
	}

	config_t* config = *it;
	return config->value;
}

int config_read_int(const char* tag, int default_value)
{
	const char* value = config_read_str(tag, NULL);

	int ret = 0;

	if (value != NULL)
	{
		ret = atoi(value);
	}

	if (ret == 0)
	{
		ret = default_value;
	}

	return ret;
}

int config_write_str(const char* tag, const char* value)
{
	config_t cmp;
	config_t* config;

	cmp.tag = tag;

	auto it = internal_registered->find(&cmp);

	if (it != internal_registered->end())
	{
		config = *it;
	}
	else
	{
		config = internal_register(tag);

		if (config == NULL)
		{
			return 0;
		}
	}
	const char * old_value = config->value;

	config->value = utils_dupstr(value);
	free((void*)old_value);
	return 1;
}

int config_write_int(const char* tag, int value)
{
	std::string str = std::to_string(value);

	return config_write_str(tag, str.c_str());
}

void config_finish()
{
	char buffer[PATH_LIMIT];
	FILE* f;

	utils_data_path(buffer, PATH_LIMIT, CONFIG_FILE);

	if ((f = fopen(buffer, "w")) == NULL)
	{
		log_error("Something went wrong while writing configuration file :(");
		return;
	}

	fprintf(f, "<config>\n");
	printf("Write config %s\n", buffer);
	for (auto it = internal_registered->begin();
		      it != internal_registered->end();)
	{
		config_t* config = *it;
		printf("<entry key='%s' value='%s' />\n", config->tag, config->value);
		fprintf(f, "\t<entry key='%s' value='%s' />\n", config->tag, config->value);

		free((void*)config->value);
		free((void*)config->tag);
		free(config);

		it = internal_registered->erase(it);

	}
	internal_registered->clear();
	delete internal_registered;

	fprintf(f, "</config>\n");
	fclose(f);
}
