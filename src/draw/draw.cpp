/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <draw/internal/opengl.h>
#include <draw/internal/internal.h>
#include <draw/internal/opengl_utils.h>

#include <window/window.h>
#include <window/internal/internal.h>

#include <log/log.h>

static int internal_push_depth = 0;
static float internal_r;
static float internal_g;
static float internal_b;
static float internal_a;

void draw_save()
{
	glPushMatrix();
	internal_push_depth++;
}

void draw_restore()
{
	glPopMatrix();
	internal_push_depth--;
}

void draw_clear(void)
{
	if(window_was_reset())
	{
		opengl_reset(window_width(), window_height());
	}

	opengl_load_identity();

	draw_clip_finish();
	draw_clip_begin(0, 0, window_width(), window_height());

	if (internal_push_depth != 0)
	{
		log_debug("Asymmetric push/pop calls!!");
		internal_push_depth = 0;
	}
}

void draw_clip_begin(
	unsigned int px, 
	unsigned int py,
	unsigned int sx,
	unsigned int sy)
{
	opengl_clip(
		px, 
		py,
		sx,
		sy,
		window_width(),
		window_height());
}

void draw_clip_finish()
{
	opengl_stop_clip(
		window_width(), 
		window_height());
}

void draw_translate(float dx, float dy)
{
	glTranslatef(dx, dy, 0.0f);
}

void draw_scale(float sx, float sy)
{
	glScalef(sx, sy, 1.0f);
}

void draw_rotate(float angle)
{
	glRotatef(angle, 0.0f, 0.0f, 1.0f);
}

void draw_color_rgba(
	float r, 
	float g, 
	float b, 
	float a)
{
	internal_r = r;
	internal_g = g;
	internal_b = b;
	internal_a = a;
}

int draw_begin()
{
	GLenum err = glewInit();

	if (err != GLEW_OK)
	{
		log_error("Failed to initialize GLEW: %s", glewGetErrorString(err));
		return 0;
	}

	if (!opengl_init())
	{
		return 0;
	}

	draw_color_rgba(1.0f, 1.0f, 1.0f, 1.0f);

	return 1;
}

void draw_finish()
{
	opengl_finish();
}

void draw_rectangle(float sx, float sy)
{
	opengl_draw_rectangle(
		internal_r,
		internal_g,
		internal_b,
		internal_a,
		sx,
		sy);
}

void draw_line(int dx, int dy, float width)
{
	opengl_draw_line(
		internal_r,
		internal_g,
		internal_b,
		internal_a,
		dx,
		dy,
		width);
}

extern void draw_circle(float outer_radius, float inner_ratio)
{
	opengl_draw_circle(
		internal_r,
		internal_g,
		internal_b,
		internal_a,
		outer_radius,
		inner_ratio);
}

void draw_texture(texture_t* texture, int flip)
{
	opengl_draw_texture(texture, flip);
}

void internal_set_color()
{
	glColor4f(
		(GLfloat)internal_r,
		(GLfloat)internal_g,
		(GLfloat)internal_b,
		(GLfloat)internal_a);
}

void draw_blend(blend_t blend)
{
	if (blend == BLEND_NORMAL)
	{
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}
	else if (blend == BLEND_MASKER)
	{
		glBlendFuncSeparate(GL_ZERO, GL_ONE, GL_DST_ALPHA, GL_ZERO);
	}
	else if (blend == BLEND_MASKED)
	{
		glBlendFunc(GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
	}
	else
	{
		log_error("Unknown blend value: %d", blend);
	}
}

void draw_cursor(int show)
{
	window_cursor_show(show);
}
