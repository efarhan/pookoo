/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

// OpenGL headers are in here.
#include <draw/internal/opengl.h>
#include <draw/internal/opengl_utils.h>
#include <utils/const.h>
#include <utils/alloca.h>
#include <log/log.h>

#include <stdlib.h>

static const GLcharARB* internal_fragment_circle = "                                                        \
	uniform float ratio;                                                                                    \
	void main() {                                                                                           \
		float dist = distance(gl_TexCoord[0].xy, vec2(0.5, 0.5)) * 2.0;                                     \
		float delta = fwidth(dist * 2.0);                                                                   \
		float a1 = smoothstep(1.0 - delta, 1.0, dist);                                                      \
		float a2 = smoothstep(ratio - delta, ratio, dist);                                                  \
																											\
		gl_FragColor = mix(vec4(gl_Color.rgb, 0.0), gl_Color, (1.0 - a1) * a2);                   \
	}";

static GLhandleARB internal_arb_program;
static GLhandleARB internal_arb_fragment_shader;

int opengl_init()
{
	internal_arb_program = glCreateProgramObjectARB();
	internal_arb_fragment_shader = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);

	// Compile the fragment shader.
	glShaderSourceARB(internal_arb_fragment_shader, 1, &internal_fragment_circle, NULL);
	glCompileShaderARB(internal_arb_fragment_shader);

	if (!opengl_shader_log(internal_arb_fragment_shader))
	{
		return 0;
	}

	// Create a complete program object.
	glAttachObjectARB(internal_arb_program, internal_arb_fragment_shader);
	glLinkProgramARB(internal_arb_program);

	if (!opengl_shader_log(internal_arb_program))
	{
		return 0;
	}

	return 1;
}

void opengl_finish()
{
	if (internal_arb_program != 0)
	{
		glDeleteObjectARB(internal_arb_program);
		glDeleteObjectARB(internal_arb_fragment_shader);

	}
}  
	
	

void opengl_clip(
	unsigned int px, 
	unsigned int py,
	unsigned int sx,
	unsigned int sy,
	unsigned int wx,
	unsigned int wy)
{
	(void)wx;

	/* Why is the top OpenGL's bottom ?! */
	glScissor(
		(GLint)px, 
		(GLint)(wy - py - sy), 
		(GLsizei)sx, 
		(GLsizei)sy);
}

void opengl_stop_clip(
	unsigned int wx,
	unsigned int wy)
{
	glScissor(
		0, 
		0, 
		(GLsizei)wx, 
		(GLsizei)wy);
}

void opengl_draw_rectangle(
	float r,
	float g,
	float b,
	float a,
	float w,
	float h)
{
	GLfloat vertices[] = 
	{
		      0.0f,       0.0f,
		(GLfloat)w,       0.0f,
		      0.0f, (GLfloat)h,
		(GLfloat)w, (GLfloat)h,
	};

	GLfloat colors[] =
	{
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
	};

	opengl_view_push();

	opengl_draw_data(vertices, NULL, colors, GL_TRIANGLE_STRIP, 4);
	
	opengl_view_pop();
}

void opengl_draw_line(
	float r,
	float g,
	float b,
	float a,
	float dx,
	float dy,
	float width)
{
	GLfloat vertices[] =
	{
		0.0f, 0.0f,
		(GLfloat)dx, (GLfloat)dy,
	};

	GLfloat colors[] =
	{
		r, g, b, a,
		r, g, b, a,
	};

	opengl_view_push();

	glLineWidth(width);
	glEnable(GL_LINE_SMOOTH);

	opengl_draw_data(vertices, NULL, colors, GL_LINE_LOOP, 2);
	opengl_view_pop();

	glDisable(GL_LINE_SMOOTH);
}

void opengl_draw_circle(
	float r,
	float g,
	float b,
	float a,
	float outer_radius,
	float inner_ratio)
{
	glUseProgramObjectARB(internal_arb_program);
	
	GLuint ratio = glGetUniformLocationARB(internal_arb_program, "ratio");
	glUniform1fARB(ratio, inner_ratio);

	GLfloat vertices[] = {
		-outer_radius, -outer_radius,
		outer_radius, -outer_radius,
		-outer_radius, outer_radius,
		outer_radius, outer_radius,
	};

	GLfloat tex_coord[] = {
		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,
		1.0f, 1.0f
	};

	GLfloat colors[] = {
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
		r, g, b, a,
	};

	opengl_view_push();
	opengl_draw_data(vertices, tex_coord, colors, GL_TRIANGLE_STRIP, 4);
	opengl_view_pop();

	glUseProgramObjectARB(0);
}

void opengl_draw_texture(texture_t* texture, int flip)
{
	GLuint tex = texture->texid;

	GLfloat vertices[] =
	{
		0.0f, 0.0f,
		(GLfloat)texture->width, 0.0f,
		0.0f, (GLfloat)texture->height,
		(GLfloat)texture->width, (GLfloat)texture->height,
	};
	GLfloat tex_indices [] = {
			0.0f,0.0f,
			0.0f,0.0f,
			0.0f,0.0f,
			0.0f,0.0f,
	};

	if(flip == 1)
	{
		GLfloat indices [] =
		{
			1.0f, 0.0f,
			0.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
		};
		for (int i = 0; i<8; i++)
			tex_indices[i] = indices[i];
	}
	else
	{
		GLfloat indices []=
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			0.0f, 1.0f,
			1.0f, 1.0f,
		};
		for (int i = 0; i<8; i++)
			tex_indices[i] = indices[i];
	}




	GLfloat colors[] =
	{
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f, 1.0f,
	};

	opengl_view_push();

	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex);

	opengl_draw_data(vertices, tex_indices, colors, GL_TRIANGLE_STRIP, 4);

	glDisable(GL_TEXTURE_2D);

	opengl_view_pop();

}

void opengl_reset(
	unsigned int wx, 
	unsigned int wy)
{
	glEnable(GL_SCISSOR_TEST);	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDisable(GL_DEPTH_TEST);

	glViewport(
		0,
		0,
		(GLsizei)wx,
		(GLsizei)wy);
	
	opengl_setup_projection(0, (GLfloat)wx, 0, (GLfloat)wy, 1.0f, -1.0f);
	opengl_load_identity();
}
