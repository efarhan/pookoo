/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <draw/internal/opengl_utils.h>
#include <draw/internal/opengl.h>
#include <draw/draw.h>

#include <log/log.h>
#include <utils/alloca.h>

#include <math.h>
#include <string.h> // memcpy

static GLfloat proj_matrix[16];

void opengl_setup_projection(
	GLfloat left,
	GLfloat right,
	GLfloat top,
	GLfloat bottom,
	GLfloat fr,
	GLfloat ner)
{
	GLfloat* m = proj_matrix;
	
	GLfloat tx = - (right + left  ) / (right -   left),
	        ty = - (top   + bottom) / (top   - bottom),
	        tz = - (fr    + ner   ) / (fr    -    ner);

	m[0]  = 2.0f / (right - left); 
	m[4]  = 0.0f; 
	m[8]  = 0.0f; 
	m[12] = tx;
	m[1]  = 0.0f; 
	m[5]  = 2.0f / (top - bottom); 
	m[9]  = 0.0f; 
	m[13] = ty;
	m[2]  = 0.0f; 
	m[6]  = 0.0f; 
	m[10] = -2.0f / (fr - ner); 
	m[14] = tz;
	m[3]  = 0.0f; 
	m[7]  = 0.0f; 
	m[11] = 0.0f; 
	m[15] = 1.0f;
}

void opengl_draw_data(
	GLfloat* vertices,
	GLfloat* texcoord,
	GLfloat* colors,
	GLenum type,
	GLsizei nb)
{
	
	// In any case.
	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
		
	glVertexPointer(2, GL_FLOAT, 0, vertices);
	glColorPointer (4, GL_FLOAT, 0, colors);

	// The only one optionnal, actually.
	if(texcoord != NULL)
	{
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		glTexCoordPointer(2, GL_FLOAT, 0, texcoord);
	}
	
	opengl_apply_matrix();

	glDrawArrays(type, 0, nb);

	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
		
	if(texcoord != NULL)
	{
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
	}
}

void opengl_load_identity()
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

void opengl_view_push()
{
	glPushMatrix();
}

void opengl_view_pop()
{
	glPopMatrix();
}

void opengl_apply_matrix()
{
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(proj_matrix);

	glMatrixMode(GL_MODELVIEW);
}

int opengl_shader_log(GLhandleARB handle)
{
	char* buffer;
	GLint buffer_size;

	glGetObjectParameterivARB(handle, GL_OBJECT_INFO_LOG_LENGTH_ARB, &buffer_size);

	if (buffer_size > 1)
	{
		buffer = (char*)alloca(buffer_size);
		glGetInfoLogARB(handle, buffer_size, NULL, buffer);

		log_info("Compile Log: %s\n", buffer);

		return 0;
	}

	return 1;
}