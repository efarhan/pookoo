/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#include <utils/utils.h>
#include <utils/internal/psnprintf.h>
#include <utils/const.h>

#include <stdlib.h> // getenv
#include <string.h> // strlen

#include <iostream>
#include <string>
#include <sstream>
#include <sys/stat.h>

#ifndef _WIN32
	#include <sys/stat.h> // mkdir
	#include <dirent.h>
#else
	#include <windows.h>
#endif

void utils_data_path(char* buffer, size_t bufflen, const char* filename)
{
#ifdef _WIN32
	psnprintf(buffer, bufflen, "%s/%s/%s", getenv("APPDATA"), PROGRAM_NAME, filename);
#else

	psnprintf(buffer, bufflen, "%s/.config/%s/%s", getenv("HOME"), PROGRAM_NAME, filename);
#endif
}

char* utils_dupstr(const char* str)
{
	size_t len = strlen(str) + 1;
	char* ret = (char*)malloc(len);
	memcpy((void*)ret, str, len);
	return ret;
}

int utils_snprintf(char* str, size_t n, const char* format, ...)
{
	va_list args;
	int ret;

	va_start(args, format);
	ret = pvsnprintf(str, n, format, &args);
	va_end(args);
	return ret;
}

char* utils_strtok(
	char* s1,
	const char* s2,
	char** lasts)
{
	char *ret;

	if (s1 == NULL)
	{
		s1 = *lasts;
	}

	while (*s1 && strchr(s2, *s1))
	{
		++s1;
	}

	if (*s1 == '\0')
	{
		return NULL;
	}

	ret = s1;

	while (*s1 && !strchr(s2, *s1))
	{
		++s1;
	}

	if (*s1)
	{
		*s1++ = '\0';
	}

	*lasts = s1;
	return ret;
}

int utils_makedir(char* directory)
{
	char* ptr;
	char* remember = NULL;
	char buffer[PATH_LIMIT],
		filename[PATH_LIMIT];

	buffer[0] = '\0';

	if ((ptr = utils_strtok(directory, "/\\", &remember)) != NULL)
	{
		do
		{
			if (buffer[0] != '\0' || directory[0] == '/')
			{
				psnprintf(filename, PATH_LIMIT, "%s/%s", buffer, ptr);
			}
			else
			{
				psnprintf(filename, PATH_LIMIT, "%s", ptr);
			}

			psnprintf(buffer, PATH_LIMIT, "%s", filename);

#ifdef _WIN32
			struct stat sb;
			if (stat(filename, &sb) != 0)
			{
				if (CreateDirectoryA(filename, NULL) == 0)
				{
					// We check for a *real* error, if the directory
					// wasn't fond we really don't care.
					if (GetLastError() == ERROR_PATH_NOT_FOUND)
					{
						// Oh no, we failed :(
						std::cerr << "Failed to create: "<<filename<<"\n";
						return 0;
					}
				}
			}

#else
			struct stat sb;
			if (stat(filename, &sb) != 0)
			{
				if ( mkdir(filename, S_IRWXU) < 0)
				{
					// Oh no, we failed :(
					fprintf(stderr, "Failed to create: %s\n", filename);
					return 0;
				}
			}

#endif
		} while ((ptr = utils_strtok(NULL, "/\\", &remember)) != NULL);
	}

	return 1;
}

int utils_dir_foreach(
	const char* dirname,
	void(*process)(const char*, int, void*),
	void* data)
{
#ifndef _WIN32

	DIR* dir;
	struct dirent* rem, entry;
	int err;

	if ((dir = opendir(dirname)) == NULL)
	{
		return 0;
	}

	for (err = readdir_r(dir, &entry, &rem);
		rem != NULL && err == 0;
		err = readdir_r(dir, &entry, &rem))
	{
		// Only list regular filed and directories.
		if (entry.d_type & DT_REG)
		{
			process(entry.d_name, 1, data);
		}
		else if (entry.d_type & DT_DIR)
		{
			process(entry.d_name, 0, data);
		}
	}

	closedir(dir);

	return 1;

#else

	HANDLE h;
	WIN32_FIND_DATA fd;

	char buffer[PATH_LIMIT];

	// We need to add a * to get all files in the
	// current directory.
	utils_snprintf(
		buffer,
		PATH_LIMIT,
		"%s*",
		dirname);

	if ((h = FindFirstFileA(buffer, &fd)) == INVALID_HANDLE_VALUE)
	{
		return 0;
	}

	do
	{
		int is_file = !(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY);
		process(fd.cFileName, is_file, data);
	} while (FindNextFileA(h, &fd));

	FindClose(h);

	return 1;

#endif
}

// Useful for python as library
void utils_free(void* ptr)
{
	free(ptr);
}
