############################################################
# SoundTouch audio processing library v1.6.0               #
#                                                          #
# SoundTouch library Copyright � Olli Parviainen 2001-2011 #
############################################################

# This CMakeLists.txt (C) Joshua Netterfield 2012.

########################## COMMON ##########################

cmake_minimum_required(VERSION 2.8)
add_definitions(-D_CRT_SECURE_NO_WARNINGS -DSOUNDTOUCH_INTEGER_SAMPLES)

file (GLOB soundtouch_cpp
    source/SoundTouch/*.cpp
    source/SoundTouch/*.h
)

include_directories (
    ./include
)

include_directories (.)

add_library (SoundTouch STATIC
    ${soundtouch_cpp}
)

if (UNIX)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -msse2")
endif (UNIX)