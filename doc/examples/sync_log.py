#
# This file shows the usage of all sync and log functions.
#

from pookoo import sync
from pookoo import log

import random

# Init library
if not sync.begin():
	log.error('Failed to init sync :(')
	exit()

# This will wait 1/10 of a second (10 FPS) between each s.step() call.
s = sync.Sync(10)

if s is None:
	# Fatal erro messages.
	log.error('Failed to create sync instance :(')

# Debug stuff.
log.debug('This message is only shown in debug mode.')

while True:
	n = int(random.random() * 1000000000)

	# We can know how much time elapsed between the last two calls to s.step().
	# Add this value at each frame to know the current time.
	# We will print this value as information.
	log.info(str(s.dt()))

	# We can set manually the number of nanoseconds between each call to s.step()
	# Argument is the number of nanoseconds between each frame.
	s.set(n)

	# This will wait the required time pass between each s.step() call as asked by s.set(),
	# or to the number of FPS which was passed in the constuctor of the instance if not set manually.
	s.step()

# Uninitialize everything we used.
del s

sync.finish()
