#
# This file shows the usage of the audio module.
#

from pookoo import audio
from pookoo import sync
from pookoo import log

# Initialize audio.
if not audio.begin():
	log.error('Failed to initialize audio :(')
	exit()
log.info("Audio succesfully initilaized")
# Initialize sync stuff not to go too fast.
if not sync.begin():
	log.error('Failed to initialize sync :(')
	exit()

# We sync at 60 FPS
sy = sync.Sync(60)

# Load a file which is streamed. Once it is fully read,
# it is unallocated automatically.
m = audio.Stream('data/sound/stream.ogg')

if m is None:
	log.error('Failed to load stream file :(')
	exit()

# Load a sound file which is loaded as a whole in memory,
# in order to be played at will multiple times.
s = audio.Sound('data/sound/sound.ogg')

if s is None:
	log.error('Failed to load sound file :(')
	exit()

# Some info to know when to play sounds.
t = 0.0
ot = int(t)

while True:
	audio.step()

	t = t + sy.dt()

	# Play the sound once each 3 seconds.
	if int(t) > ot + 3.0:
		s.play()
		ot = int(t)

	sy.step()

	# Status can be 'playing', 'pending' or 'stopped'
	# 'pending' means you stated multiple streams at once
	# and the once you are asking for will come later.
	if m.status() == 'stopped':
		log.info('Stream is done playing.')
		break

# Uninitialize everything we used.
del m
del sy
del s

audio.finish()
sync.finish()
