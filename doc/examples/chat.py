from pookoo import chat
from pookoo import sync
from pookoo import log

# First time initialization
if not chat.begin():
	log.error('Failed to initialize chat :(')
	exit()

# Create our connection to the server.
c = chat.Chat('wormspartybot@swissjabber.org/HOME', 'secret2')

# If we fail, None is returned.
if c is None:
	log.error('Failed to connect.')
	exit()

# We will sync to 30 FPS not to go too fast.
sync.begin()
s = sync.Sync(30)

while True:
	# This will fill an internal list with all the messages
	# received since the last call to this function
	c.step()

	# Get the first message.
	# Call 'receive' as long as the result is not None
	# to get each message one after another.
	m = c.receive()
	
	while m is not None:
		# Each time, give a simple response.
		c.send(m[0], m[1] + ' to you too!')

		# Get the next message.
		m = c.receive()

	s.step()
	
del s
del c

# Final uninitialization
chat.finish()