#
# This file shows the usage of the utils module.
#

from pookoo import utils
from pookoo import log

# Get the path where we can write configuration files.
# Differs from an OS to another.
path = utils.path()

log.info('Path: ' + path)

# At startup, call this to make sure you can write
# in the directory we received:
utils.mkdir(path)