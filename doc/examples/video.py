#
# This file shows the usage of the video module.
#

from pookoo import window
from pookoo import draw
from pookoo import video
from pookoo import sync
from pookoo import log
from pookoo import audio

# Open the window and initialize graphics.
if not window.begin():
    log.error('Failed to open window :(')
    exit()

if not draw.begin():
    log.error('Failed to init draw :(')
    exit()

# First time initialization. Since videos can play
# audio, video.begin() will also call audio.begin(), no need
# to call it yourself.
if not video.begin():
    log.error('Failed to init video :(')
    exit()

if not sync.begin():
    log.error('Failed to init sync :(')
    exit()

# Open the video at the specified location.
v = video.Video('data/video/video.webm')

# None is returned if we fail.
if v is None:
    log.error('Failed to open video :(')
    exit()

# Get the video's resolution, which is constant.
vs = v.size()

s = sync.Sync(60)

old_t = 0
new_t = 0

while window.step():
    draw.clear()

    # Scale the window such that the video is always fullscreen.
    ws = window.size()
    draw.scale(ws[0] / vs[0], ws[1] / vs[1])

    #
    # This will render the next frame of the video, and returns the
    # timestamp of this frame. This is useful to synchronize at the
    # FPS the video requires.
    #
    # This will simply render a square of the resolution of the video
    # in an OpenGL context, so you can also vall draw.rotate() or similar
    # functions if you need fancy effects.
    #
    new_t = v.next()

    # None is returned if the video is over.
    if new_t is None:
        break

    # We tell that we should wait the time between the current frame
    # and the previous frame, in case the framerate is not constant
    # within the video.
    s.set(new_t - old_t)
    old_t = new_t

    # If you don't call this, no audio will be played for the video
    # if it has an audio track.
    audio.step()

    s.step()

del v
del s

# This will also call audio.finish(), no need to call it yourself.
video.finish()

sync.finish()
draw.finish()
window.finish()
