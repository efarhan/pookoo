#
# This file shows the usage of the texture module.
# It also shows the usage of draw.blend
#

from pookoo import window
from pookoo import draw
from pookoo import sync
from pookoo import log
from pookoo import texture

import math

if not window.begin():
	log.error('Failed to open window :(')
	exit()

if not draw.begin():
	log.error('Failed to init draw :(')
	exit()

if not sync.begin():
	log.error('Failed to init sync :(')
	exit()
texture.begin()


s = sync.Sync(60)

# We try to open out mask texture
m = texture.Texture('doc/examples/data/mask.png')

# We can get the textures size, which is constant
ms = m.size()

# None is returned in case of failure.
if m is None:
	log.error('Failed to open mask texture :(')
	exit()

# Try and open the background texture.
t = texture.Texture('doc/examples/data/texture.png')
ts = t.size()

if t is None:
	log.error('Failed to open texture :(')
	exit()

time = 0

while window.step():
	draw.clear()

	ws = window.size()
	draw.color(0.1, 0.1, 0.1)

	# Tell we want to draw a normal texture.
	draw.blend.normal()
	draw.rectangle(ws)

	draw.scale(ws[0] / 640)

	# This tells that the texture we are drawing is a mask,
	# for what will be drawn as 'draw.blend.masked'
	draw.move([320 - ms[0] / 2 - 75, 240 - ts[1] / 2])
	draw.blend.masker()
	m.render(0)

	# We can draw multiple mask textures after another
	# which are combined.
	draw.move([150, 0])
	m.render(0)

	# We now draw our texture as 'masked'. Only a small region
	# will be drawn, according to the two masks applied above.
	draw.move([(ms[0] - ts[0]) / 2, (ms[1] - ts[1]) / 2])
	draw.move([-100 + 200 * math.sin(time), 0.0])
	draw.blend.masked()
	t.render(0)

	time = time + s.dt()
	s.step()
	print(s.dt())

del s
del m
del t

texture.finish()
draw.finish()
sync.finish()
window.finish()