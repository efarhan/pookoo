#
# This file shows the usage of the font and input module.
#

from pookoo import window
from pookoo import draw
from pookoo import sync
from pookoo import log
from pookoo import font
from pookoo import input

if not window.begin():
	log.error('Failed to open window :(')
	exit()

if not draw.begin():
	log.error('Failed to init draw :(')
	exit()

if not sync.begin():
	log.error('Failed to init sync :(')
	exit()

# Open the font
f = font.Font('data/font.ttf')

# None is returned if we fail.
if f is None:
	log.error('Failed to open font :(')
	exit()

s = sync.Sync(60)

# Call only if you need joysticks.
# No initialization required for keyboard or mouse
# except to have window.open called beforehand.
if not input.joystick.begin():
	log.error('Failed to open joystick :(')
	exit()

j = None

# Let's try to open the first joystick, but it's not fatal.
if input.joystick.count() > 0:
	j = input.joystick.Joystick(0)

#
# Unfortunately, we don't provide bindings for each keyboard key.
# Will have to look at standard scancodes yourself.
# You can check the values in standard SDL header or:
# http://wiki.libsdl.org/SDLScancodeLookup
#
class Keys():
	Right = 79
	Left = 80
	Down = 81
	Up = 82
	W = 26
	A = 4
	S = 22
	D = 7

# Prints some text, and moves to the right not to overlap
# with the next text.
def print_label(label, fs):
	draw.push()
	draw.move([50, fs[1]])
	draw.color(1.0, 1.0, 1.0)

	# Prints the given text on the screen. draw functions can be combined
	# like draw.color, draw.rotate, etc.
	f.render(label)

	draw.move([fs[0], 0.0])

# Go back to the left and jump to the next line.
def move_back(fs):
	draw.pop()
	draw.move([0.0, fs[1]])

def print_key(key, text):
	label = 'Keyboard ' + text + ': '

	# We get the size the text would take on the screen
	# if it was rendered. We need this to make multiple
	# texts not overlap.
	fs = f.size(label)

	print_label(label, fs)

	if input.keyboard.pressed(key):
		draw.color(0.1, 0.8, 0.1)
		f.render('Yes')
	else:
		draw.color(0.8, 0.1, 0.1)
		f.render('No')

	move_back(fs)

# Prints some text, and goes down one line, used if no color is needed.
def simple_print(label):
	fs = f.size(label)
	print_label(label, fs)
	move_back(fs)

def print_custom():
	# We get the size the text would take on the screen
	# if it was rendered
	label = 'Keyboard custom : '
	fs = f.size(label)

	print_label(label, fs)

	# Get the scancode of the key that is currently
	# being pressed. None is returned in the appropriate case.
	key = input.keyboard.get()

	if key is not None:
		# We can get a human-readable string of the given scancode.
		name = input.keyboard.name(key)

		if name is not None:
			f.render(name)
		else:
			f.render('None')
	else:
		f.render('None')

	move_back(fs)

# Prints if the given mouse button is pressed.
def print_mouse(position, text):
	label = 'Mouse ' + text + ' : '
	fs = f.size(label)
	
	print_label(label, fs)

	# We can get if the given button is pressed on the mouse.
	# Values are 1 -> left, 2 -> middle, 3 -> right.
	if input.mouse.pressed(position):
		draw.color(0.1, 0.8, 0.1)
		f.render('Yes')
	else:
		draw.color(0.8, 0.1, 0.1)
		f.render('No')

	move_back(fs)

def print_position(label, pos):
	simple_print(label + '(' + '{0:.2f}'.format(pos[0]) + ', ' + '{0:.2f}'.format(pos[1]) + ')')

def print_joystick():
	if j is None:
		simple_print('No joystick detected.')
	else:
		# We can get the number of axis the joystick has.
		count = j.axis_count()
		simple_print('Joystick axis count: ' + str(count))

		# For each of them, print the axis value.
		for x in range(count):
			simple_print('Joystick axis ' + str(x) + ': ' + '{0:.2f}'.format(j.axis_get(x)))
		
		# We can get the button count.
		count = j.button_count()

		# For each of them, print if the button is pressed or not.
		for x in range(count):
			simple_print('Joystick button ' + str(x) + ': ' + str(j.button_get(x)))
		
# Full input string by the user.
input_string = ''

while window.step():
	draw.clear()

	ws = window.size()
	draw.color(0.1, 0.1, 0.1)
	draw.rectangle(ws)

	# Don't do draw.scale or the result will be ugly.
	# Do font.facesize(size) instead to get nicely rendered text.
	# Also, don't rely on the default font size.
	f.facesize(int(15 * ws[0] / 640.0))

	# We print the status of the arrow keys.
	print_key(Keys.Up, 'Up')
	print_key(Keys.Left, 'Left')
	print_key(Keys.Down, 'Down')
	print_key(Keys.Right, 'Right')

	# Print the name of the key that was last pressed.
	print_custom()

	# Get what the user typed and concatenate with what we got before.
	ks = input.keyboard.string()

	if ks is not None:
		input_string = input_string + ks

	# Print the resulting string.
	simple_print('Unicode input: ' + input_string)
		
	# Print if each mouse button is pressed or not.
	print_mouse(1, 'Left')
	print_mouse(2, 'Middle')
	print_mouse(3, 'Right')

	# We can get the mouse position.
	print_position('Mouse position: ', input.mouse.position())

	# Print the full joystick status.
	print_joystick()

	s.step()

del j
del f
del s

input.joystick.finish()
draw.finish()
sync.finish()
window.finish()
