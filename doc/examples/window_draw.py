#
# This file shows the usage of all functions but draw.blend.
# These are shown in the texture.py example.
#

from pookoo import window
from pookoo import draw
from pookoo import sync
from pookoo import log

import math

# Open the window
if not window.begin():
	log.error('Failed to open window :(')
	exit()

# Initialize graphics stuff
if not draw.begin():
	log.error('Failed to open draw :(')
	exit()

# Initialize sync stuff not to go too fast.
if not sync.begin():
	log.error('Failed to initialize sync :(')
	exit()

# We sync � 60 FPS
s = sync.Sync(60)
t = 0

# window.step() returns False if the user pressed keys
# which ask the application to close, like Alt+F4 or
# clicked on the windows top right X.
while window.step():
	w = 200

	# Clear window state
	draw.clear()

	# Fill the screen with red
	ws = window.size()
	draw.color(1.0, 0.0, 0.0)
	draw.rectangle(ws)

	# We only draw on a specific square, leaving a red border around the window.
	draw.clip.begin([10, 10], [ws[0] - 20, ws[1] - 20])
	draw.color(0.4, 0.2, 0.2)
	draw.rectangle(ws)

	# Move to the center of the screen
	draw.move([ws[0] / 2.0, ws[1] / 2.0])
	draw.color(0.0, 0.0, 1.0)

	# Scale such that for whatever resolution it looks somewhat the same.
	# With one argument we scale both X and Y, while with two is scales
	# each axis separately.
	scale = ws[0] / 640
	draw.scale(scale)

	# Draw a rotating blue square. Angle in degrees.
	# We push and pop because we don't want to keep the rotation.
	draw.push()
	draw.rotate(4 * t * math.pi)
	draw.move([- w, - w])
	draw.rectangle([w * 2, w * 2])
	draw.color(0.0, 1.0, 0.0)
	draw.move([w, w])

	# Draw a circle inside the square we just drawn, with dynamic size.
	draw.scale(0.5 * math.sin(t / 2) + 0.5)
	draw.circle(w, 0.5 * math.cos(t / 4) + 0.5)
	draw.pop()

	# Save the current state
	draw.push()

	# Let's now draw black lines around the window.
	draw.color(0.0, 0.0, 0.0)
	draw.move([-ws[0] / 2 + 10, -ws[1] / 2 + 10])
	draw.line([ws[0] - 20, 0])
	draw.move([ws[0] - 20, 0])
	draw.line([0, ws[1] - 20])
	draw.move([0, ws[1] - 20])
	draw.line([-ws[0] + 20, 0])
	draw.move([-ws[0] + 20, 0])
	draw.line([0, -ws[1] + 20])

	# Restore the saved state to go back to [0, 0]
	draw.pop()

	# Stop clipping (Not necessary if you call draw.reset right afterwards)
	draw.clip.stop()

	# Sync to the desired framerate.
	t = t + s.dt()

	s.step()

# Uninitialize everything we used.
del s

draw.finish()
window.finish()
sync.finish()
