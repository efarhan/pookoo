#ifndef VIDEO_H
#define VIDEO_H


#include <stdint.h> // uint64_t

struct video_t;

extern int      video_init(void);
extern video_t* video_open(const char* filename);
extern void     video_size(video_t* handle, unsigned int* width, unsigned int* height);
extern int      video_next(video_t* handle, uint64_t* timestamp);
extern void     video_close(video_t* handle);
extern void     video_finish(void);


#endif
