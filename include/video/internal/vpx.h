
#ifndef VPX_H
#define VPX_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h> // size_t

struct vpx_t;

extern vpx_t*          vpx_open(void);
extern int             vpx_feed(vpx_t* handle, unsigned char* buffer, size_t buffer_size);
extern unsigned char** vpx_read(vpx_t* handle, unsigned int* width, unsigned int* height, int* stride);
extern void            vpx_close(vpx_t* handle);

#ifdef __cplusplus
}
#endif

#endif