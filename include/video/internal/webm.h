
#ifndef WEBM_H
#define WEBM_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h> // size_t
#include <stdio.h> // FILE*
#include <stdint.h> // uint64_t

#include <vorbis/codec.h>

// webm_read's data_type
enum
{
	WEBM_VIDEO,
	WEBM_AUDIO
};

typedef struct
{
	vorbis_info info;
	vorbis_block block;
	vorbis_comment comment;
	vorbis_dsp_state state;
} vorbis_stream_t;

struct webm_t;

extern webm_t* webm_open(FILE* file, unsigned int* width, unsigned int* height, vorbis_stream_t* vorbis);
extern int     webm_read(webm_t* handle, unsigned char** data, size_t* data_size, uint64_t* timestamp, int* data_type);
extern void    webm_close(webm_t* handle);

#ifdef __cplusplus
}
#endif

#endif