/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _FONT_H_
#define _FONT_H_


#define FONT_MODULE_NAME "font"

struct font_t;

extern font_t* font_open(const char* filename);
extern void    font_close(font_t* font);

// Get the size rendering the given text would take
extern void font_size(font_t* font, float* width, float* height, const char* text, ...);

// Set the given face size to the font
extern void font_facesize(font_t* font, unsigned int size);

// Render the given text.
extern void font_render(font_t* font, const char* text, ...);


#endif
