/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _TEXTURE_H_
#define _TEXTURE_H_


#define TEXTURE_MODULE_NAME "texture"

struct texture_t;

extern void texture_begin(void);
extern int texture_finish(void);

// From file
extern texture_t* texture_open(const char*);
extern void texture_close(texture_t*);
extern int texture_width(texture_t* t);
extern int texture_height(texture_t* t);

#endif
