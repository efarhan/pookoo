/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef __OPENAL_H__
#define __OPENAL_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <audio/internal/internal.h>

#include <stdlib.h> // size_t

struct openal_t;

extern int  openal_begin(void);
extern void openal_finish(void);

extern int    openal_capture_begin(void);
extern size_t openal_capture_read(char* buffer, size_t buffsize);
extern void   openal_capture_finish(void);

extern openal_t* openal_source_create(size_t channels, size_t samplerate);
extern void      openal_source_close(openal_t* source);
extern void      openal_source_params(openal_t* source, size_t* channels, size_t* samplerate);

extern int openal_source_update_stream(audio_stream_t* data);
extern int openal_source_update_sound(playing_sound_t* data);
extern int openal_source_update_buffer(audio_buffer_t* data);

#ifdef __cplusplus
}
#endif

#endif
