/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#include <stdlib.h> // size_t

#ifdef __cplusplus
extern "C" {
#endif

struct vorbis_t;

extern vorbis_t* vorbis_open (const char*, size_t*, size_t*);
extern size_t    vorbis_read(vorbis_t*, const char*, size_t);
extern void      vorbis_close(vorbis_t*);

#ifdef __cplusplus
}
#endif
