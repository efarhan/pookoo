/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/
#ifndef _AUDIO_INTERNAL_H_
#define _AUDIO_INTERNAL_H_

#include <stdlib.h> // size_t

#ifdef __cplusplus
extern "C" {
#endif

#define OPENAL_BUFFER_SIZE 2048
#define OPENAL_NB_BUFFERS 8

struct vorbis_t;
struct openal_t;

// Used when streaming music (ogg/vorbis)
struct audio_stream_t
{
	vorbis_t* vorbis_data;
	openal_t* openal_source;
	void* stream_soundtouch;
	float volume;

	int is_stopped;
};

// Used to keep in memory a given full sound (wav)
struct audio_sound_t
{
	char* buffer;
	size_t buffer_size;

	size_t channels;
	size_t samplerate;
};

// Used to keep in memory buffered data from video decoding (webm)
struct audio_buffer_t
{
	// This seems to be enough of a buffer. It needs to be long enough to hold
	// multiple audio packets between two video frames. We can't afford to realloc this,
	// so it needs to be big!
	char buffer[OPENAL_BUFFER_SIZE * OPENAL_NB_BUFFERS];
	size_t buffer_pos;
	openal_t* openal_source;
};

struct playing_sound_t
{
	audio_sound_t* sound;

	size_t current_position;
	openal_t* openal_source;
};

extern void* internal_stream_soundtouch_get(void);
extern float internal_stream_tempo_get(void);
extern float internal_volume_get(void);

#ifdef __cplusplus
}
#endif

#endif
