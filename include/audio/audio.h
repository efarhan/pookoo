/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#ifndef _AUDIO_H
#define _AUDIO_H



#define AUDIO_MODULE_NAME "audio"

struct audio_stream_t;
struct audio_sound_t;
struct audio_buffer_t;

enum stream_status
{
	PLAYING,
	PENDING,
	STOPPED
};

// First time init and uninit after application close
extern int  audio_begin(void);
extern void audio_finish(void);

// Load a given sound file and add it to the current playlist.
extern audio_stream_t* audio_stream_load(const char*);
extern void            audio_stream_unload(audio_stream_t*);
extern void            audio_stream_clear(void); // Delete all loaded streams
extern stream_status   audio_stream_status(audio_stream_t* audio); // Get the stream status, see above enum.
extern void            audio_stream_volume_set(audio_stream_t* audio, float volume);
extern float           audio_stream_volume_get(audio_stream_t* audio);

// Microphone input handling.
extern int    audio_capture_begin(void);
extern size_t audio_capture_read(char* buffer, size_t buffsize);
extern void   audio_capture_finish(void);

extern void audio_tempo(float tempo);
extern void audio_pitch(float pitch);

extern audio_sound_t* audio_sound_load(const char*);
extern void audio_sound_unload(audio_sound_t*);
extern void audio_sound_play(audio_sound_t*);

extern void audio_buffer_register(audio_buffer_t*);
extern void audio_buffer_unregister(audio_buffer_t*);

// Set global volume level. Combined with the volume of each sound.
extern void audio_volume(float volume);
// To be called between each frame.
extern void audio_step(void);


#endif
