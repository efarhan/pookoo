/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _OPENGL_DRIVER_H_
#define _OPENGL_DRIVER_H_

#include "opengl_header.h"
#include <texture/internal/gltexture.h>
#include <draw/draw.h>

#ifdef __cplusplus
extern "C" {
#endif

extern int  opengl_init(void);
extern void opengl_finish(void);

extern void opengl_reset(
	unsigned int wx, 
	unsigned int wy);

extern void opengl_clip(unsigned int x, unsigned int y, unsigned int w, unsigned int h, unsigned int, unsigned int);
extern void opengl_stop_clip(unsigned int, unsigned int);

extern void opengl_draw_rectangle(
	float r,
	float g,
	float b,
	float a,
	float w,
	float h); // Size

extern void opengl_draw_line(
	float r,
	float g,
	float b,
	float a,
	float dx,
	float dy, // Offset
	float width); // Line width

extern void opengl_draw_circle(
	float r,
	float g,
	float b,
	float a,
	float inner_radius,
	float outer_radius);

extern void opengl_draw_texture(texture_t*, int flip);

#ifdef __cplusplus
}
#endif

#endif
