/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _OPENGL_UTILS_H_
#define _OPENGL_UTILS_H_

#include "opengl_header.h"
#include <draw/draw.h>

#ifdef __cplusplus
extern "C" {
#endif

extern void opengl_setup_projection(
	GLfloat,
	GLfloat,
	GLfloat,
	GLfloat,
	GLfloat,
	GLfloat);

extern void opengl_load_identity(void);
extern void opengl_view_push(void);
extern void opengl_view_pop(void);
extern void opengl_apply_matrix(void);

extern void opengl_draw_data(
	GLfloat*,
	GLfloat*,
	GLfloat*,
	GLenum,
	GLsizei);

extern int opengl_shader_log(GLhandleARB);

#ifdef __cplusplus
}
#endif

#endif
