/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _OPENGL_HEADER_H_
#define _OPENGL_HEADER_H_

#include <GL/glew.h>

// Make sure Windows header is included,
// or including gl.h will fail.
#ifdef _WIN32

	#include <windows.h>

#endif

#if defined(__APPLE__)

	#include <OpenGL/gl.h>
	#include <OpenGL/glext.h>

#else

	#include <GL/gl.h>

	#ifndef _WIN32
		#include <GL/glext.h>
	#endif

#endif

#endif
