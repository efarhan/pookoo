/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _INTERNAL_DRAW_H_
#define _INTERNAL_DRAW_H_

#ifdef __cplusplus
extern "C" {
#endif

extern void internal_set_color(void);

#ifdef __cplusplus
}
#endif

#endif
