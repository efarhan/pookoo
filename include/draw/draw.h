/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _DRAWER_H_
#define _DRAWER_H_



#define DRAW_MODULE_NAME "draw"

struct texture_t;

enum blend_t {
	BLEND_NORMAL = 0,
	BLEND_MASKER = 1,
	BLEND_MASKED = 2,
};

// First time initialization
extern int  draw_begin(void);
extern void draw_finish(void);

// Call between each frame.
extern void draw_clear(void);

extern void draw_save();
extern void draw_restore();

extern void draw_color_rgba(
	float r, // 0.0 <= r, g, b, a <= 1.0
	float g,
	float b,
	float a);

extern void draw_translate(
	float dx, 
	float dy);

extern void draw_rotate(float angle);

extern void draw_clip_begin(
	unsigned int x,
	unsigned int width,
	unsigned int y,
	unsigned int height);

extern void draw_clip_finish(void);

extern void draw_scale(float sx, float sy);

extern void draw_rectangle(
	float size_x, 
	float size_y); 

extern void draw_line(
	int dx,
	int dy,
	float width);

extern void draw_circle(float outer_radius, float inner_ratio);

// See texture/texture.h
extern void draw_texture(texture_t*, int flip);

extern void draw_blend(blend_t blend);

extern void draw_cursor(int visible);


#endif
