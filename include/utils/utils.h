/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#ifndef _UTILS_H_
#define _UTILS_H_


#include <stdlib.h> // size_t
#include <string>

extern char* utils_dupstr(const char* str);

extern void utils_data_path(
	char* buffer, 
	size_t bufflen, 
	const char* filename);

extern char* utils_strtok(
	char*, 
	const char*, 
	char**);

template <typename T> extern std::string to_string(T value);

extern int utils_makedir(char* directory);

extern int utils_snprintf(
	char* str, 
	size_t n, 
	const char* format, ...);

extern int utils_dir_foreach(
	const char* dirname,
	void(*process)(const char* filename, int isfile, void* data),
	void* data);

extern void utils_free(void* ptr);


#endif
