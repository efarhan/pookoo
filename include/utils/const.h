
#ifndef _CONST_H_
#define _CONST_H_

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288   /* pi */
#endif

#define PROGRAM_NAME "Pookoo"
#define CONFIG_FILE "config.xml"
#define DATA_PATH ""
#define PATH_LIMIT 4096

#endif