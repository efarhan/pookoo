/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#ifndef _PHYSICS_H_
#define _PHYSICS_H_

#define PHYSICS_MODULE_NAME "physics"

struct physics_world_t;
struct physics_body_t;
struct physics_event_t
{
	void* user_data_a;
	void* user_data_b;
	bool begin;
};

extern void* physics_events_buffer_get();

extern physics_world_t* physics_world_begin(
	float g_x,
	float g_y,
	int velocity_it,
	int position_it);

extern void physics_world_step(
	physics_world_t* world, 
	float dt);

extern void physics_world_end(physics_world_t* world);

extern physics_body_t* physics_body_add_dynamic(
	physics_world_t* world,
	float px,
	float py,
	float angle);

extern physics_body_t* physics_body_add_static(
	physics_world_t* world,
	float px,
	float py,
	float angle);

extern void physics_body_dealloc(
	physics_body_t* body);

extern void physics_body_remove(
	physics_world_t* world, 
	physics_body_t* body);

extern void physics_body_set_position(
	physics_body_t* body,
	float x,
	float y);

extern void physics_body_get_position(
	physics_body_t* body,
	float* px,
	float* py);
extern void physics_body_set_angle(
	physics_body_t* body,
	float angle);
extern void physics_body_move(
	physics_body_t* body,
	float x,
	float y,
	float dt,
	int type);
extern void physics_body_stop(
	physics_body_t* body,
	int x,
	int y,
	float dt,
	int type);
extern void physics_body_jump(
	physics_body_t* body,
	float vy);

extern void physics_geometry_add_box(
	physics_body_t* body,
	float px,
	float py,
	float sx,
	float sy,
	float angle,
	int sensor,
	void* data);

extern void physics_geometry_add_circle(
	physics_body_t* body,
	float px,
	float py,
	float radius,
	int sensor,
	void* data);

extern void physics_raycast(
	physics_world_t* world,
	float p1x,
	float p1y,
	float p2x,
	float p2y);


#endif
