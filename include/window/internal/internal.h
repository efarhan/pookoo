/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */
#ifndef WINDOW_HANDLER_H
#define WINDOW_HANDLER_H

extern void* internal_window_handler;

extern int internal_last_keyboard_scancode;

#define MAX_KEYBOARD_INPUT_LEN 32
extern char internal_keyboard_text[MAX_KEYBOARD_INPUT_LEN];

extern int window_was_reset(void);

extern void window_cursor_show(bool show);


#endif
