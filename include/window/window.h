/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _WINDOW_H_
#define _WINDOW_H_

#define WINDOW_MODULE_NAME "window"

/*
 * Global functions.
 */
extern int  window_begin(void);
extern void window_finish(void);

extern int window_toggle_fullscreen(void);
extern int window_step(void);

extern char         window_is_fullscreen(void);
extern unsigned int window_width(void);
extern unsigned int window_height(void);



#endif
