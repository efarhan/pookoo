/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#ifndef _INPUT_H_
#define _INPUT_H_



#define INPUT_MODULE_NAME "input"

/*
 * Keyboard part
 */

// Returns 1 if the given key is pressed (use standard scancodes) or 0 otherwise.
// You can check the values in standard SDL header or :
// http ://wiki.libsdl.org/SDLScancodeLookup
extern int input_keyboard_pressed(int key);

// Returns the last pressed scancode. -1 for none.
extern int input_keyboard_get(void);


// Returns the last input the user typed (unicode string)
extern const char* input_keyboard_string_get(void);

// Marks the last input the user typed as read.
extern void input_keyboard_string_clear(void);

/*
 * Mouse part
 */
extern void input_mouse_position(int* px, int* py);
extern bool input_mouse_pressed(int button_nb);

/*
* Joystick part
*/
extern int input_joystick_begin(void);
extern void input_joystick_finish(void);


extern int input_joystick_count(void);
extern int input_joystick_axis_count(int joystick_nb);
extern int input_joystick_button_count(int joystick_nb);

extern float input_joystick_axis_get(int joystick_nb, int axis);
extern bool   input_joystick_button_get(int joystick_nb, int button_nb);


#endif
