/*
 * This file is part of the masters project:
 *
 * 'Implementation of a distributed computation framework'.
 *
 * Matej Pavlovic
 * Marc-Olivier Bloch
 *
 * 2012
 */
 
#ifndef _SLEEP_H_
#define _SLEEP_H_

#ifdef __cplusplus
extern "C" {
#endif

// Create a handle to be able to sleep.
// Create one per thread which needs to sleep.
extern void* sleep_open(void);

// Sleep for the given time.
extern void sleep_wait(
	int sec,
	int nsec,
	void* handle);

// Call when the given thread doesn't need to sleep.
extern void sleep_close(void* handle);

#ifdef __cplusplus
}
#endif

#endif
