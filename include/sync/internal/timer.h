/*
 * This file is part of the masters project:
 *
 * 'Implementation of a distributed computation framework'.
 *
 * Matej Pavlovic
 * Marc-Olivier Bloch
 *
 * 2012
 */
 
#ifndef _TIMER_H_
#define _TIMER_H_

#ifdef __cplusplus
extern "C" {
#endif

// Call once at startup to allocate timer stuff.
extern int timer_begin(void);

// Call every time you need the time.
// This function is thread-safe.
extern void timer_get(
	int* sec,
	int* nsec);

// Call once when closing the application
// to free timer memory.
extern void timer_finish(void);

#ifdef __cplusplus
}
#endif

#endif
