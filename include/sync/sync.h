/*
 * This file is part of the masters project:
 *
 * 'Implementation of a distributed computation framework'.
 *
 * Matej Pavlovic
 * Marc-Olivier Bloch
 *
 * 2012
 */
 
#ifndef _SYNC_H_
#define _SYNC_H_


struct sync_t;

extern int     sync_begin(void);
extern void    sync_finish(void);

extern sync_t* sync_open(unsigned int framerate);
extern void    sync_close(sync_t* handler);

extern void    sync_set(sync_t* handler, int nanoseconds); // Overrides original framerate for the given wait of nanoseconds between sync_step
extern int     sync_step(sync_t* handler);
extern float   sync_delta(sync_t* handler);


#endif
