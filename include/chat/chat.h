
#ifndef _CHAT_H_
#define _CHAT_H_

#include <stdlib.h> // size_t

struct chat_t;

extern int     chat_begin(void);
extern chat_t* chat_open(const char* login, const char* password);

extern void chat_step(chat_t* chat);
extern int  chat_receive(chat_t* chat, const char** from, const char** message);

// Send the given message to the given person.
extern void chat_direct(chat_t* chat, const char* to, const char* message, ...);
// Send the given message to the given group
extern void chat_group(chat_t* chat, const char* to, const char* message, ...);
// Send presence message to a given destinator. Useful for group chats.
extern void chat_presence(chat_t* chat, const char* to);

extern const char* chat_contact_added(chat_t* chat);
extern const char* chat_contact_deleted(chat_t* chat);

extern void chat_close(chat_t* chat);
extern void chat_finish(void);



#endif
