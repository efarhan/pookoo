/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#ifndef _PYTHON_INTERNAL_H_
#define _PYTHON_INTERNAL_H_



#include <Python.h>

#if PY_MAJOR_VERSION < 3
#error "At least Python32 is required"
#elif PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION < 2
#error "At least Python32 is required"
#endif


#endif
