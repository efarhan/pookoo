/*
* Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
*
* This file is part of the 'Beautiful, absurd, subtle.' project.
*
* 'Beautiful, absurd, subtle.' is free software: you can redistribute it
* and/or modify it under the terms of the 'New BSD License'.
*
*/

#ifndef _PYTHON_H_
#define _PYTHON_H_

#ifdef __cplusplus
extern "C" {
#endif

extern void python_exit(void);
extern void python_init(const char* script_path);

extern int python_runfile(const char* filename);

#ifdef __cplusplus
}
#endif

#endif
