/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _CONFIG_H_
#define _CONFIG_H_



// You can now call config_begin to fill the values each module registered.
extern void        config_begin(void);

// Get the value which was read during config_parse
extern const char* config_read_str(const char* tag, const char* default_value);
extern int         config_read_int(const char* tag, int default_value);

// Set new value to be written
extern int         config_write_str(const char* tag, const char* value);
extern int         config_write_int(const char* tag, int value);

// Save and close configuration
extern void        config_finish(void);



#endif
