/*
 * Copyright (C) 2012 Marc-Olivier Bloch <wormsparty [at] gmail [dot] com>
 *
 * This file is part of the 'Beautiful, absurd, subtle.' project.
 *
 * 'Beautiful, absurd, subtle.' is free software: you can redistribute it 
 * and/or modify it under the terms of the 'New BSD License'.
 *
 */

#ifndef _LOG_H_
#define _LOG_H_



#define LOG_MODULE_NAME "log"

enum
{
	LOG_INFO,
	LOG_ERROR,
	LOG_DEBUG,
};

extern int log_begin();

extern void log_handler(void(*)(int level, const char* message));
extern void log_clear(void);

extern void log_info(const char* format, ...);
extern void log_error(const char* format, ...);
extern void log_debug(const char* format, ...);

extern void log_finish();



#endif
