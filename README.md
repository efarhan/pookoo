#POOKOO ENGINE
Cross-platform engine using OpenGL, OpenAL, Box2D, Python, Expat, libpng, libvpx, libvorbis, SDL2, SoundTouch, freetype2. 

Can embed [Kudu Engine](https://bitbucket.org/efarhan/kudu).



##TODO
- Multi-texturing with custom alpha-blending
- A python function that toggle fullscreen and window size